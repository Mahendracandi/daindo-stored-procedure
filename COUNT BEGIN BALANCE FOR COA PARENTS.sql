BEGIN TRAN A
DECLARE @AccountCode nvarchar(30),
		@ColumnBegin nvarchar(max),
		@ColumnEnd nvarchar(max),
		@ColumnMut nvarchar(max),
		@ColumnDebit nvarchar(max),
		@ColumnCredit nvarchar(max);
DECLARE @MonthPeriod numeric(2);
DECLARE @SqlStmt nvarchar(max),
        @ParmDefinition nvarchar(max);
DECLARE @AccountCode2 nvarchar(30),
		@AccountCode1 nvarchar(30),
        @Level nvarchar(2),
        @CoaAcum nvarchar(30),
        @CoaAcumLevel nvarchar(2);
DECLARE @ColumnBeginEx NUMERIC(15,2),
		@ColumnDebitEx NUMERIC(15,2),
		@ColumnCreditEx NUMERIC(15,2);
DECLARE @Value numeric(15, 2),
		@DebitValue numeric(15, 2),
		@CreditValue numeric(15, 2);
DECLARE @newBeginBalance NUMERIC(15,2),
		@newDebit NUMERIC(15,2),
		@newCredit NUMERIC(15,2),
		@newMutValue NUMERIC(15,2),
		@newEndBalance NUMERIC(15,2);

SET @MonthPeriod = 12;

DECLARE c__coa_test CURSOR LOCAL FORWARD_ONLY FOR
SELECT 
	COA_CODE, BEGIN_BAL_12, DB_12, CR_12
FROM AKT.dbo.TRX_SALDO;

OPEN c__coa_test
WHILE 1 = 1
BEGIN
	FETCH c__coa_test INTO @AccountCode, @Value, @DebitValue, @CreditValue;
	PRINT '** START **'
	PRINT @AccountCode + ' ' + CONVERT(VARCHAR, @Value)

	IF @@fetch_status = -1
	BREAK;

	SET @ColumnBegin = 'BEGIN_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
    SET @ColumnEnd = 'END_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
    SET @ColumnMut = 'MUT_VALUE_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
    SET @ColumnDebit = 'DB_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
    SET @ColumnCredit = 'CR_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');

	DECLARE C_COA_LEVEL CURSOR LOCAL FORWARD_ONLY FOR
    WITH COA_LEVEL_X AS (SELECT
        COA_MST.COA_CODE,
        COA_MST.LEVEL,
        COA_MST.COA_ACCUM,
        1 AS LEVEL
    FROM COA_MST
    WHERE COA_MST.COA_CODE = @AccountCode
    AND ACTIVE_STATUS = 1
    UNION ALL
    SELECT
        COA_MST.COA_CODE,
        COA_MST.LEVEL,
        COA_MST.COA_ACCUM,
        COA_LEVEL_X.LEVEL + 1 AS LEVELS
    FROM COA_MST,
            COA_LEVEL_X
    WHERE COA_LEVEL_X.COA_ACCUM = COA_MST.COA_CODE
    AND ACTIVE_STATUS = 1
    AND COA_MST.LEVEL > 1)

    SELECT DISTINCT
        COA_LEVEL_X.COA_CODE,
        COA_LEVEL_X.LEVEL,
        COA_LEVEL_X.COA_ACCUM
    FROM COA_LEVEL_X
    WHERE COA_LEVEL_X.LEVEL > 1
    ORDER BY COA_LEVEL_X.LEVEL DESC;

    OPEN C_COA_LEVEL
    WHILE 1 = 1
    BEGIN
        FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;
                
		PRINT @AccountCode2 + ' ' + @Level + ' ' + @CoaAcum

        IF @@FETCH_STATUS = -1
            BREAK
		
		IF (@AccountCode != @AccountCode2)
		BEGIN
			PRINT 'UPDATED!'

			SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT, @ColumnDebitEx NUMERIC(20,4) OUTPUT, @ColumnCreditEx NUMERIC(20,4) OUTPUT';
			SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ',
										@ColumnDebitEx = ' + ISNULL(@ColumnDebit, 0) + ',
										@ColumnCreditEx = ' + ISNULL(@ColumnCredit, 0) + '
										 FROM AKT.dbo.TRX_SALDO
										   WHERE COA_CODE=''' + @AccountCode2 + '''';

			PRINT 'PREPARE ' + @AccountCode2
			PRINT @SqlStmt

			EXECUTE sp_executesql @SqlStmt,
								@ParmDefinition,
								@ColumnBeginEx = @ColumnBeginEx OUTPUT,
								@ColumnDebitEx = @ColumnDebitEx OUTPUT,
								@ColumnCreditEx = @ColumnCreditEx OUTPUT;

			SET @newBeginBalance = @ColumnBeginEx + @Value;
			SET @newDebit = @ColumnDebitEx + @DebitValue;
			SET @newCredit = @ColumnCreditEx + @CreditValue;
			SET @newMutValue = @newDebit - @newCredit;
			SET @newEndBalance = @newBeginBalance + @newMutValue;

			SET @SqlStmt =
					N'UPDATE AKT.dbo.TRX_SALDO SET ' +
					ISNULL(@ColumnBegin, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newBeginBalance, 0)) + ', ' +
					ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newDebit, 0)) + ', ' +
					ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newCredit, 0)) + ', ' + 
					ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newMutValue, 0)) + ', ' + 
					ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newEndBalance, 0)) + '
							WHERE  COA_CODE =''' + @AccountCode2 + '''';

			PRINT 'UPDATE ' + @AccountCode2
			PRINT @SqlStmt
                
			-- execute update coa query'
			EXECUTE sp_executesql @SqlStmt;
		END
		ELSE
		BEGIN
			PRINT 'SAME!'
		END

		SET @CoaAcumLevel = NULL -- restart variable
        SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
	    WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'
                
        IF (@CoaAcumLevel = '1')
		BEGIN
			SELECT
                @AccountCode1 = ISNULL(COA_CODE, ' ')
            FROM COA_MST
            WHERE COA_CODE = @CoaAcum -- CoaAcum -> coa parent from #ListCoaLevel
            AND ACTIVE_STATUS = 1;

			PRINT 'LEVEL 1 ' + @AccountCode1

            IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
            BEGIN
				SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT, @ColumnDebitEx NUMERIC(20,4) OUTPUT, @ColumnCreditEx NUMERIC(20,4) OUTPUT';
				SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ',
										@ColumnDebitEx = ' + ISNULL(@ColumnDebit, 0) + ',
										@ColumnCreditEx = ' + ISNULL(@ColumnCredit, 0) + '
											 FROM AKT.dbo.TRX_SALDO
											   WHERE COA_CODE=''' + @AccountCode1 + '''';

				PRINT 'PREPARE ' + @AccountCode1
				PRINT @SqlStmt

				EXECUTE sp_executesql @SqlStmt,
									@ParmDefinition,
									@ColumnBeginEx = @ColumnBeginEx OUTPUT,
									@ColumnDebitEx = @ColumnDebitEx OUTPUT,
									@ColumnCreditEx = @ColumnCreditEx OUTPUT;

				SET @newBeginBalance = @ColumnBeginEx + @Value;
				SET @newDebit = @ColumnDebitEx + @DebitValue;
				SET @newCredit = @ColumnCreditEx  + @CreditValue;
				SET @newMutValue = @newDebit - @newCredit;
				SET @newEndBalance = @newBeginBalance + @newMutValue;

				SET @SqlStmt =
						N'UPDATE AKT.dbo.TRX_SALDO SET ' +
						ISNULL(@ColumnBegin, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newBeginBalance, 0)) + ', ' +
						ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newDebit, 0)) + ', ' +
						ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newCredit, 0)) + ', ' + 
						ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newMutValue, 0)) + ', ' + 
						ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@newEndBalance, 0)) + '
								WHERE  COA_CODE =''' + @AccountCode1 + '''';
                
				PRINT 'UPDATE ' + @AccountCode1
				PRINT @SqlStmt

				-- execute update coa query'
				EXECUTE sp_executesql @SqlStmt;
			END
		END
	-- loop coa level
	END
    CLOSE C_COA_LEVEL
	DEALLOCATE C_COA_LEVEL
-- end loop coa
END
CLOSE c__coa_test 
DEALLOCATE c__coa_test
	

SELECT A.COA_CODE, BEGIN_BAL_12, MUT_VALUE_12, END_BAL_12, DB_12, CR_12 FROM AKT.dbo.TRX_SALDO A
INNER JOIN COA_MST B ON A.COA_CODE = B.COA_CODE
WHERE B.COA_ACCUM = '110000000000'

SELECT A.COA_CODE, BEGIN_BAL_12, MUT_VALUE_12, END_BAL_12, DB_12, CR_12 FROM AKT.dbo.TRX_SALDO A
INNER JOIN COA_MST B ON A.COA_CODE = B.COA_CODE
WHERE B.COA_ACCUM = '100000000000'

SELECT COA_CODE, BEGIN_BAL_12, MUT_VALUE_12, END_BAL_12, DB_12, CR_12 FROM AKT.dbo.TRX_SALDO

ROLLBACK TRAN A
