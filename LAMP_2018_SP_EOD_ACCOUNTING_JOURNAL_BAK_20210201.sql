USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[SP_EOD_ACCOUNTING_JOURNAL]    Script Date: 1/29/2021 15:40:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @Success CHAR(1)
--EXEC SP_EOD_ACCOUNTING_JOURNAL '00000000','2016-09-19','TERSERAH', @FlagSuccess = @Success OUTPUT
--SELECT @Success
-- =============================================
-- Author:		<Author,,BI Team>
-- Create date: <5-8-2016,Jakarta>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[SP_EOD_ACCOUNTING_JOURNAL]
--DECLARE 
														  @BranchCode  NVARCHAR(20),
														  @SystemDate  DATE,
														  @UserID      NVARCHAR(24),
														  @FlagSuccess CHAR(1) OUTPUT
AS
--set @BranchCode = '0000'
--set @SystemDate = '2020-07-31'
--set @UserID = 'TEST'

    BEGIN TRY

        SET NOCOUNT ON;
        DECLARE @vMaxSeq_ACC INT, @vCutoffPeriodCollection INT, @TransactionDate DATE;
        SELECT @vMaxSeq_ACC = ISNULL(MAX(SEQUENCE_NO), 0) + 1
        FROM EOD_LOG
        WHERE BRANCH_CODE = @BranchCode
              AND CURRENT_TRANSACTION_DATE = @SystemDate
              AND EOD_JOB_TYPE = 'JOU'
              AND [DESCRIPTION] = 'SP_ACCOUNTING_JOURNAL  ';
        IF @vMaxSeq_ACC = 1
            BEGIN
                INSERT INTO EOD_LOG
                (BRANCH_CODE,
                 CURRENT_TRANSACTION_DATE,
                 SEQUENCE_NO,
                 EOD_JOB_TYPE,
                 [DESCRIPTION],
                 START_TIME,
                 END_TIME,
                 [ERROR_MESSAGE],
                 CREATE_USER,
                 CREATE_DATE,
                 START_DATE,
                 SR_NO
                )
                VALUES
                (@BranchCode,
                 @SystemDate,
                 @vMaxSeq_ACC,
                 'JOU',
                 'SP_ACCOUNTING_JOURNAL',
                 GETDATE(),
                 NULL,
                 NULL,
                 'EOD',
                 GETDATE(),
                 GETDATE(),
                 6
                );
        END;
            ELSE
            BEGIN
                UPDATE EOD_LOG
                  SET
                      END_TIME = NULL,
                      MODIFY_USER = 'EOD',
                      MODIFY_DATE = GETDATE(),
                      SEQUENCE_NO = @vMaxSeq_ACC,
                      SR_NO = 6
                WHERE BRANCH_CODE = @BranchCode
                      AND CURRENT_TRANSACTION_DATE = @SystemDate
                      AND EOD_JOB_TYPE = 'COL'
                      AND SEQUENCE_NO = @vMaxSeq_ACC - 1
                      AND [DESCRIPTION] = 'SP_ACCOUNTING_JOURNAL';
        END;
  
   --READ AND SET TO TEMPC0 (Group Account)

   --print 'MASUK GROUPCODE'

        SELECT CODE,
               TRANSACTION_CODE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE,
               DEPOSIT_ALLOCATION,
               PROCESS_NO
        INTO #TEMP0_EOD
        FROM CF_ACCOUNT_GROUP_MST
        WHERE ACTIVE_STATUS = 1
              AND CODE IN
        (
            SELECT GROUP_CODE
            FROM CF_STANDARD_JOURNAL_MST
        )
        ORDER BY PROCESS_NO ASC; 

		-- SELECT 'GROUP CODE', * FROM #TEMP0_EOD

	-- TDP TEMP21
		SELECT a.CONTRACT_NO,
               a.GOLIVE_DATE,
               a.BRANCH_CODE,
               --a.GOLIVE_TRANSACTION_DATE,
			   b.REPAYMENT_TYPE,
			   b.REPAYMENT_DATE,
			   b.TRANSACTION_CODE,
			   b.TRANSACTION_NO,
			   b.SEQUENCE_NO_HISTORY,
			   b.TRANSACTION_DATE,
			   b.AMOUNT_PAID,
			   b.REVERSE_FLAG
        INTO #TEMP21_EOD
        FROM CF_CONTRACT_INFO A
		JOIN CF_REPAYMENT_HISTORY B
		on A.CONTRACT_NO = B.CONTRACT_NO
        WHERE a.BRANCH_CODE = @BranchCode
              AND a.JOURNAL_EOD_GOLIVE_FLAG = '0'
			  AND b.JOURNAL_EOD_FLAG = '0'
              AND a.GOLIVE_TRANSACTION_DATE <= @SystemDate
			  AND b.REPAYMENT_TYPE = '6'
			  --AND a.CONTRACT_NO = '0100002000051'
		
			  --select  'TDP',* from #TEMP21_EOD
	
	--READ AND SET TO TEMPC1  (AR Realization)

        SELECT a.CONTRACT_NO,
               a.GOLIVE_DATE,
               a.BRANCH_CODE,
               a.GOLIVE_TRANSACTION_DATE
			   --,b.REPAYMENT_TYPE
        INTO #TEMP1_EOD
        FROM CF_CONTRACT_INFO A
		--JOIN CF_REPAYMENT_HISTORY B
		--on A.CONTRACT_NO = B.CONTRACT_NO
        WHERE a.BRANCH_CODE = @BranchCode
              AND a.JOURNAL_EOD_GOLIVE_FLAG = '0'
              AND a.GOLIVE_TRANSACTION_DATE <= @SystemDate
			  --AND a.CONTRACT_NO = '0200002000015'
			  --AND b.REPAYMENT_TYPE <> '6'

	--select 'AR Realization',* from #TEMP1_EOD

	--READ AND SET TO TEMPC2  --- Payment Management (Payment Input)

        SELECT A.CONTRACT_NO,
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP2_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '1'
			  --AND A.CONTRACT_NO = '0100002000051'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

	--select 'Pay Mgmt ', * from #TEMP2_EOD 

	--READ AND SET TO TEMPC3  --- Repayment INSTAL

        SELECT CONTRACT_NO,
               TRANSACTION_NO,
               TRANSACTION_CODE,
               REPAYMENT_DATE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE_CODE,
               REPAYMENT_CHANNEL_CODE,
               AMOUNT_PAID,
               SEQUENCE_NO_HISTORY,
               TRANSACTION_DATE
        INTO #TEMP3_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0'
			  AND REPAYMENT_TYPE = '1'
			  AND REPAYMENT_SOURCE_CODE <> 'UNK'
			  AND REVERSE_FLAG <> '1'
			  --AND CONTRACT_NO = '0100002000037'

       --select 'Repayment', * from #TEMP3_EOD

	--READ AND SET TO TEMPC4 (Close AR)

        SELECT CONTRACT_NO,
               CLOSED_AR_DATE,
               CLOSED_AR_TRANSACTION_DATE
        INTO #TEMP4_EOD
        FROM CF_CONTRACT_INFO
        WHERE BRANCH_CODE = @BranchCode
              AND CLOSED_AR_TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_WRITEOFF_FLAG = '0';

	--READ AND SET TO TEMPC5 (Reschedule)

        SELECT CONTRACT_NO,
               TRANSACTION_NO,
               CONTRACT_UPDATE_DATE,
               TRANSACTION_DATE
        INTO #TEMP5_EOD
        FROM CF_CONTRACT_UPDATE_HEADER
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND CONTRACT_UPDATE_STATUS = '5' --- (Close)
              AND JOURNAL_EOD_FLAG = '0'
              AND ISNULL(NEW_RECEIVABLES, 0) > 0;

	--READ AND SET TO TEMPC6 (Unknown Repayment)

        SELECT REFERENCE_NO,
               [NO],
               PAYMENT_DATE,
               TRANSACTION_DATE
        INTO #TEMP6_EOD
        FROM CF_UNKNOWN_REPAYMENT
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND ISNULL(JOURNAL_EOD_FLAG,0) = '0'
			  AND VALID_CONTRACT_NO IS NULL
			  AND STATUS = '1'

		--select 'UN-KNW',* from #TEMP6_EOD

	--READ AND SET TO TEMPC7 (Cancel AR Realization)

        SELECT CONTRACT_NO,
               CANCEL_DATE,
               CANCEL_TRANSACTION_DATE
        INTO #TEMP7_EOD
        FROM CF_CONTRACT_INFO
        WHERE BRANCH_CODE = @BranchCode
              AND CANCEL_TRANSACTION_DATE <= @SystemDate
              AND GOLIVE_TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_CANCEL_FLAG = '0';

	--READ AND SET TO TEMPC8 (Cancel Payment Management)

        SELECT CONTRACT_NO,
               PAYMENT_ID,
               PAYMENT_TYPE,
               REAL_TRANSACTION_DATE,
               TRANSACTION_DATE
        INTO #TEMP8_EOD
        FROM CF_PAY_STAGING
        WHERE BRANCH_CODE = @BranchCode
              AND REAL_TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0'
              AND OPERATION_TYPE = '3' --- Cancel Payment
			  AND PAYMENT_TYPE = '1'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;
   
   --READ AND SET TO TEMPC9 (Handover)

        SELECT BRANCH_CODE,
               OUTLET_CODE,
               BATCH_TRANSACTION_DATE,
               REFERENCE_NO,
               USER_ID,
               BATCH_NO
        INTO #TEMP9_EOD
        FROM CF_CASHIER_BALANCE_DETAIL
        WHERE BRANCH_CODE = @BranchCode
              AND BATCH_TRANSACTION_DATE <= @SystemDate
              AND STATUS = '3'
              AND TRANSFER_TO = '1'
              AND JOURNAL_EOD_FLAG = '0';	
	
  --READ AND SET TO TEMPC10 (Pengakuan Interest)

        SELECT CONTRACT_NO,
               ACCOUNTING_UNREALIZED_INCOME,
               ACCOUNTING_DATE,
               BRANCH_CODE
        INTO #TEMP10_EOD
        FROM CF_UNEARNED_ACCOUNTING
        WHERE ACCOUNTING_DATE = @SystemDate
              AND BRANCH_CODE = @BranchCode
              and JOURNAL_EOD_FLAG='0';
  
  --READ AND SET TO TEMPC13 (Jurnal balik Pengakuan Interest)

        SELECT CONTRACT_NO,
               ACCOUNTING_UNREALIZED_INCOME,
               ACCOUNTING_DATE,
               BRANCH_CODE
        INTO #TEMP13_EOD
        FROM CF_UNEARNED_ACCOUNTING
        WHERE ACCOUNTING_DATE = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @SystemDate), 0))
		--EOMONTH(@SystemDate, -1)
              AND BRANCH_CODE = @BranchCode
              and JOURNAL_EOD_REVERSE_FLAG='0';

  --READ AND SET TO TEMPC11 (Cancel Handover ) 

        SELECT BRANCH_CODE,
               OUTLET_CODE,
               BATCH_DATE,
               CANCEL_DATE,
               RECEIVE_MONEY_FROM,
               REFERENCE_NO
        INTO #TEMP11_EOD
        FROM CF_CASHIER_RECEIVED      
        WHERE BRANCH_CODE = @BranchCode
              AND CANCEL_DATE <= @SystemDate
              AND JOURNAL_EOD_CANCEL_FLAG = '0'
              AND BRANCH_CODE = @BranchCode;
              
  --READ AND SET TO TEMPC12 (Cashier Received) 

        SELECT BRANCH_CODE,
               OUTLET_CODE,
               RECEIVED_DATE,
               REFERENCE_NO,
               RECEIVED_STATUS
        INTO #TEMP12_EOD
        FROM CF_CASHIER_RECEIVED
        WHERE BRANCH_CODE = @BranchCode
              AND RECEIVED_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0';
 
	--READ AND SET TO TEMPC14  --- Payment Management (Fiducia Input)

        SELECT 
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP14_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '3'
			  --AND A.PAYMENT_ID = '00002000050'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

		--select 'AP-FID', * from #TEMP14_EOD 
 
	--READ AND SET TO TEMPC15  --- Payment Management (Insurance Input)

        SELECT 
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP15_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '2'
			  --AND A.PAYMENT_ID = '00002000048'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

		--select 'AP-INS', * from #TEMP15_EOD

		--READ AND SET TO TEMPC16  --- Payment Management (Other Dealer, Non Delaer)
	   SELECT A.CONTRACT_NO,
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP16_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '5'
			  --AND A.CONTRACT_NO = '0100002000027'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

		--select 'AP-NDLR', * from #TEMP16_EOD
  
  	--READ AND SET TO TEMPC17  --- Payment Management (refund customer)
	   SELECT A.CONTRACT_NO,
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP17_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '6'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

		--select 'AP-RFC', * from #TEMP17_EOD

	 --READ AND SET TO TEMPC18  --- Payment Management (endors)
	   SELECT A.CONTRACT_NO,
               A.PAYMENT_ID,
               A.PAYMENT_APPLICATION_NO,
               A.PAYMENT_TYPE,
               A.REAL_TRANSACTION_DATE,
               A.TRANSACTION_DATE
        INTO #TEMP18_EOD
        FROM CF_PAY_STAGING A
        WHERE A.BRANCH_CODE = @BranchCode
              AND A.TRANSACTION_DATE <= @SystemDate
              AND A.OPERATION_TYPE = '2'
              AND A.JOURNAL_EOD_FLAG = '0'
			  AND A.PAYMENT_TYPE = '4'
        ORDER BY PAYMENT_TYPE,
                 CONTRACT_NO;

		--select 'AP-END', * from #TEMP18_EOD

		--READ AND SET TO TEMPC19
		SELECT 
			CONTRACT_NO,
			CAST(TERMINATION_DATE AS DATE) AS TERMINATION_DATE,
			REPAYMENT_TRANSACTION_NO,
			TRANSACTION_DATE
		INTO #TEMP19_EOD
		FROM CF_TERMINATION_HEADER
		WHERE [STATUS] = 'CL'
		  AND BRANCH_CODE = @BranchCode
		  AND TRANSACTION_DATE <= @SystemDate
		  AND JOURNAL_EOD_FLAG = '0'
		--  AND CONTRACT_NO = '0100002000058'
		ORDER BY CONTRACT_NO ASC;
    
		--select 'ET-TERMI', * from #TEMP19_EOD
    
    -- CASH INDOOR TEMP20
    SELECT CONTRACT_NO,
         TRANSACTION_NO,
         TRANSACTION_CODE,
         REPAYMENT_DATE,
         REVERSE_FLAG,
         REPAYMENT_SOURCE_CODE,
         REPAYMENT_CHANNEL_CODE,
         AMOUNT_PAID,
         SEQUENCE_NO_HISTORY,
         TRANSACTION_DATE
        INTO #TEMP20_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND REPAYMENT_CHANNEL_CODE = 'CAS'
              AND JOURNAL_EOD_FLAG = '0'

	-- Allocated Unknown

		SELECT REFERENCE_NO,
               [NO],
               PAYMENT_DATE,
               TRANSACTION_DATE,
			   VALID_CONTRACT_NO
        INTO #TEMP22_EOD
        FROM CF_UNKNOWN_REPAYMENT
		WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND ISNULL(JOURNAL_EOD_FLAG,0) = '0'
			  AND STATUS = '4'

		--select 'AL-UNK ',* from #TEMP22_EOD

	-- Reverse Repayment

        SELECT CONTRACT_NO,
               TRANSACTION_NO,
               TRANSACTION_CODE,
               REPAYMENT_DATE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE_CODE,
               REPAYMENT_CHANNEL_CODE,
               AMOUNT_PAID,
               SEQUENCE_NO_HISTORY,
               TRANSACTION_DATE,
			   CREATE_DATE
        INTO #TEMP23_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
			  --AND CONTRACT_NO = '0100002000037'
              AND TRANSACTION_DATE <= @SystemDate
              AND ISNULL(JOURNAL_EOD_FLAG,'0') = '0'
			  AND REPAYMENT_TYPE = '1'
			  AND REPAYMENT_SOURCE_CODE <> 'UNK'
			  AND REVERSE_FLAG = '1'
			  --AND REVERSE_REASON IS NOT NULL
			 

		--select 'REV', * from #TEMP23_EOD 


	--  Repayment DEPOST

        SELECT CONTRACT_NO,
               TRANSACTION_NO,
               TRANSACTION_CODE,
               REPAYMENT_DATE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE_CODE,
               REPAYMENT_CHANNEL_CODE,
               AMOUNT_PAID,
               SEQUENCE_NO_HISTORY,
               TRANSACTION_DATE
        INTO #TEMP24_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0'
			  AND REPAYMENT_TYPE = '5'
			  AND REPAYMENT_SOURCE_CODE <> 'UNK'
			  AND REVERSE_FLAG <> '1'
			  AND TRANSACTION_CODE = 'T003'
			  --AND CONTRACT_NO = '0300002000005'

       --select 'DEPOST', * from #TEMP24_EOD


	--  Repayment PENALT

        SELECT CONTRACT_NO,
               TRANSACTION_NO,
               TRANSACTION_CODE,
               REPAYMENT_DATE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE_CODE,
               REPAYMENT_CHANNEL_CODE,
               AMOUNT_PAID,
               SEQUENCE_NO_HISTORY,
               TRANSACTION_DATE
        INTO #TEMP25_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0'
			  AND REPAYMENT_TYPE = '5'
			  AND REPAYMENT_SOURCE_CODE <> 'UNK'
			  AND REVERSE_FLAG <> '1'
			  AND TRANSACTION_CODE = 'T002'
			  --AND CONTRACT_NO = '0300002000005'

       --select 'PENALT', * from #TEMP25_EOD


		-- Acrual

		SELECT	A.CONTRACT_NO, 
				A.TOTAL_ACRUAL, 
				A.PAY_STATUS, 
				A.DUE_DATE, 
				A.INSTALLMENT_NO, 
				A.PAID_DATE, 
				B.CURRENT_TRANSACTION_DATE
		INTO #TEMP26_EOD
		FROM CF_CONTRACT_PAYMENT_SCHEDULE A
		JOIN BRANCH_EOD_MST B on A.BRANCH_CODE = B.BRANCH_CODE
		WHERE MONTH(a.DUE_DATE) <= MONTH(@SystemDate) 
		AND YEAR(a.DUE_DATE) <= YEAR(@SystemDate)
		AND A.BRANCH_CODE = @BranchCode
		AND A.PAY_STATUS = '3'
		AND A.INSTALLMENT_NO <> '0'
		AND A.TOTAL_ACRUAL > 0
		AND ISNULL(A.EOM_FLAG,'0') = '0'
        AND MONTH(a.PAID_DATE) <= MONTH(A.DUE_DATE) 
		AND YEAR(a.PAID_DATE) <= YEAR(A.DUE_DATE)
		--AND A.CONTRACT_NO = '0100002000020'

		--select 'Acrual ', * from #TEMP26_EOD

	--  Repayment INSTALM (2020/11/16)
	
      SELECT CONTRACT_NO,
               TRANSACTION_NO,
               TRANSACTION_CODE,
               REPAYMENT_DATE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE_CODE,
               REPAYMENT_CHANNEL_CODE,
               AMOUNT_PAID,
               SEQUENCE_NO_HISTORY,
               TRANSACTION_DATE
        INTO #TEMP27_EOD
        FROM CF_REPAYMENT_HISTORY
        WHERE BRANCH_CODE = @BranchCode
              AND TRANSACTION_DATE <= @SystemDate
              AND JOURNAL_EOD_FLAG = '0'
			  AND REPAYMENT_TYPE = '5'
			  AND REPAYMENT_SOURCE_CODE <> 'UNK'
			  AND REVERSE_FLAG <> '1'
			  AND TRANSACTION_CODE = 'T001'
			  --AND CONTRACT_NO = '21905089'

       --select 'INSTALM', * from #TEMP27_EOD

	   -- group code PDREC

        SELECT	A.CONTRACT_NO, 
				A.INSTALLMENT_AMT, 
				A.PAY_STATUS, 
				A.DUE_DATE, 
				A.INSTALLMENT_NO, 
				A.PAID_DATE, 
				B.CURRENT_TRANSACTION_DATE
		INTO #TEMP28_EOD
		FROM CF_CONTRACT_PAYMENT_SCHEDULE A
		JOIN BRANCH_EOD_MST B on A.BRANCH_CODE = B.BRANCH_CODE
		WHERE 
		MONTH(a.DUE_DATE) = MONTH(@SystemDate) 
        AND YEAR(a.DUE_DATE) = YEAR(@SystemDate)
        AND A.BRANCH_CODE = @BranchCode
        AND A.INSTALLMENT_NO <> '0'
        AND A.PAY_STATUS = '0' 
        OR FORMAT(A.DUE_DATE, 'yyyyMM') < FORMAT(A.PAID_DATE, 'yyyyMM') 
        AND MONTH(a.DUE_DATE) = MONTH(@SystemDate) 
        AND YEAR(a.DUE_DATE) = YEAR(@SystemDate)
        AND A.BRANCH_CODE = @BranchCode
        AND A.INSTALLMENT_NO <> '0'

        -- SELECT 'TEMP 28', * FROM #TEMP28_EOD;
		
        -- group code RC-TERMI

        SELECT 
			CONTRACT_NO,
			CAST(TERMINATION_DATE AS DATE) AS TERMINATION_DATE,
			REPAYMENT_TRANSACTION_NO,
			TRANSACTION_DATE
		INTO #TEMP29_EOD
		FROM CF_TERMINATION_HEADER
		WHERE [STATUS] = 'CL'
		  AND BRANCH_CODE = @BranchCode
		  AND TRANSACTION_DATE <= @SystemDate
		  AND JOURNAL_EOD_FLAG = '1'
		ORDER BY CONTRACT_NO ASC;

   --To Get EOM Period, Just for Unearn

        SELECT @vCutoffPeriodCollection = CONVERT(INT, [VALUE])
        FROM GLOBAL_PARAMETER_MST
        WHERE CONDITION = 'CUTOFF_COLLECTION'
              AND ACTIVE_STATUS = '1';
        DECLARE @RowCount INT;
        DECLARE @RowCount1 INT;
        DECLARE @RowCount2 INT;
        DECLARE @RowCount3 INT;
        DECLARE @RowCount4 INT;
        DECLARE @RowCount5 INT;
        DECLARE @RowCount6 INT;
        DECLARE @RowCount7 INT;
        DECLARE @RowCount8 INT;
        DECLARE @RowCount9 INT;
        DECLARE @RowCount10 INT;
        DECLARE @RowCount11 INT;
        DECLARE @RowCount12 INT;
        DECLARE @RowCount13 INT;
		DECLARE @RowCount16 INT;
		DECLARE @RowCount17 INT;
		DECLARE @RowCount18 INT;
		DECLARE @RowCount19 INT;
    DECLARE @RowCount20 INT;
	DECLARE @RowCount21 INT;
	DECLARE @RowCount22 INT;
	DECLARE @RowCount23 INT;
	DECLARE @RowCount24 INT;
	DECLARE @RowCount25 INT;
	DECLARE @RowCount26 INT;
  DECLARE @RowCount27 INT;
  DECLARE @RowCount28 INT;
  DECLARE @RowCount29 INT;
        DECLARE @GroupCode NVARCHAR(12);
        DECLARE @TransactionCode NVARCHAR(10);
        DECLARE @ReverseFlag NCHAR(2);
        DECLARE @RepaymentSource NCHAR(10);
        DECLARE @DepositAllocation NCHAR(2);
        DECLARE @ValueDate DATE;
        DECLARE @ContractNo NVARCHAR(30);
        DECLARE @TransactionNo NVARCHAR(40);
        DECLARE @Success CHAR(1);
        DECLARE @SeqNoHist NUMERIC(6);
        DECLARE @BatchNo NUMERIC(3);
        DECLARE @UserHandover NVARCHAR(12);
        DECLARE @ReferenceNo NVARCHAR(20);

	--FOR CURSOR0

        SELECT CODE,
               TRANSACTION_CODE,
               REVERSE_FLAG,
               REPAYMENT_SOURCE,
               DEPOSIT_ALLOCATION,
               PROCESS_NO
        INTO #Cursor0
        FROM #TEMP0_EOD
        ORDER BY PROCESS_NO ASC;

		--select  'Cursor 0 ',* from #Cursor0

        SELECT TOP 1 @GroupCode = CODE,
                     @TransactionCode = ISNULL(TRANSACTION_CODE, ''),
                     @ReverseFlag = ISNULL(REVERSE_FLAG, '0'),
                     @RepaymentSource = ISNULL(REPAYMENT_SOURCE, ''),
                     @DepositAllocation = ISNULL(DEPOSIT_ALLOCATION, '0')
        FROM #Cursor0
        ORDER BY PROCESS_NO ASC;
        SET @RowCount =
        (
            SELECT COUNT(0)
            FROM #Cursor0
        );
        SET @SeqNoHist = 0; 

		--print @rowcount
	
	--LOGIC--
        WHILE @RowCount <> 0
            BEGIN  
				print @GroupCode
				IF(@GroupCode = 'TDP'
                   AND ISNULL(@TransactionCode, '') = ''
				   )
                    BEGIN
                        SET @RowCount21 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP21_EOD
                        );
                        WHILE @RowCOUNT21 <> 0
                            BEGIN
                                SELECT TOP 1 
                                             @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
											 @ValueDate = REPAYMENT_DATE,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP21_EOD;
		
								--select top 1 * from #TEMP21_EOD
		
								
				                
				--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
		
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
		
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;

								UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                               -- UPDATE CF_CONTRACT_INFO
                               --   SET
                               --       JOURNAL_EOD_GOLIVE_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                               -- WHERE CONTRACT_NO = @ContractNo;
                                DELETE FROM #TEMP21_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @ValueDate = REPAYMENT_DATE
                                FROM #TEMP21_EOD;
		
                                SET @RowCount21 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP21_EOD
                                );
                END;
                END
				ELSE




			--LOOP C1 
                IF(@GroupCode = 'GOLIVE'
                   AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount1 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP1_EOD
                        );
                        WHILE @RowCOUNT1 <> 0
                            BEGIN
                                SELECT TOP 1 @ValueDate = GOLIVE_DATE,
                                             @ContractNo = CONTRACT_NO,
                                             @TransactionNo = CONTRACT_NO,
                                             @TransactionDate = GOLIVE_TRANSACTION_DATE
                                FROM #TEMP1_EOD;
				                
				--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;

                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;

                                UPDATE CF_CONTRACT_INFO
                                  SET
                                      JOURNAL_EOD_GOLIVE_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo;
                                DELETE FROM #TEMP1_EOD
                                WHERE GOLIVE_DATE = @ValueDate
                                      AND CONTRACT_NO = @ContractNo;
                                SELECT TOP 1 @ValueDate = GOLIVE_DATE,
                                             @ContractNo = CONTRACT_NO,
                                             @TransactionNo = CONTRACT_NO
                                FROM #TEMP1_EOD;
                                SET @RowCount1 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP1_EOD
                                );
                END;
                END

			
			--LOOP C2;
                    ELSE
                IF(@GroupCode IN('AP-DLR') --, 'AP-INS', 'AP-FID')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount2 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP2_EOD
                        );
                        WHILE(@RowCount2 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP2_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP2_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP2_EOD;
                                SET @RowCount2 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP2_EOD
                                );
                END;
                END
			--LOOP C3;
                    ELSE
                IF(@GroupCode in ('INSTAL') AND ISNULL(@TransactionCode, '') <> '')
                    BEGIN
                        SET @RowCount3 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP3_EOD
                            WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                  AND (AMOUNT_PAID > 0
                                       AND isnull(@DepositAllocation, '0') = '0'
									   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND ISNULL(@DepositAllocation, '0') = '1'
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                        );
		
                        WHILE @RowCount3 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP3_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
										   AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      --AND TRANSACTION_CODE = @TransactionCode
                                      --AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                DELETE FROM #TEMP3_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      --AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP3_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
                                           AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
                                SET @RowCount3 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP3_EOD
                                    WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                          AND (AMOUNT_PAID > 0
                                               AND isnull(@DepositAllocation, '0') = '0'
											   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND ISNULL(@DepositAllocation, '0') = '1'
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                                );
                END;
                END

				--LOOP C4;
                    ELSE
                IF(@GroupCode IN('CLOSE-AR', 'WO')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount4 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP4_EOD
                        );
                        WHILE @RowCount4 <> 0
                            BEGIN
                                SELECT TOP 1 @ValueDate = CLOSED_AR_DATE,
                                             @ContractNo = CONTRACT_NO,
                                             @TransactionDate = CLOSED_AR_TRANSACTION_DATE
                                FROM #TEMP4_EOD;
                                SET @TransactionNo = @ContractNo;
				
					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CONTRACT_INFO
                                  SET
                                      JOURNAL_EOD_WRITEOFF_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo;
                                DELETE FROM #TEMP4_EOD
                                WHERE CONTRACT_NO = @ContractNo;
                                SELECT TOP 1 @ValueDate = CLOSED_AR_DATE,
                                             @ContractNo = CONTRACT_NO
                                FROM #TEMP4_EOD;
                                SET @RowCount4 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP4_EOD
                                );
                END;
                END
			--LOOP C5;
                    ELSE
                IF(@GroupCode = 'RESCHE'
                   AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount5 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP5_EOD
                        );
                        WHILE @RowCount5 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @ValueDate = CONTRACT_UPDATE_DATE,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP5_EOD;
          
								
				--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CONTRACT_UPDATE_HEADER
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo;
                                DELETE FROM #TEMP5_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      AND TRANSACTION_DATE = @TransactionDate;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @ValueDate = CONTRACT_UPDATE_DATE
                                FROM #TEMP5_EOD;
                                SET @RowCOUNT5 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP5_EOD
                                );
                END;
                END
			--LOOP C6;
                    ELSE
                IF(@GroupCode = 'UN-KNW'
                   AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount6 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP6_EOD
                        );
                        WHILE @RowCount6 <> 0
                            BEGIN
                                SELECT TOP 1 @ValueDate = PAYMENT_DATE,
                                             @ContractNo = '',
                                             @TransactionNo = [NO],
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP6_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_UNKNOWN_REPAYMENT
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE [NO] = @TransactionNo;
                                DELETE FROM #TEMP6_EOD
                                WHERE PAYMENT_DATE = @ValueDate
                                      AND [NO] = @TransactionNo;
                                SELECT TOP 1 @ValueDate = PAYMENT_DATE,
                                             @ContractNo = '',
                                             @TransactionNo = [NO]
                                FROM #TEMP6_EOD;
                                SET @RowCount6 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP6_EOD
                                );
                END;
                END
			--LOOP C7;
                    ELSE
                IF(@GroupCode = 'CAN-GL'
                   AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount7 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP7_EOD
                        );
                        WHILE @RowCount7 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = CANCEL_DATE,
                                             @TransactionDate = CANCEL_TRANSACTION_DATE
                                FROM #TEMP7_EOD;
                                SET @TransactionNo = @ContractNo;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactioNNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CONTRACT_INFO
                                  SET
                                      JOURNAL_EOD_CANCEL_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo;
                                DELETE FROM #TEMP7_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND CANCEL_DATE = @ValueDate;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = CANCEL_DATE
                                FROM #TEMP7_EOD;
                                SET @RowCount7 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP7_EOD
                                );
                END;
                END;
                    ELSE
                IF(@GroupCode IN('REVAP-DLR', 'REVAP-INS', 'REVAP-FID')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount8 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP8_EOD
                        );
                        WHILE(@RowCount8 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP8_EOD;
      
					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP8_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP8_EOD;
                                SET @RowCount8 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP8_EOD
                                );
                END;
                END;
                    ELSE
                IF(@GroupCode IN('HANOVR')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount9 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP9_EOD
                        );
                        WHILE(@RowCount9 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = BATCH_TRANSACTION_DATE,
                                             @UserHandover = USER_ID,
                                             @BatchNo = BATCH_NO,
                                             @BranchCode = BRANCH_CODE,
                                             @ReferenceNo = REFERENCE_NO,
                                             @TransactionDate = BATCH_TRANSACTION_DATE
                                FROM #TEMP9_EOD;
                                SET @TransactionNo = @ReferenceNo;
				
			--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CASHIER_BALANCE_DETAIL
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE BRANCH_CODE = @BranchCode
                                      AND BATCH_TRANSACTION_DATE = @ValueDate
                                      AND REFERENCE_NO = @TransactionNo
                                      AND USER_ID = @UserHandover
                                      AND BATCH_NO = @BatchNo
                                      AND STATUS = '3'
                                      AND TRANSFER_TO = '1';
                                DELETE FROM #TEMP9_EOD
                                WHERE BRANCH_CODE = @BranchCode
                                      AND BATCH_TRANSACTION_DATE = @ValueDate
                                      AND USER_ID = @UserHandover
                                      AND BATCH_NO = @BatchNo;
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = BATCH_TRANSACTION_DATE,
                                             @UserHandover = USER_ID,
                                             @BatchNo = BATCH_NO,
                                             @BranchCode = BRANCH_CODE
                                FROM #TEMP9_EOD;
                                SET @RowCount9 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP9_EOD
                                );
                END;
                END;	
                    ELSE
                IF(@GroupCode IN('UNEARN')
                AND ISNULL(@TransactionCode, '') = ''
                AND @SystemDate = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @SystemDate) + 1, 0)))
				--EOMONTH(@SystemDate))
                    BEGIN
                        SET @RowCount10 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP10_EOD
                        );
                        WHILE(@RowCount10 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = ACCOUNTING_DATE,
                                             @TransactionDate = ACCOUNTING_DATE,
											 @BranchCode = BRANCH_CODE
                                FROM #TEMP10_EOD;
                                SET @TransactionNo = @ContractNo;
				
			--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_UNEARNED_ACCOUNTING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND ACCOUNTING_DATE = @ValueDate
                                      AND BRANCH_CODE = @BranchCode;
                                DELETE FROM #TEMP10_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND ACCOUNTING_DATE = @ValueDate
                                      AND BRANCH_CODE = @BranchCode;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = ACCOUNTING_DATE,
											 @TransactionDate = ACCOUNTING_DATE,
											 @BranchCode = BRANCH_CODE
                                FROM #TEMP10_EOD;
                                SET @RowCount10 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP10_EOD
                                );
                END;
                END;
                    ELSE
                IF(@GroupCode IN('EARN')
                AND ISNULL(@TransactionCode, '') = ''
                AND @SystemDate = DATEADD(day, 1, DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, GETDATE()), 0))))
				--AND @SystemDate = DATEADD(day, 1, EOMONTH(@SystemDate, -1)))
                    BEGIN
                        SET @RowCount13 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP13_EOD
                        );
                        WHILE(@RowCount13 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = ACCOUNTING_DATE,
                                             --@TransactionDate = ACCOUNTING_DATE,
                                             @BranchCode = BRANCH_CODE
                                FROM #TEMP13_EOD;
                                SET @TransactionNo = @ContractNo;
								SET @TransactionDate = @SystemDate;	

			--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_UNEARNED_ACCOUNTING
                                  SET
                                      JOURNAL_EOD_REVERSE_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  --IIF(@Success = 'Y', 1, 0)
                                WHERE CONTRACT_NO = @ContractNo
                                      AND ACCOUNTING_DATE = @ValueDate;
                                DELETE FROM #TEMP13_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND ACCOUNTING_DATE = @ValueDate;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = ACCOUNTING_DATE,
											 --@TransactionDate = ACCOUNTING_DATE,
                                             @BranchCode = BRANCH_CODE
                                FROM #TEMP13_EOD;
                                SET @RowCount13 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP13_EOD
                                );
                END;
                END	
		---Cancel HandOver;
                    ELSE
                IF(@GroupCode IN('CHANOVR')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount11 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP11_EOD
                        );
                        WHILE(@RowCount11 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = CANCEL_DATE,
                                             @ReferenceNo = REFERENCE_NO,
                                             @TransactionDate = CANCEL_DATE
                                FROM #TEMP11_EOD;
                                SET @TransactionNo = @ReferenceNo;
				
			--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CASHIER_RECEIVED
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  --IIF(@Success = 'Y', 1, 0)
                                WHERE BRANCH_CODE = @BranchCode
                                      AND CANCEL_DATE = @ValueDate
                                      AND REFERENCE_NO = @ReferenceNo;
                                DELETE FROM #TEMP11_EOD
                                WHERE BRANCH_CODE = @BranchCode
                                      AND CANCEL_DATE = @ValueDate
                                      AND REFERENCE_NO = @ReferenceNo;
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = CANCEL_DATE,
                                             @BranchCode = BRANCH_CODE
                                FROM #TEMP11_EOD;
                                SET @RowCount11 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP11_EOD
                                );
                END;
                END

		----- Received Money;
                    ELSE
                IF(@GroupCode IN('RECEIVE')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount12 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP12_EOD
                        );
                        WHILE(@RowCount12 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = RECEIVED_DATE,
                                             @ReferenceNo = REFERENCE_NO,
                                             @TransactionDate = RECEIVED_DATE
                                FROM #TEMP12_EOD;
                                SET @TransactionNo = @ReferenceNo;
				
			--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CASHIER_RECEIVED
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  --IIF(@Success = 'Y', 1, 0)
                                WHERE BRANCH_CODE = @BranchCode
                                      AND RECEIVED_DATE = @ValueDate
                                      AND REFERENCE_NO = @ReferenceNo;
                                DELETE FROM #TEMP12_EOD
                                WHERE BRANCH_CODE = @BranchCode
                                      AND RECEIVED_DATE = @ValueDate
                                      AND REFERENCE_NO = @ReferenceNo;
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = RECEIVED_DATE,
                                             @BranchCode = BRANCH_CODE
                                FROM #TEMP12_EOD;
                                SET @RowCount12 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP12_EOD
                                );
                END;
                END;

				--add by yoga


			
			--LOOP C15;
                    ELSE
                IF(@GroupCode IN('AP-INS')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount2 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP15_EOD
                        );
                        WHILE(@RowCount2 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP15_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  --IIF(@Success = 'Y', 1, 0)
                                WHERE PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP15_EOD
                                WHERE REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP15_EOD;
                                SET @RowCount2 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP15_EOD
                                );
                END;
                END

				
			--LOOP C14;
                    ELSE
                IF(@GroupCode IN('AP-FID')
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount2 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP14_EOD
                        );
                        WHILE(@RowCount2 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = '',
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP14_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  --IIF(@Success = 'Y', 1, 0)
                                WHERE PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP14_EOD
                                WHERE REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP14_EOD;
                                SET @RowCount2 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP14_EOD
                                );
                END;
                END

				--end add by yoga
								 ELSE
				 -- add by haidir
                IF(@GroupCode IN('AP-NDLR')--,'AP-OTD') 
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount16 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP16_EOD
                        );
                        WHILE(@RowCount16 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP16_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP16_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP16_EOD;
                                SET @RowCount16 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP16_EOD
                                );
                END;
                END

				ELSE
                IF(@GroupCode IN('AP-RFC') 
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount17 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP17_EOD
                        );
                        WHILE(@RowCount17 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP17_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP17_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP17_EOD;
                                SET @RowCount17 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP17_EOD
                                );
                END;
                END
				ELSE
                IF(@GroupCode IN('AP-END') 
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount18 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP18_EOD
                        );
                        WHILE(@RowCount18 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP18_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_PAY_STAGING
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND PAYMENT_ID = @TransactionNo;
                                DELETE FROM #TEMP18_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REAL_TRANSACTION_DATE = @ValueDate
                                      AND PAYMENT_ID = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = REAL_TRANSACTION_DATE,
                                             @TransactionNo = PAYMENT_ID
                                FROM #TEMP18_EOD;
                                SET @RowCount18 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP18_EOD
                                );
                END;
                END
				-- end add by haidir
				ELSE
				IF(@GroupCode IN('ET-TERMI') 
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount19 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP19_EOD
                        );
                        WHILE(@RowCount19 <> 0)
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = TERMINATION_DATE,
                                             @TransactionNo = REPAYMENT_TRANSACTION_NO,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP19_EOD;

								IF @TransactionNo IS NULL
								BEGIN
									BREAK;
								END

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_TERMINATION_HEADER
                                SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y' THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REPAYMENT_TRANSACTION_NO = @TransactionNo;
                                DELETE FROM #TEMP19_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND CAST(TERMINATION_DATE AS DATE) = @ValueDate
                                      AND REPAYMENT_TRANSACTION_NO = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = TERMINATION_DATE,
                                             @TransactionNo = REPAYMENT_TRANSACTION_NO
                                FROM #TEMP19_EOD;
                                SET @RowCount19 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP19_EOD
                                );
                END;
                END
                    ELSE
                IF(@GroupCode IN('CASHIN') 
                AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount20 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP20_EOD
                            WHERE 
                            --ISNULL(TRANSACTION_CODE, '') = @TransactionCode
                                  --AND 
                                  (AMOUNT_PAID > 0
                                       AND isnull(@DepositAllocation, '0') = '0'
									   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND ISNULL(@DepositAllocation, '0') = '1'
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                        );
                        WHILE @RowCount20 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP20_EOD
                                WHERE 
                                      (AMOUNT_PAID > 0
										   AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;

                                UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      --AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                DELETE FROM #TEMP20_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      --AND TRANSACTION_NO = @TransactionNo
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP20_EOD
                                WHERE 
                                      (AMOUNT_PAID > 0
                                           AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
                                SET @RowCount20 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP20_EOD
                                    WHERE 
                                          (AMOUNT_PAID > 0
                                               AND isnull(@DepositAllocation, '0') = '0'
											   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND ISNULL(@DepositAllocation, '0') = '1'
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                                );
                END;
                END
				
				--Loop AL-UNK
				  ELSE
				 
                IF(@GroupCode = 'AL-UNK'
			    AND ISNULL(@TransactionCode, '') = '')
                    BEGIN
                        SET @RowCount22 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP22_EOD
                        );
                        WHILE @RowCount22 <> 0
                            BEGIN
                                SELECT TOP 1 @ValueDate = PAYMENT_DATE,
                                             @ContractNo = VALID_CONTRACT_NO,
                                             @TransactionNo = [NO],
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP22_EOD;

					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_UNKNOWN_REPAYMENT
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE [NO] = @TransactionNo
								AND VALID_CONTRACT_NO = @ContractNo;
								UPDATE CF_REPAYMENT_HISTORY
								SET JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
								WHERE CONTRACT_NO = @ContractNo
								AND REPAYMENT_SOURCE_CODE = 'UNK'
                                DELETE FROM #TEMP22_EOD
                                WHERE PAYMENT_DATE = @ValueDate
                                      AND [NO] = @TransactionNo
									  AND VALID_CONTRACT_NO = @ContractNo
                                SELECT TOP 1 @ValueDate = PAYMENT_DATE,
                                             @ContractNo = VALID_CONTRACT_NO,
                                             @TransactionNo = [NO]
                                FROM #TEMP22_EOD;
                                SET @RowCount22 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP22_EOD
                                );
                END;
                END
				ELSE
				
				 IF(@GroupCode = 'REV-INSTAL'
                   AND ISNULL(@TransactionCode, '') <> ''
				   )
                    BEGIN
                        SET @RowCount23 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP23_EOD
                        );
                        WHILE @RowCount23 <> 0
                            BEGIN
                                SELECT TOP 1 
                                             @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
											 @ValueDate = REPAYMENT_DATE,
                                             @TransactionDate = TRANSACTION_DATE,
											 @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP23_EOD;
	
				--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
		
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
		
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
	
								UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
									  AND REVERSE_FLAG = '1'

                                DELETE FROM #TEMP23_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                     AND TRANSACTION_NO = @TransactionNo
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @ValueDate = REPAYMENT_DATE
                                FROM #TEMP23_EOD;
		
                                SET @RowCount23 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP23_EOD
                                );
                END;
                END
				ELSE

				 IF(@GroupCode in ('DEPOST'))
                    BEGIN
                    
                        SET @RowCount24 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP24_EOD
                            WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                  AND (AMOUNT_PAID > 0
                                       AND isnull(@DepositAllocation, '0') = '0'
									   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND ISNULL(@DepositAllocation, '0') = '1'
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                        );
                        WHILE @RowCount24 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP24_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
										   AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
			
					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                DELETE FROM #TEMP24_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
									  AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP24_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
                                           AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
                                SET @RowCount24 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP24_EOD
                                    WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                          AND (AMOUNT_PAID > 0
                                               AND isnull(@DepositAllocation, '0') = '0'
											   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND ISNULL(@DepositAllocation, '0') = '1'
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                                );
                END;
                END
				ELSE

				 IF(@GroupCode in ('PENALT'))
                    BEGIN
                    
                        SET @RowCount25 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP25_EOD
                            WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                  AND (AMOUNT_PAID > 0
                                       AND isnull(@DepositAllocation, '0') = '0'
									   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND ISNULL(@DepositAllocation, '0') = '1'
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                        );
                        WHILE @RowCount25 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP25_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
										   AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
			
					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                DELETE FROM #TEMP25_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
									  AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP25_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
                                           AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
                                SET @RowCount25 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP25_EOD
                                    WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                          AND (AMOUNT_PAID > 0
                                               AND isnull(@DepositAllocation, '0') = '0'
											   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND ISNULL(@DepositAllocation, '0') = '1'
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                                );
                END;
                END
				ELSE
				IF (@GroupCode = 'ACRUAL' AND @Systemdate = EOMONTH(@SystemDate))
				BEGIN
                        SET @RowCount26 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP26_EOD
                        );
                        WHILE @RowCount26 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionDate = CURRENT_TRANSACTION_DATE,
                                             @ValueDate = CURRENT_TRANSACTION_DATE,
                                             @SeqNoHist = INSTALLMENT_NO
                                FROM #TEMP26_EOD;


								SET @TransactionNo = ''
								
					            --TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_CONTRACT_PAYMENT_SCHEDULE
                                  SET EOM_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
									  WHERE CONTRACT_NO = @ContractNo
								      AND INSTALLMENT_NO = @SeqNoHist;
                                DELETE FROM #TEMP26_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND INSTALLMENT_NO = @SeqNoHist
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionDate = CURRENT_TRANSACTION_DATE,
                                             @ValueDate = CURRENT_TRANSACTION_DATE,
                                             @SeqNoHist = INSTALLMENT_NO
                                FROM #TEMP26_EOD;
                                SET @RowCount26 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP26_EOD
                                );
                            END;
                END
                ELSE
				IF (@GroupCode = 'PDREC' AND @Systemdate = EOMONTH(@SystemDate))
				BEGIN
                        SET @RowCount28 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP28_EOD
                        );
                        WHILE @RowCount28 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionDate = CURRENT_TRANSACTION_DATE,
                                             @ValueDate = CURRENT_TRANSACTION_DATE,
                                             @SeqNoHist = INSTALLMENT_NO
                                FROM #TEMP28_EOD;


								SET @TransactionNo = ''
								
					            --TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                     
                                DELETE FROM #TEMP28_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND INSTALLMENT_NO = @SeqNoHist
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionDate = CURRENT_TRANSACTION_DATE,
                                             @ValueDate = CURRENT_TRANSACTION_DATE,
                                             @SeqNoHist = INSTALLMENT_NO
                                FROM #TEMP28_EOD;
                                SET @RowCount28 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP28_EOD
                                );
                            END;
                END
                ELSE
				IF (@GroupCode = 'RC-TERMI')
				BEGIN
                        SET @RowCount29 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP29_EOD
                        );
                        WHILE @RowCount29 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = TERMINATION_DATE,
                                             @TransactionNo = REPAYMENT_TRANSACTION_NO,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP29_EOD;

								IF @TransactionNo IS NULL
								BEGIN
									BREAK;
								END
								
					            --TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;
                                UPDATE CF_TERMINATION_HEADER
                                SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y' THEN 2 ELSE 1 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND REPAYMENT_TRANSACTION_NO = @TransactionNo;
                                DELETE FROM #TEMP29_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND CAST(TERMINATION_DATE AS DATE) = @ValueDate
                                      AND REPAYMENT_TRANSACTION_NO = @TransactionNo;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @ValueDate = TERMINATION_DATE,
                                             @TransactionNo = REPAYMENT_TRANSACTION_NO
                                FROM #TEMP29_EOD;
                                SET @RowCount29 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP29_EOD
                                );
                            END;
                END
          ELSE
				IF(@GroupCode in ('INSTALM'))
                    BEGIN
                        SET @RowCount27 =
                        (
                            SELECT COUNT(0)
                            FROM #TEMP27_EOD
                            WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                  AND (AMOUNT_PAID > 0
                                       AND isnull(@DepositAllocation, '0') = '0'
									   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                       OR AMOUNT_PAID < 0
                                       AND ISNULL(@DepositAllocation, '0') = '1'
                                       AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                        );

                        WHILE @RowCount27 <> 0
                            BEGIN
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY,
                                             @TransactionDate = TRANSACTION_DATE
                                FROM #TEMP27_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
										   AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
								
					--TRACE PARAMETER
                                TRUNCATE TABLE CF_ACCOUNTING_PARAMETER_TEMP;
                                INSERT INTO CF_ACCOUNTING_PARAMETER_TEMP
                                       SELECT @BranchCode AS BranchCode,
                                              @SystemDate AS TransactionDate,
                                              @ValueDate AS ValueDate,
                                              @ContractNo AS ContractNo,
                                              @TransactionNo AS TransactionNo,
                                              @GroupCode AS GroupCode,
                                              @UserID AS UserID;
                                INSERT INTO LastParameter
                                       SELECT @BranchCode,
                                              @SystemDate,
                                              @ValueDate,
                                              @ContractNo,
                                              @TransactionNo,
                                              @GroupCode,
                                              @UserID;
                                
                                EXEC [SP_GENERATE_ACCOUNTING_JOURNAL]
                                     @BranchCode,
                                     @TransactionDate,
                                     @ValueDate,
                                     @ContractNo,
                                     @TransactionNo,
                                     @GroupCode,
                                     @UserID,
                                     @SeqNoHist,
                                     @SystemDate,
                                     @Success OUTPUT;


                                UPDATE CF_REPAYMENT_HISTORY
                                  SET
                                      JOURNAL_EOD_FLAG = CASE WHEN @Success = 'Y'THEN 1 ELSE 0 END
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
                                      AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;


                                DELETE FROM #TEMP27_EOD
                                WHERE CONTRACT_NO = @ContractNo
                                      AND TRANSACTION_NO = @TransactionNo
									  AND TRANSACTION_CODE = @TransactionCode
                                      AND SEQUENCE_NO_HISTORY = @SeqNoHist;
                                SELECT TOP 1 @ContractNo = CONTRACT_NO,
                                             @TransactionNo = TRANSACTION_NO,
                                             @TransactionCode = TRANSACTION_CODE,
                                             @ValueDate = REPAYMENT_DATE,
                                             @SeqNoHist = SEQUENCE_NO_HISTORY
                                FROM #TEMP27_EOD
                                WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                      AND (AMOUNT_PAID > 0
                                           AND ISNULL(@DepositAllocation, '0') = '0'
										   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                           OR AMOUNT_PAID < 0
                                           AND ISNULL(@DepositAllocation, '0') = '1'
                                           AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'));
								
                                SET @RowCount27 =
                                (
                                    SELECT COUNT(0)
                                    FROM #TEMP27_EOD
                                    WHERE ISNULL(TRANSACTION_CODE, '') = @TransactionCode OR ISNULL(TRANSACTION_CODE, '') in ('T001','T002','T003')
                                          AND (AMOUNT_PAID > 0
                                               AND isnull(@DepositAllocation, '0') = '0'
											   AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0')
                                               OR AMOUNT_PAID < 0
                                               AND ISNULL(@DepositAllocation, '0') = '1'
                                               AND isnull(REVERSE_FLAG,'0') = isnull(@ReverseFlag, '0'))
                                );
                END;
                END


                DELETE FROM #Cursor0
                WHERE CODE = @GroupCode
                      AND (TRANSACTION_CODE = @TransactionCode
                           OR TRANSACTION_CODE IS NULL);
                SELECT TOP 1 @GroupCode = CODE,
                             @TransactionCode = ISNULL(TRANSACTION_CODE, ''),
                             @ReverseFlag = ISNULL(REVERSE_FLAG, '0'),
                             @RepaymentSource = ISNULL(REPAYMENT_SOURCE, ''),
                             @DepositAllocation = ISNULL(DEPOSIT_ALLOCATION, '0')
                FROM #Cursor0
                ORDER BY PROCESS_NO ASC;
                SET @RowCOUNT =
                (
                    SELECT COUNT(0)
                    FROM #Cursor0
                );
            END;


        DROP TABLE #Cursor0;
        DROP TABLE #TEMP0_EOD;
        DROP TABLE #TEMP1_EOD;
        DROP TABLE #TEMP2_EOD;
        DROP TABLE #TEMP3_EOD;
        DROP TABLE #TEMP4_EOD;
        DROP TABLE #TEMP5_EOD;
        DROP TABLE #TEMP6_EOD;
        DROP TABLE #TEMP7_EOD;
        DROP TABLE #TEMP8_EOD;
        DROP TABLE #TEMP9_EOD;
        DROP TABLE #TEMP10_EOD;
        DROP TABLE #TEMP11_EOD;
        DROP TABLE #TEMP12_EOD;
        DROP TABLE #TEMP13_EOD;
        DROP TABLE #TEMP14_EOD;
        DROP TABLE #TEMP15_EOD;
		DROP TABLE #TEMP16_EOD;
		DROP TABLE #TEMP17_EOD;
		DROP TABLE #TEMP18_EOD;
		DROP TABLE #TEMP19_EOD;
    DROP TABLE #TEMP20_EOD;
	DROP TABLE #TEMP21_EOD;
	DROP TABLE #TEMP22_EOD;
	DROP TABLE #TEMP23_EOD;
	DROP TABLE #TEMP24_EOD;
	DROP TABLE #TEMP25_EOD;
	DROP TABLE #TEMP26_EOD;
  DROP TABLE #TEMP27_EOD;
  DROP TABLE #TEMP28_EOD;
  DROP TABLE #TEMP29_EOD;

--RETURN
        SET @FlagSuccess = 'Y';
        UPDATE EOD_LOG
          SET
              END_DATE = GETDATE(),
              END_TIME = GETDATE(),
              SUCCESS_FLAG = 'Y',
              [ERROR_MESSAGE] = 'SUCCESS',
              MODIFY_USER = @UserID,
              MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
              AND CURRENT_TRANSACTION_DATE = @SystemDate
              AND SEQUENCE_NO = @vMaxSeq_ACC
              AND EOD_JOB_TYPE = 'JOU'
              AND [DESCRIPTION] = 'SP_ACCOUNTING_JOURNAL';
    END TRY
    BEGIN CATCH
        SET @FlagSuccess = 'N';
/*
SELECT ERROR_NUMBER() AS ErrorNumber
     ,ERROR_SEVERITY() AS ErrorSeverity
     ,ERROR_STATE() AS ErrorState
     ,ERROR_PROCEDURE() AS ErrorProcedure
     ,ERROR_LINE() AS ErrorLine
     ,ERROR_MESSAGE() AS ErrorMessage;
 --print @FlagSucess
*/

        UPDATE EOD_LOG
          SET
              END_DATE = GETDATE(),
              END_TIME = GETDATE(),
              SUCCESS_FLAG = @FlagSuccess,
              [ERROR_MESSAGE] = CONVERT(NVARCHAR(5), ERROR_LINE())+': '+ERROR_MESSAGE(),
              MODIFY_USER = @UserID,
              MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
              AND CURRENT_TRANSACTION_DATE = @SystemDate
              AND SEQUENCE_NO = @vMaxSeq_ACC
              AND EOD_JOB_TYPE = 'JOU'
              AND [DESCRIPTION] = 'SP_ACCOUNTING_JOURNAL';
    END CATCH;
