USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[SP_GENERATE_ACCOUNTING_JOURNAL]    Script Date: 1/29/2021 15:40:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @Success CHAR(1)
--EXEC [dbo].[SP_GENERATE_ACCOUNTING_JOURNAL] '00000003','2016-01-31',NULL,'128','128','GOLIVE','SYSTEM',@Success = @Success OUTPUT
--SELECT @Success

ALTER PROCEDURE [dbo].[SP_GENERATE_ACCOUNTING_JOURNAL]
	@BranchCode NVARCHAR(20),
	@TransactionDate DATE,
	@ValueDate DATE,
	@ContractNo NVARCHAR(30),
	@TransactionNo NVARCHAR(40),
	@GroupCode NVARCHAR(12),
	@UserID NVARCHAR(12),
	@SeqNoHist Numeric(6),
    @SystemDate date,
	@Success CHAR(1) OUTPUT
	
AS
BEGIN TRY	

	SET NOCOUNT ON;
	SET ANSI_NULLS OFF;
--GO
  
  DECLARE @vMaxSeq_GENACC int
  DECLARE @Outlet_code NVARCHAR(30)
  DECLARE @branch_code NVARCHAR(30)
  DECLARE @RepaymentOutlet NVARCHAR(30)
  DECLARE @RepaymentBranch NVARCHAR(30)
  DECLARE @businessLine NVARCHAR(30)
  DECLARE @paymentMethod NVARCHAR(30)
  DECLARE @RowContractInfo int
	DECLARE @PrincipalAmt NUMERIC(15,2)
	DECLARE @InterestAmt NUMERIC(15,2)
	DECLARE @InstallmentAmt NUMERIC(15,2)
	DECLARE @AdminFee NUMERIC(15,2)
	DECLARE @Fiducia NUMERIC(15,2)
	DECLARE @ApplicationFee NUMERIC(15,2)
	DECLARE @CreditInsurance NUMERIC(15,2)
	DECLARE @CashInsuranceAmt NUMERIC(15,2)
	DECLARE @DownpaymentAmt NUMERIC(15,2)
	DECLARE @RentPaymentType NCHAR(1)
	DECLARE @DPPaymentType NCHAR(1)
	DECLARE @TDPPaymentType NCHAR(1)
	DECLARE @AdminFeePaymentType NCHAR(1)
	DECLARE @FiduciaFeePaymentType NCHAR(1)
	DECLARE @FirstInstPaymentType NCHAR(1)
	DECLARE @InsuranceFeePaymentType NCHAR(1)
	DECLARE @NotaryFlag NCHAR(1)
	DECLARE @ProvisionFlag NCHAR(1)
    DECLARE @APAmt NUMERIC(15,2)
	DECLARE @PayAmount NUMERIC(15,2)
	DECLARE @ProvisionAmount NUMERIC(15,2)
	DECLARE @NotaryAmount NUMERIC(15,2)
	DECLARE @OthersAmount NUMERIC(15,2)
	DECLARE @TransactionCode NVARCHAR(10)
	DECLARE @BankAccount NVARCHAR(50)
	DECLARE @RepaymentType NVARCHAR(3)
	DECLARE	@RepaymentDate DATE
    DECLARE	@RepaymentSource NVARCHAR(3)
    DECLARE @RepaymentChannel NVARCHAR(3)
    DECLARE @Voucher NVARCHAR(10)
	DECLARE @Amount NUMERIC(15,2)
	DECLARE	@ReferenceNo NVARCHAR(20)
    DECLARE @CardType NVARCHAR(3)
    DECLARE @CardNumber  NVARCHAR(20)
    DECLARE @CardName  NVARCHAR(50)
    DECLARE @ReverseFlag  NVARCHAR(3)
	DECLARE @TypeReceive NVARCHAR(12)
	DECLARE @FundInAmt NUMERIC(15,2)
	DECLARE @FundOutAmt NUMERIC(15,2)
	DECLARE @AdjustmentAmt NUMERIC(15,2)
	DECLARE @TransferTo NVARCHAR(2)
	DECLARE @BankHOFlag NCHAR(1)
	DECLARE @BranchofBank NVARCHAR(10)
	DECLARE @InstallmentNoFrom int
	DECLARE @InstallmentNoTo int
	DECLARE @InstallmentNoFromPlus int
	DECLARE @InstallmentNoToPlus int
	DECLARE @RowTemphist int
	DECLARE @FunctionalCode NVARCHAR(20)
    DECLARE @ClassificationCode NVARCHAR(40)
	DECLARE @UnearnedLeaseCredit MONEY
	DECLARE @vOld_AR NUMERIC(15,2),@vOld_Unearn NUMERIC(15,2),@vNew_AR NUMERIC(15,2),@vNew_Unearn NUMERIC(15,2)
	DECLARE @Unearned MONEY
	DECLARE @SeqNo INT
	DECLARE @FlagCrossBranch NVARCHAR(1)
	DECLARE @OSPrinAmt MONEY
	DECLARE @OSIntAmt MONEY
	DECLARE @OSAR MONEY
	DECLARE @OSDep MONEY
    DECLARE @UserOutletFlag NVARCHAR(1)
    DECLARE @COUNT_FAILD_JOURNAL INT
	DECLARE @InstallmentNotes  nvarchar(35)
	DECLARE @SourceCode        nvarchar(10)
	DECLARE @AccountGroupNotes nvarchar(35)
	DECLARE @CustomerName	   nvarchar(30)
	DECLARE @ContractCurrency  nvarchar(5)
    DECLARE @UppingRateAmt	   NUMERIC(15,2)
	DECLARE @TdpAmt			NUMERIC(15,2)
	DECLARE @SuppliesType   NVARCHAR(40)
	DECLARE @PastDueAmt         numeric(15,2)
	DECLARE @ConsumerRecAmt	    numeric(15,2)
	DECLARE @OverduePenaltAmt   numeric(15,2)
	DECLARE @PreterIncomeAmt    numeric(15,2)
	DECLARE @AccruedInterestAmt numeric(15,2)
	DECLARE @TerminationFeeAmt  numeric(15,2)
	DECLARE @TotalTermination   numeric(15,2)
	DECLARE @AssetPriceAmt numeric(15,2)
	DECLARE @SumInstalAmt numeric(15,2)
	DECLARE @SumPenaltAmt numeric(15,2)
	DECLARE @SumDepostAmt numeric(15,2)
	DECLARE @SumInstalTRFAmt numeric(15,2)
	DECLARE @SumInstalCSHAmt numeric(15,2)
	DECLARE @SumRevInstalTRFAmt numeric(15,2)
	DECLARE @SumRevInstalCSHAmt	numeric(15,2)
	DECLARE @SumUnkInstalAmt numeric (15,2)
	DECLARE @SumUnkPenaltAmt numeric (15,2)
	DECLARE	@SumUnkDepostAmt numeric (15,2)
	DECLARE @SumDBAlUnkAmt numeric (15,2)
	DECLARE @RevAmt numeric (15,2)
	DECLARE @SumRevInstalAmt numeric (15,2)
	DECLARE @SumRevPenaltAmt numeric (15,2)
	DECLARE @SumRevDepostAmt numeric (15,2)
	DECLARE @SumRevDepostPlusAmt numeric (15,2)
	DECLARE @InstalMinAmt numeric (15,2)
	DECLARE @InstalRevPlusAmt numeric (15,2)
	DECLARE @SumDepostMinAmt numeric (15,2)
	DECLARE @Credit_term numeric (15,2)
	DECLARE @DepostAmt numeric (15,2)
	DECLARE @DiscountTerminationFeeAmt numeric (15,2)
	DECLARE @DiscountTerminationAmt numeric (15,2)
	DECLARE @HnvCIDAmt numeric (15,2)
	DECLARE @HnvCODAmt numeric (15,2)
	DECLARE @HnvAmt numeric (15,2)
	DECLARE @InsuranceCustName nvarchar (30)
	DECLARE @FiduciaCustName nvarchar (30)
	DECLARE @UnknownName nvarchar (30)
	DECLARE @DepostCSHAMt numeric (15,2)
	DECLARE @DepostCSHMINAMt numeric (15,2)
	DECLARE @DepostTRFAmt numeric (15,2)
	DECLARE @DepostTRFMINAmt numeric (15,2)
	DECLARE @DepostPLUSAmt numeric (15,2)
	DECLARE @DepostMINAMt numeric (15,2)
	DECLARE @PenaltCSHAmt numeric (15,2)
	DECLARE @PenaltCSHMINAmt numeric (15,2)
	DECLARE	@PenaltTRFAmt numeric (15,2)
	DECLARE @PenaltTRFMINAmt numeric (15,2)
	DECLARE @PenaltPLUSAmt numeric (15,2)
	DECLARE @PenaltMINAmt numeric (15,2)
	DECLARE @InstalmCSHAmt numeric (15,2)
	DECLARE @InstalmCSHMINAmt numeric (15,2)
	DECLARE	@InstalmTRFAmt numeric (15,2)
	DECLARE @InstalmTRFMINAmt numeric (15,2)
	DECLARE @InstalmPLUSAmt numeric (15,2)
	DECLARE @InstalmMINAmt numeric (15,2)
	DECLARE @TotalAcrualAmt numeric (15,2)
	DECLARE @PdRecAmount numeric (15,2)
	DECLARE @RcTermiUnearnedIncome NUMERIC(15,2),
					@RcTermiAccountReceivable NUMERIC(15,2),
					@RcTermiReceivable NUMERIC(15,2),
					@RcTermiInstallmentNo NUMERIC;

  set @BankHOFlag = '0'
  set @FlagCrossBranch = '0'
  set @UserOutletFlag = '0'
  
  SELECT @vMaxSeq_GENACC =ISNULL(MAX(SEQUENCE_NO),0)+1
  FROM EOD_LOG
  WHERE BRANCH_CODE = @BranchCode
  AND CURRENT_TRANSACTION_DATE = @SystemDate
  AND EOD_JOB_TYPE='JOU'
  AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'

  IF @vMaxSeq_GENACC =1
  BEGIN
  INSERT INTO EOD_LOG (
     BRANCH_CODE,
     CURRENT_TRANSACTION_DATE,
     SEQUENCE_NO, 
     EOD_JOB_TYPE, 
     [DESCRIPTION], 
     START_TIME, 
     END_TIME, 
     [ERROR_MESSAGE], 
     CREATE_USER, 
     CREATE_DATE,
	 START_DATE,
	 SR_NO
  )VALUES(
    @BranchCode,
    @SystemDate,
    @vMaxSeq_GENACC ,
    'JOU',
    'GEN_ACCOUNTING_JOURNAL',
    getdate(),
    NULL,
    NULL,
    @UserID,
	getdate(),
    getdate(),
    7
   )
   END 
  ELSE
  BEGIN
   UPDATE EOD_LOG
    SET START_TIME = GETDATE(), 
        END_TIME =NULL,
        MODIFY_USER =@UserID,
        MODIFY_DATE =GETDATE(),
        SEQUENCE_NO = @vMaxSeq_GENACC,
		SR_NO =7 
    WHERE BRANCH_CODE = @BranchCode
    AND CURRENT_TRANSACTION_DATE = @SystemDate
    AND EOD_JOB_TYPE = 'JOU'   
    AND SEQUENCE_NO = @vMaxSeq_GENACC -1
    AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'
  
   END

 -- set @ContractNo ='010000001600008'
  --SET @TransactionNo ='010000001600008'
  
  --PRINT ' MASUK SP_GENERATE_ACCOUNTING_JOURNAL'
  	--READ MAPPING JOURNAL DATA AND INSERT TO #TEMP1
	SELECT 
		FIELD_CODE,
		POS_JOURNAL,
		CRITERIA_ACCT_CODE,
		CRITERIA_CODE, 
		JOURNAL_HO_FLAG,
		BANK_HO_FLAG,
		CROSS_BRANCH_FLAG,
		REPAYMENT_BRANCH_FLAG,
		USER_OUTLET_FLAG,
		SEQ_NO
	INTO #TEMP1
	FROM CF_STANDARD_JOURNAL_MST
	WHERE ACTIVE_STATUS = 1 AND GROUP_CODE = @GroupCode
	
		-- Add By Haidir -> Date : 24 July 2019
	-- READ INSTALLMENT NOTES AND SET TO VARIABLE
	set @InstallmentNotes = (SELECT (CASE 
								     WHEN a.INSTALLMENT_TO IS NULL THEN convert(varchar,a.INSTALLMENT_FROM)
								     ELSE convert(varchar,a.INSTALLMENT_FROM)+' - '+convert(varchar,a.INSTALLMENT_TO)+'/'+convert(varchar,b.credit_term)
							      END) as [Installment Notes]
						     FROM CF_REPAYMENT_HISTORY a
							 INNER JOIN CF_CONTRACT_INFO b on a.CONTRACT_NO = b.CONTRACT_NO
						     WHERE  a.CONTRACT_NO         = @ContractNo
							    AND a.TRANSACTION_NO      = @TransactionNo
							    AND a.SEQUENCE_NO_HISTORY = @SeqNoHist)

	SET @Credit_term = (SELECT CREDIT_TERM FROM CF_CONTRACT_INFO
					   WHERE CONTRACT_NO =  @ContractNo)

	-- Add By Haidir -> Date : 24 July 2019
	-- READ SOURCE CODE AND SET TO VARIABLE
	set @SourceCode = ( SELECT  SOURCE_CODE
						FROM CF_ACCOUNT_GROUP_MST
						WHERE CODE = @GroupCode )

	 -- Add By Haidir -> Date : 24 July 2019
	 -- READ NOTES AND SET TO VARIABLE
	 set @AccountGroupNotes = ( SELECT (CASE @GroupCode
											WHEN 'INSTAL'   THEN 'Pembayaran Angsuran'
											WHEN 'INSTALM' THEN 'Pembayaran Angsuran'
											WHEN 'ET-COST'  THEN 'Pembayaran Pretermination'
											WHEN 'ETACCRUE' THEN 'Bunga Berjalan'
											WHEN 'REV-INSTAL' THEN 'Reverse Pembayaran Angsuran'
											ELSE NOTES
										END) as NOTES
								FROM CF_ACCOUNT_GROUP_MST
								WHERE CODE = @GroupCode )
	
	-- Add By Haidir -> Date : 24 July 2019
	-- READ CUSTOMER NAME AND SET TO VARIABLE
	set @CustomerName = (SELECT B.CUSTOMER_NAME FROM CF_CONTRACT_INFO A,CUSTOMER_MST B
						 WHERE B.CUSTOMER_CODE = A.CUSTOMER_CODE
						   AND A.CONTRACT_NO   = @ContractNo)
	

	SET @InsuranceCustName = (SELECT d.CUSTOMER_NAME
						      FROM CF_PAY_INFO a
							  INNER JOIN CF_RECEIVE_POLICY_DTL b on a.INVOICE_NO = b.INVOICE_NO
							  INNER JOIN CF_CONTRACT_INFO c on b.CONTRACT_NO = c.CONTRACT_NO
							  INNER JOIN CUSTOMER_MST d on c.CUSTOMER_CODE = d.CUSTOMER_CODE
							  WHERE a.PAYMENT_ID = @TransactionNo)

	SET @FiduciaCustName = (SELECT e.CUSTOMER_NAME
							FROM CF_PAY_INFO a
							INNER JOIN FIDUCIA_RECEIVE_HEADER b on a.INVOICE_NO = b.INVOICE_NO
							INNER JOIN FIDUCIA_RECEIVE_DETAIL c on b.RECEIVE_NO = c.RECEIVE_NO
							INNER JOIN CF_CONTRACT_INFO d on c.CONTRACT_NO = d.CONTRACT_NO
							INNER JOIN CUSTOMER_MST e on d.CUSTOMER_CODE = e.CUSTOMER_CODE
							WHERE a.PAYMENT_ID = @TransactionNo)

	SET @UnknownName = (SELECT REFERENCE_NAME FROM CF_UNKNOWN_REPAYMENT
						WHERE NO = @TransactionNo)


	-- GET CONTRACT DATA AND INSERT TO #TEMP2
	IF ISNULL(@ContractNo,'') <> '' 
	BEGIN
	SELECT 
		A.CONTRACT_NO,
		A.BRANCH_CODE,
		OUTLET_CODE,
		BUSINESS_LINE,
		PAYMENT_METHOD,
		CONTRACT_CURRENCY,
		PRINCIPAL_AMT,
		INTEREST_AMT,
		INSTALLMENT_AMT,
		ADMIN_FEE_FLAG,
		IIF(ADMIN_FEE_FLAG = 0,ADMIN_FEE_AMT,0) AS 'ADMIN_FEE_AMT',
		FIDUCIA_FEE_FLAG,
		IIF(FIDUCIA_FEE_FLAG = 0,FIDUCIA_AMT,0) AS 'FIDUCIA_AMT',
		APPLICATION_FEE_FLAG,
		IIF(APPLICATION_FEE_FLAG = 0,APPLICATION_FEE_AMT,0) AS 'APPLICATION_FEE_AMT',
		CREDIT_INSURANCE_AMT,
		CASH_INSURANCE_AMT,
		DOWNPAYMENT_AMT,
		(SELECT SUM(PAY_AMOUNT - OFFSET_AMOUNT) 
		FROM CF_PAY_INFO B WHERE B.CONTRACT_NO = A.CONTRACT_NO
		AND PAYMENT_TYPE = '1') AS PAY_AMOUNT,
		RENT_PAYMENT_TYPE,
		A.DP_PAYMENT_TYPE,
		A.ADMIN_FEE_PAYMENT_TYPE,
		A.FIDUCIA_FEE_PAYMENT_TYPE,
		A.FIRST_INST_PAYMENT_TYPE,
		A.INSURANCE_FEE_PAYMENT_TYPE,
		A.TDP_PAYMENT_TYPE,
		ISNULL(A.TDP_AMT,A.DOWNPAYMENT_AMT) AS 'TDP_AMT',
		A.NOTARY_FLAG,
		IIF(A.NOTARY_FLAG = 0, A.NOTARY_AMT,0) AS 'NOTARY_AMT',
		A.PROVISION_FLAG,
		IIF(A.PROVISION_FLAG = 0,A.PROVISION_AMT,0) AS 'PROVISION_AMT',
		A.OTHERS_FLAG,
		ISNULL(A.OTHERS_AMT,0) AS 'OTHERS_AMT',
		A.UPPING_RATE_FLAG,
		ISNULL(A.UPPING_RATE_AMT,0) AS 'UPPING_RATE_AMT',
		A.ASSET_PRICE_AMT
    INTO #TEMP2
	FROM CF_CONTRACT_INFO A
	WHERE A.CONTRACT_NO = @ContractNo 

	SET @RowContractInfo = (SELECT COUNT(0) FROM #TEMP2)

	--SELECT 'CF_CONTRACT_INFO',* FROM #TEMP2

	IF ISNULL(@RowContractInfo,0) > 0 
	BEGIN
	  SELECT @Outlet_code = OUTLET_CODE,
		     @branch_code = BRANCH_CODE,
			 @businessLine = BUSINESS_LINE,
			 @paymentMethod = PAYMENT_METHOD,
			 @ContractCurrency = CONTRACT_CURRENCY,
			 @PrincipalAmt = PRINCIPAL_AMT,
			 @InterestAmt = INTEREST_AMT,
		     @InstallmentAmt = INSTALLMENT_AMT,
		     @AdminFee = ADMIN_FEE_AMT,
		     @Fiducia = FIDUCIA_AMT,
		     @ApplicationFee = APPLICATION_FEE_AMT,
		     @CreditInsurance = CREDIT_INSURANCE_AMT,
		     @CashInsuranceAmt = CASH_INSURANCE_AMT,
		     @DownpaymentAmt = DOWNPAYMENT_AMT,
		     @PayAmount = PAY_AMOUNT,
		     @RentPaymentType = RENT_PAYMENT_TYPE,
		     @DPPaymentType = DP_PAYMENT_TYPE,
			 @TdpAmt = TDP_AMT,
		     @AdminFeePaymentType = ADMIN_FEE_PAYMENT_TYPE,
		     @FiduciaFeePaymentType = FIDUCIA_FEE_PAYMENT_TYPE,
		     @FirstInstPaymentType = FIRST_INST_PAYMENT_TYPE,
		     @InsuranceFeePaymentType = INSURANCE_FEE_PAYMENT_TYPE,
			 @TDPPaymentType = TDP_PAYMENT_TYPE,
			 @ProvisionAmount = PROVISION_AMT,
			 @NotaryAmount = NOTARY_AMT,
			 @OthersAmount = OTHERS_AMT,
			 @UppingRateAmt = UPPING_RATE_AMT,
			 @AssetPriceAmt = ASSET_PRICE_AMT
	  FROM #TEMP2	 	

    
    DECLARE @vFound_RESCH INT
    
    select @vFound_RESCH=COUNT(0)
    from CF_CONTRACT_UPDATE_HEADER
    WHERE CONTRACT_NO =@ContractNo
    and CONTRACT_UPDATE_STATUS='5'

    IF @vFound_RESCH >0
    BEGIn
      SELECT @Outlet_code = OUTLET_CODE,
		     @branch_code = BRANCH_CODE,
  			 @businessLine = BUSINESS_LINE,
  			 @PrincipalAmt = PRINCIPAL_AMT,
  			 @InterestAmt = INTEREST_AMT,
  		     @InstallmentAmt = INSTALLMENT_AMT,
  		     @AdminFee = ADMIN_FEE_AMT,
  		     @Fiducia = FIDUCIA_AMT,
  		     @ApplicationFee = APPLICATION_FEE_AMT,
  		     @CreditInsurance = CREDIT_INSURANCE_AMT,
  		     @CashInsuranceAmt = CASH_INSURANCE_AMT,
  		     @DownpaymentAmt = DOWNPAYMENT_AMT,
			 @AssetPriceAmt	= ASSET_PRICE_AMT,
  		     @PayAmount = (SELECT SUM(PAY_AMOUNT - OFFSET_AMOUNT) 
		                   FROM CF_PAY_INFO B WHERE B.CONTRACT_NO = @ContractNo
		                   AND PAYMENT_TYPE = '1'),
  		     @RentPaymentType = RENT_PAYMENT_TYPE,
  		     @DPPaymentType = DP_PAYMENT_TYPE,
  		     @AdminFeePaymentType = ADMIN_FEE_PAYMENT_TYPE,
  		     @FiduciaFeePaymentType = FIDUCIA_FEE_PAYMENT_TYPE,
  		     @FirstInstPaymentType = FIRST_INST_PAYMENT_TYPE,
  		     @InsuranceFeePaymentType = INSURANCE_FEE_PAYMENT_TYPE
		     FROM CF_CONTRACT_INFO_HISTORY WHERE CONTRACT_NO=@ContractNo	
		     AND HISTORY_NO = (SELECT MAX(HISTORY_NO) FROM CF_CONTRACT_INFO_HISTORY
                      WHERE CONTRACT_NO=@ContractNo	)
    END

		SELECT @OSDep = SUM(DEPOSIT_AMOUNT) FROM CF_DEPOSIT_HISTORY
		WHERE CONTRACT_NO =  @ContractNo
		
		SELECT @OSPrinAmt = SUM(PRINCIPAL_AMT),
	       @OSIntAmt = SUM(INTEREST_AMT),
		   @OSAR = SUM(INSTALLMENT_AMT) + ISNULL(@OSDep,0)
		FROM CF_CONTRACT_PAYMENT_SCHEDULE  
		WHERE CONTRACT_NO =  @ContractNo 
		AND (PAY_STATUS ='0' OR TERMINATE_FLAG = '1')
	END
	ELSE
	BEGIN
			 SET @Outlet_code = ''
		     SET @branch_code = ''
			 SET @businessLine = ''
			 SET @PrincipalAmt = 0
			 SET @InterestAmt = 0
		     SET @InstallmentAmt = 0
		     SET @AdminFee = 0
		     SET @Fiducia = 0
		     SET @ApplicationFee = 0
		     SET @CreditInsurance = 0
		     SET @CashInsuranceAmt = 0
		     SET @DownpaymentAmt = 0
		     SET @PayAmount = 0
		     SET @RentPaymentType = ''
		     SET @DPPaymentType = ''
		     SET @AdminFeePaymentType = ''
		     SET @FiduciaFeePaymentType = ''
		     SET @FirstInstPaymentType = ''
		     SET @InsuranceFeePaymentType = ''
			 SET @TDPPaymentType = ''
			 SET @ProvisionAmount = 0
			 SET @NotaryAmount = 0
			 SET @TdpAmt = 0
			 SET @AssetPriceAmt = 0
	END

	IF ISNULL(@RowContractInfo,0) = 0 AND @GroupCode = 'APPFEE'
	BEGIN
		SELECT BRANCH_CODE,
			   OUTLET_CODE,
			   BUSINESS_LINE
        INTO #TEMPAPPFEE
		FROM CF_APPLY_INFO
		WHERE APPLICATION_NO = @ContractNo	   	

		DECLARE @RowApplyInfo int
		SET @RowApplyInfo = (SELECT COUNT(0) FROM #TEMPAPPFEE)

		IF @RowApplyInfo > 0 
		BEGIN
		  SELECT @Outlet_code = OUTLET_CODE,
				 @branch_code = BRANCH_CODE,
				 @businessLine = BUSINESS_LINE
		  FROM #TEMPAPPFEE	 	
		END
		DROP TABLE #TEMPAPPFEE
	END

	--READ UNEARN PER INSTALLMENT AND SET TO VARIAB
	IF @GroupCode = 'UNEARN'
	BEGIN
		SET @UnearnedLeaseCredit = ISNULL((
		SELECT ACCOUNTING_INTEREST
		FROM CF_PAYMENT_SCHEDULE_ACCOUNTING
		WHERE CONTRACT_NO = @ContractNo
		AND YEAR(ACCOUNTING_DATE) = YEAR(@TransactionDate) AND MONTH(ACCOUNTING_DATE) = MONTH(@TransactionDate)),0)
	END
	IF @GroupCode = 'EARN'
	BEGIN
		SET @UnearnedLeaseCredit = ISNULL((
		SELECT ACCOUNTING_INTEREST
		FROM CF_PAYMENT_SCHEDULE_ACCOUNTING
		WHERE CONTRACT_NO = @ContractNo
		AND YEAR(ACCOUNTING_DATE) = YEAR(DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @TransactionDate), 0))) 
		--YEAR(EOMONTH(@TransactionDate, -1)) 
		AND MONTH(ACCOUNTING_DATE) = MONTH(DATEADD(MONTH,-1,@TransactionDate))),0)
	END

	--READ ASSET CLASSIFICATION AND SET TO VARIABLE
	SET @ClassificationCode = (SELECT  top 1 CLASSIFICATION_CODE
	FROM PRODUCT_MST
	WHERE PRODUCT_CODE IN (SELECT DISTINCT PRODUCT_CODE FROM CF_CONTRACT_ASSET WHERE CONTRACT_NO = @ContractNo))

	--print 'Clas Code '+@ClassificationCode

	--READ SUPPLIES TYPE ASSET AND SET TO VARIABLE
	SET @SuppliesType = (SELECT max(B.SUPPLIES_TYPE) FROM CF_CONTRACT_ASSET A
								 JOIN PRODUCT_CATEGORY_MST B ON A.CATEGORY_CODE = B.CATEGORY_CODE 
									  AND A.CLASSIFICATION_CODE = B.CLASSIFICATION_CODE 
									  AND A.SPECIFICATION_CODE = B.SPECIFICATION_CODE
						 WHERE A.CONTRACT_NO = @ContractNo)

	--print 'Sup Type '+@SuppliesType



	-- UNCOMMENT
	IF (@GroupCode = 'INSTAL' OR @GroupCode = 'REV-INSTAL' OR @GroupCode = 'INSTALM') or @TransactionCode in ('T001','T002','T003')
	BEGIN
		SET @BankAccount = (SELECT DISTINCT BANK_ACCOUNT FROM CF_REPAYMENT_HISTORY
							WHERE CONTRACT_NO = @ContractNo
							AND REPAYMENT_CHANNEL_CODE in ('TRF','CHQ'))
	END


	IF @GroupCode = 'RESCHE'
	BEGIN
		SELECT @vOld_AR=AR_BALANCE,
			   @vOld_Unearn = AR_BALANCE - PRINCIPAL_BALANCE, 
			   @vNew_AR = NEW_RECEIVABLES, 
			   @vNew_Unearn = NEW_RECEIVABLES- NEW_PRINCIPAL
		FROM CF_CONTRACT_UPDATE_HEADER
		WHERE CONTRACT_NO= @ContractNo
		AND TRANSACTION_NO =@TransactionNo
	END

	IF @GroupCode IN ('AP-DLR','AP-END','AP-NDLR') --'AP-OTD'-- add by haidir
	BEGIN
		--- Get Data AP Payment
		SET @PayAmount = (SELECT PAY_AMOUNT - OFFSET_AMOUNT
						  FROM CF_PAY_INFO
						  WHERE CONTRACT_NO = @ContractNo
						  AND PAYMENT_ID = @TransactionNo)

		-- Get Bank AP Payment
		SET @BankAccount = (SELECT PAY_ORIGINIAL_ACCOUNT_NO
							FROM CF_PAY_INFO
							WHERE CONTRACT_NO = @ContractNo
							AND PAYMENT_ID = @TransactionNo)
	END 

	--GET TRANSACTION CODE AND SET TO VARIABLE 
	SET @TransactionCode = (SELECT TRANSACTION_CODE
	FROM CF_ACCOUNT_GROUP_MST
	WHERE CODE = @GroupCode)


	--READ REPAYMENT DATA AND INSERT DATA TO #TEMP3
	IF ISNULL(@TransactionCode,'') <> ''
	BEGIN
	SELECT 
		BRANCH_CODE,
		OUTLET_CODE,
		REPAYMENT_TYPE,
		REPAYMENT_BRANCH_CODE,
		REPAYMENT_OUTLET_CODE,
		REPAYMENT_DATE,
		REPAYMENT_SOURCE_CODE,
		INSTALLMENT_FROM,
		INSTALLMENT_TO,
		REPAYMENT_CHANNEL_CODE,
		VOUCHER_NUMBER,
		AMOUNT_PAID,
		REFERENCE_NO,
		BANK_ACCOUNT,
		CARD_TYPE,
		CARD_NUMBER,
		CARD_NAME,
		REVERSE_FLAG, 
        TRANSACTION_DATE
	INTO #TEMP3
	FROM CF_REPAYMENT_HISTORY
	WHERE CONTRACT_NO = @ContractNo
		AND TRANSACTION_NO = @TransactionNo
		AND	TRANSACTION_CODE = @TransactionCode
		AND SEQUENCE_NO_HISTORY = @SeqNoHist

	SET @RowTemphist = (SELECT COUNT(0) FROM #TEMP3)

	IF ISNULL(@RowTemphist,0) > 0 
	BEGIN
	  SELECT @RepaymentBranch = REPAYMENT_BRANCH_CODE,
	         @RepaymentOutlet = REPAYMENT_OUTLET_CODE,
	         @Outlet_code = OUTLET_CODE,
		     @branch_code = BRANCH_CODE,
			 @BankAccount = BANK_ACCOUNT,
			 @InstallmentNoFrom = INSTALLMENT_FROM,
			 @InstallmentNoTo = INSTALLMENT_TO,
			 @RepaymentType = REPAYMENT_TYPE,
			 @RepaymentDate = REPAYMENT_DATE,
			 @RepaymentSource = REPAYMENT_SOURCE_CODE,
			 @RepaymentChannel = REPAYMENT_CHANNEL_CODE,
		     @Voucher = VOUCHER_NUMBER,
			 @Amount = AMOUNT_PAID,
			 @ReferenceNo = REFERENCE_NO,
		     @CardType = CARD_TYPE,
		     @CardNumber = CARD_NUMBER,
		     @CardName = CARD_NAME,
		     @ReverseFlag = REVERSE_FLAG
	  FROM #TEMP3	 	
	  
 	    --- Get Unearn
		SET @Unearned = ISNULL((select SUM(interest_amt) from cf_contract_payment_schedule 
	    where contract_no = @ContractNo and installment_no between @InstallmentNoFrom AND @InstallmentNoTo),0)

		IF @RepaymentBranch <> @branch_code and @RepaymentBranch <> '0000'
		BEGIN
			SET @FlagCrossBranch = '1'
		END
		ELSE
		Begin
			SET @FlagCrossBranch = '0'
		END 	
	END
	ELSE
	BEGIN
			SET @Outlet_code = ''
		    SET @branch_code = ''
			--SET @BankAccount = ''
			SET @InstallmentNoFrom = 0
			SET @InstallmentNoTo = 0
			SET @RepaymentType = ''
			SET @RepaymentDate = NULL
			SET @RepaymentSource = ''
			SET @RepaymentChannel = ''
		    SET @Voucher = ''
			SET @Amount = 0
			SET @ReferenceNo = ''
		    SET @CardType = ''
		    SET @CardNumber = ''
		    SET @CardName = ''
		    SET @ReverseFlag = '0'
			SET @RepaymentBranch =''
			SET @RepaymentOutlet =''
	END  	
	  DROP TABLE #TEMP3
	END --- END Transaction Code not null
	  DROP TABLE #TEMP2
	END --- END ContractNo Not Null
	

	--READ UNKNOWN TRANSACTION AND INSERT TO #TEMPUNK
	IF ISNULL(@ContractNo ,'') = '' 
	BEGIN
		IF @GroupCode = 'UN-KNW'
		SET @ContractCurrency = 'IDR'
		BEGIN
			SELECT 
				BRANCH_CODE,
				OUTLET_CODE,
				BANK_ACCOUNT,
				TYPE_RECEIVE,
				REFERENCE_NO,
				AMOUNT,
				'' As BUSINESS_LINE
			INTO #TEMPUNK
			FROM CF_UNKNOWN_REPAYMENT
			WHERE BRANCH_CODE = @BranchCode
			AND [NO] = @TransactionNo

			DECLARE @RowTempunk int
			SET @RowTempunk = (SELECT COUNT(0) FROM #TEMPUNK)

			IF ISNULL(@RowTempunk,0) > 0 
			BEGIN
			  SELECT @Outlet_code = OUTLET_CODE,
					 @branch_code = BRANCH_CODE,
					 @businessLine = BUSINESS_LINE,
					 @BankAccount = BANK_ACCOUNT,
					 @ReferenceNo = REFERENCE_NO,
					 @TypeReceive = TYPE_RECEIVE,
					 @Amount = AMOUNT
			  FROM #TEMPUNK	 	
		   END
		   ELSE 
		   BEGIN
			SET @Outlet_code = ''
			SET @branch_code = ''
			SET @businessLine = ''
			SET @BankAccount = ''
			SET @ReferenceNo = ''
			SET @TypeReceive = ''
			SET @Amount = 0
		   END
		   DROP TABLE #TEMPUNK
	   END -- END UNKNOWN

	 
	 IF @GroupCode = 'HANOVR' --OR @GroupCode = 'RECEIVE'
	 BEGIN
		 SELECT BRANCH_CODE,						
				OUTLET_CODE,										
				REPAYMENT_SOURCE_CODE,						
				FUND_IN_AMT,						
				FUND_OUT_AMT,					
				ADJUSTMENT_AMT,						
				STATUS,					
				TRANSFER_TO,							
				BANK_HO_FLAG,						
				BANK_ACCOUNT,							
				REFERENCE_NO,
				'' As BUSINESS_LINE,
				USER_ID
		INTO #TEMPHANDOVER
		FROM CF_CASHIER_BALANCE_DETAIL							
		WHERE BRANCH_CODE = @BranchCode				
		AND	BATCH_TRANSACTION_DATE = @TransactionDate		
		AND REFERENCE_NO = @TransactionNo
		AND STATUS = '3'
		AND TRANSFER_TO ='1'
		AND	JOURNAL_EOD_FLAG ='0'	

		SET @ContractCurrency = 'IDR'

		DECLARE @RowHandover int
		SET @RowHandover = (SELECT COUNT(0) FROM #TEMPHANDOVER)

		IF ISNULL(@RowHandover,0) > 0
		BEGIN
		  SELECT @Outlet_code = OUTLET_CODE,
				 @branch_code = BRANCH_CODE,
				 @businessLine = BUSINESS_LINE, 
				 @BankAccount = BANK_ACCOUNT,
				 @RepaymentSource = REPAYMENT_SOURCE_CODE,
				 @FundInAmt = FUND_IN_AMT,
				 @FundOutAmt = FUND_OUT_AMT,
				 @AdjustmentAmt = ADJUSTMENT_AMT,
				 @TransferTo = TRANSFER_TO,
				 @BankHOFlag = BANK_HO_FLAG,
				 @ReferenceNo = REFERENCE_NO,
				 @UserOutletFlag = '0' ---CASE WHEN isnull(OUTLET_CODE,'') = '' THEN '0' ELSE '1' END
		  FROM #TEMPHANDOVER 	
		END
		ELSE
		BEGIN
			SET @Outlet_code = ''
			SET @branch_code = ''
			SET @businessLine = ''
			SET @BankAccount = ''
			SET @RepaymentSource = ''
			SET @FundInAmt = 0
			SET @FundOutAmt = 0
			SET @AdjustmentAmt = 0
			SET @TransferTo = ''
			SET @BankHOFlag = '0'
			SET @ReferenceNo = ''
			SET @UserOutletFlag ='0'
		END
		DROP TABLE #TEMPHANDOVER
	 END --- END HANDOVER


	 IF  @GroupCode = 'RECEIVE'
	 BEGIN
		 SELECT BRANCH_CODE,						
				OUTLET_CODE,										
				RECEIVED_AMT,						
				REFERENCE_NO,
				'' As BUSINESS_LINE
		INTO #TEMPRECEIVED
		FROM CF_CASHIER_RECEIVED
		WHERE BRANCH_CODE	= @BranchCode				
		AND REFERENCE_NO = @TransactionNo
		AND RECEIVED_DATE = @TransactionDate
		AND	JOURNAL_EOD_FLAG	='0'	

		DECLARE @RowReceived int
		SET @RowReceived = (SELECT COUNT(0) FROM #TEMPRECEIVED)

		IF ISNULL(@RowReceived,0) > 0
		BEGIN
		  SELECT @Outlet_code = OUTLET_CODE,
				 @branch_code = BRANCH_CODE,
				 @businessLine = BUSINESS_LINE,
				 @Amount = RECEIVED_AMT,
				 @ReferenceNo = REFERENCE_NO
		  FROM #TEMPRECEIVED 	
		END
		ELSE
		BEGIN
			SET @Outlet_code = ''
			SET @branch_code = ''
			SET @businessLine = ''
			SET @Amount = 0
			SET @ReferenceNo = ''
		END
		DROP TABLE #TEMPRECEIVED
      END --- END RECEIVE MONEY

	 IF  @GroupCode = 'CHANOVR'
	 BEGIN
		 
		 SELECT BRANCH_CODE,						
				OUTLET_CODE,											
				RECEIVED_AMT,						
				REFERENCE_NO,
				'' As BUSINESS_LINE
		INTO #TEMPCANCELRECEIVED
		FROM CF_CASHIER_RECEIVED
		WHERE BRANCH_CODE	= @BranchCode				
		AND REFERENCE_NO = @TransactionNo
		AND CANCEL_DATE = @TransactionDate
		AND	JOURNAL_EOD_FLAG	='0'	

		DECLARE @RowCanReceived int
		SET @RowCanReceived = (SELECT COUNT(0) FROM #TEMPCANCELRECEIVED)

		IF @RowCanReceived > 0
		BEGIN
		  SELECT @Outlet_code = OUTLET_CODE,
				 @branch_code = BRANCH_CODE,
				 @businessLine = BUSINESS_LINE,
				 @Amount = RECEIVED_AMT,
				 @ReferenceNo = REFERENCE_NO
		  FROM #TEMPCANCELRECEIVED 	
		END
		ELSE
		BEGIN
			SET @Outlet_code = ''
			SET @branch_code = ''
			SET @businessLine = ''
			SET @Amount = 0
			SET @ReferenceNo = ''
		END
		DROP TABLE #TEMPCANCELRECEIVED
	END -- END CANCEL HANDOVER

	--IF @GroupCode IN ('AP-INS','AP-FID') 
	--BEGIN
	--	--- Get Data AP Payment
	--	SET @PayAmount = (SELECT PAY_AMOUNT - OFFSET_AMOUNT
	--	FROM CF_PAY_INFO
	--	WHERE PAYMENT_ID = @TransactionNo)
	--END -- End Payment Insurance & Fiducia Input

	END --- CONTRACT NO ISNULL

	--DELETE CONTENT DATA CF_JOURNAL_TEMP
	DELETE FROM CF_JOURNAL_TEMP
	WHERE CREATE_USER = @UserID
	
	--- Get Flag Bank HO
	SET @BankHOFlag = '0'

	--Read Account Info

	IF ISNULL(@BankAccount,'') <> ''
	BEGIN
		SET @BranchofBank = (SELECT  top 1 customer_code FROM ACCOUNT_INFO WHERE account_no = @BankAccount)
		SET @BankHOFlag = ISNULL((select isnull(head_office_flag,'0') from company_mst where company_code = @BranchofBank),'0')
	END

	SET @SeqNo = 0


	IF @GroupCode IN ('AP-RFC') -- add by haidir
	BEGIN
		--- Get Data AP Payment
		SET @PayAmount = (SELECT PAY_AMOUNT
						  FROM CF_PAY_INFO
						  WHERE CONTRACT_NO = @ContractNo
						  AND PAYMENT_ID = @TransactionNo)

		-- Get Bank AP Payment
		SET @BankAccount = (SELECT PAY_ORIGINIAL_ACCOUNT_NO
							FROM CF_PAY_INFO
							WHERE CONTRACT_NO = @ContractNo
							AND PAYMENT_ID = @TransactionNo)
	END 

	IF @GroupCode IN ('AP-INS','AP-FID') -- add by haidir
	BEGIN
		--- Get Data AP Payment
		SET @PayAmount = (SELECT PAY_AMOUNT
						  FROM CF_PAY_INFO
						  WHERE PAYMENT_ID = @TransactionNo)

		-- Get Bank AP Payment
		SET @BankAccount = (SELECT PAY_ORIGINIAL_ACCOUNT_NO
							FROM CF_PAY_INFO
							WHERE PAYMENT_ID = @TransactionNo)

		SET @ContractCurrency =	(SELECT CURRENCY_ID
								 FROM CF_PAY_INFO
								 WHERE PAYMENT_ID = @TransactionNo)
	END 


	IF @GroupCode IN ('APPFEE','CPROV','CNOTARY','CFIDPAY','CINSUR','CDPCON')
	BEGIN
		SET @BankAccount =	(SELECT TOP 1 BANK_ACCOUNT FROM CF_REPAYMENT_HISTORY
							WHERE CONTRACT_NO = @ContractNo 
							  AND TRANSACTION_NO = @TransactionNo
							  AND SEQUENCE_NO_HISTORY = @SeqNoHist)
	END

	IF @GroupCode IN ('ET-TERMI')
	BEGIN
	-- GET BUSINESS_LINE, PAYMENT_METHOD, CONTRACT_CURRENCY
	SELECT 
		@businessline = BUSINESS_LINE, 
		@paymentMethod = PAYMENT_METHOD, 
		@ContractCurrency = CONTRACT_CURRENCY 
	FROM CF_CONTRACT_INFO
	WHERE CONTRACT_NO = @ContractNo

	SET @BankAccount = (SELECT TOP 1 BANK_ACCOUNT FROM CF_REPAYMENT_HISTORY
						WHERE REPAYMENT_TYPE = 2 
						  AND CONTRACT_NO = @ContractNo 
						  AND TRANSACTION_NO = @TransactionNo)

	SET @TerminationFeeAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							  WHERE REPAYMENT_TYPE = 2 
							    AND CONTRACT_NO = @ContractNo 
							    AND TRANSACTION_NO = @TransactionNo
							    AND TRANSACTION_CODE = 'T027'
								AND REVERSE_FLAG <> '1')

	SET @TotalTermination = (SELECT ISNULL(SUM(AMOUNT_PAID),0) FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 2 
						       AND CONTRACT_NO = @ContractNo 
						       AND TRANSACTION_NO = @TransactionNo
							   AND REVERSE_FLAG <> '1')

	SET @PastDueAmt = (	SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
						WHERE REPAYMENT_TYPE = 2 
						  AND CONTRACT_NO = @ContractNo 
						  AND TRANSACTION_NO = @TransactionNo
						  AND TRANSACTION_CODE = 'T034'
						  AND REVERSE_FLAG <> '1')

	SET @ConsumerRecAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
						   WHERE REPAYMENT_TYPE = 2 
							 AND CONTRACT_NO = @ContractNo 
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = 'T015'
							 AND REVERSE_FLAG <> '1')

	SET @OverduePenaltAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 2 
							   AND CONTRACT_NO = @ContractNo 
							   AND TRANSACTION_NO = @TransactionNo
							   AND TRANSACTION_CODE = 'T002'
							   AND REVERSE_FLAG <> '1')

	SET @AccruedInterestAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							   WHERE REPAYMENT_TYPE = 2 
							     AND CONTRACT_NO = @ContractNo 
							     AND TRANSACTION_NO = @TransactionNo
							     AND TRANSACTION_CODE = 'T026'
								 AND REVERSE_FLAG <> '1')

	SET @DepostAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
					   WHERE REPAYMENT_TYPE = 2 
					   AND CONTRACT_NO = @ContractNo 
					   AND TRANSACTION_NO = @TransactionNo
					   AND TRANSACTION_CODE = 'T003'
					   AND REVERSE_FLAG <> '1')

	SET @DiscountTerminationFeeAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 2 
									  AND CONTRACT_NO = @ContractNo 
									  AND TRANSACTION_NO = @TransactionNo
									  AND TRANSACTION_CODE = 'T017'
									  AND REVERSE_FLAG <> '1')
	
	SET @DiscountTerminationAmt =  (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 2 
									  AND CONTRACT_NO = @ContractNo 
									  AND TRANSACTION_NO = @TransactionNo
									  AND TRANSACTION_CODE = 'T018'
									  AND REVERSE_FLAG <> '1')
	END

	IF (@GroupCode = 'INSTAL')
	BEGIN

		SET @InstallmentNoFromPlus = (SELECT MAX(INSTALLMENT_FROM) from CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 1
									  AND REVERSE_FLAG <> '1'
									  AND CONTRACT_NO = @ContractNo
									  AND TRANSACTION_NO = @TransactionNo
									  AND REPAYMENT_SOURCE_CODE <> 'UNK'
									  AND TRANSACTION_CODE = 'T003'
									  AND AMOUNT_PAID > 0
									  )

		SET @InstallmentNoToPlus = (SELECT MAX(INSTALLMENT_TO) from CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 1
									  AND REVERSE_FLAG <> '1'
									  AND CONTRACT_NO = @ContractNo
									  AND TRANSACTION_NO = @TransactionNo
									  AND REPAYMENT_SOURCE_CODE <> 'UNK'
									  AND TRANSACTION_CODE = 'T003'
									  AND AMOUNT_PAID > 0 
										)

		SET @SumInstalAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 AND TRANSACTION_CODE = 'T001'
							 AND AMOUNT_PAID > 0
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND REPAYMENT_DATE = @RepaymentDate
							 )
	
		SET @SumPenaltAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 AND TRANSACTION_CODE = 'T002'
							 AND AMOUNT_PAID > 0
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND REPAYMENT_DATE = @RepaymentDate
							)
	
		SET @SumDepostAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 AND TRANSACTION_CODE = 'T003'
							 AND AMOUNT_PAID > 0
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND REPAYMENT_DATE = @RepaymentDate
							 )

		SET @SumDepostMinAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 AND TRANSACTION_CODE = 'T003'
							 AND AMOUNT_PAID < 0
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND REPAYMENT_DATE = @RepaymentDate
							 )
	

		SET @SumInstalTRFAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 --AND REPAYMENT_DATE = @RepaymentDate
							 AND REPAYMENT_CHANNEL_CODE in ('TRF','CHQ')
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND AMOUNT_PAID > 0
							 )
		SET @SumInstalCSHAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 --AND REPAYMENT_DATE = @RepaymentDate
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 --AND JOURNAL_EOD_FLAG <> 1
							 --AND AMOUNT_PAID > 0
							 )

		SET @InstalMinAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG <> '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = 'T003'
							 --AND REPAYMENT_DATE = @RepaymentDate
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 --AND JOURNAL_EOD_FLAG <> 1
							 AND AMOUNT_PAID <= 0

							 )
	END


	IF (@GroupCode = 'AL-UNK')
	BEGIN
	
		SET @SumDBAlUnkAmt = (SELECT AMOUNT  FROM CF_UNKNOWN_REPAYMENT
							 WHERE VALID_CONTRACT_NO = @ContractNo
							 )
	
			
		SET @SumUnkInstalAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_CODE = 'T001'
							 AND REPAYMENT_SOURCE_CODE = 'UNK'
							 --AND REPAYMENT_DATE = @RepaymentDate
							 )
	
		SET @SumUnkPenaltAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND CONTRACT_NO = @ContractNo
							 AND REPAYMENT_SOURCE_CODE = 'UNK'
							 AND TRANSACTION_CODE = 'T002'
						     --AND REPAYMENT_DATE = @RepaymentDate
							)
	
		SET @SumUnkDepostAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND CONTRACT_NO = @ContractNo
							 AND REPAYMENT_SOURCE_CODE = 'UNK'
							 AND TRANSACTION_CODE = 'T003'
							 --AND REPAYMENT_DATE = @RepaymentDate
							 )
	END

	IF(@GroupCode = 'REV-INSTAL')
	BEGIN
		
		SET @RevAmt = (SELECT REVERSE_AMOUNT FROM CF_REVERSE_REPAYMENT
						WHERE CONTRACT_NO = @ContractNo
						AND TRANSACTION_NO = @TransactionNo)
		
		SET @InstallmentNoFromPlus = (SELECT MAX(INSTALLMENT_FROM) from CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 1
									  AND REVERSE_FLAG = '1'
									  AND CONTRACT_NO = @ContractNo
									  AND TRANSACTION_NO = @TransactionNo
									  AND REPAYMENT_SOURCE_CODE <> 'UNK'
									  AND TRANSACTION_CODE = 'T003'
									  AND AMOUNT_PAID < 0
									  )

		SET @InstallmentNoToPlus = (SELECT MAX(INSTALLMENT_TO) from CF_REPAYMENT_HISTORY
									  WHERE REPAYMENT_TYPE = 1
									  AND REVERSE_FLAG = '1'
									  AND CONTRACT_NO = @ContractNo
									  AND TRANSACTION_NO = @TransactionNo
									  AND REPAYMENT_SOURCE_CODE <> 'UNK'
									  AND TRANSACTION_CODE = 'T003'
									  AND AMOUNT_PAID < 0 
										)


		SET @SumRevInstalAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
								WHERE CONTRACT_NO = @ContractNo
								AND TRANSACTION_NO = @TransactionNo
								AND REPAYMENT_SOURCE_CODE <> 'UNK'
								AND REPAYMENT_TYPE = 1
								AND REVERSE_FLAG = '1'
								AND TRANSACTION_CODE = 'T001'
								AND REVERSE_REASON is not null)

		SET @SumRevPenaltAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
								WHERE CONTRACT_NO = @ContractNo
								AND TRANSACTION_NO = @TransactionNo
								AND REPAYMENT_SOURCE_CODE <> 'UNK'
								AND REPAYMENT_TYPE = 1
								AND REVERSE_FLAG = '1'
								AND TRANSACTION_CODE = 'T002'
								AND REVERSE_REASON is not null)

		SET @SumRevDepostAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
								WHERE CONTRACT_NO = @ContractNo
								AND TRANSACTION_NO = @TransactionNo
								AND REPAYMENT_SOURCE_CODE <> 'UNK'
								AND REPAYMENT_TYPE = 1
								AND REVERSE_FLAG = '1'
								AND TRANSACTION_CODE = 'T003'
								AND REVERSE_REASON is not null
								AND AMOUNT_PAID < 0)

	    SET @SumRevDepostPlusAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
								WHERE CONTRACT_NO = @ContractNo
								AND TRANSACTION_NO = @TransactionNo
								AND REPAYMENT_SOURCE_CODE <> 'UNK'
								AND REPAYMENT_TYPE = 1
								AND REVERSE_FLAG = '1'
								AND TRANSACTION_CODE = 'T003'
								AND REVERSE_REASON is not null
								AND AMOUNT_PAID > 0)
	
		SET @SumRevInstalTRFAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
								   WHERE REPAYMENT_TYPE = 1
								   AND REVERSE_FLAG = '1'
								   AND CONTRACT_NO = @ContractNo
								   AND TRANSACTION_NO = @TransactionNo
								   AND REPAYMENT_CHANNEL_CODE in ('TRF','CHQ')
								   AND REPAYMENT_SOURCE_CODE <> 'UNK'
								   AND REVERSE_REASON is not null)

		SET @SumRevInstalCSHAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
								   WHERE REPAYMENT_TYPE = 1
								   AND REVERSE_FLAG = '1'
								   AND CONTRACT_NO = @ContractNo
								   AND TRANSACTION_NO = @TransactionNo
								   AND REPAYMENT_CHANNEL_CODE = 'CAS'
								   AND REPAYMENT_SOURCE_CODE <> 'UNK'
								   AND REVERSE_REASON is not null)

		SET @InstalRevPlusAmt = (SELECT SUM(AMOUNT_PAID)  FROM CF_REPAYMENT_HISTORY
							 WHERE REPAYMENT_TYPE = 1
							 AND REVERSE_FLAG = '1'
							 AND CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = 'T003'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
							 AND REVERSE_REASON is not null
							 AND AMOUNT_PAID >= 0)

	END

	IF (@GroupCode = 'HANOVR')
	BEGIN
		SET @HnvCIDAmt = (SELECT SUM(FUND_OUT_AMT) FROM CF_CASHIER_BALANCE_DETAIL
					   WHERE REFERENCE_NO = @TransactionNo 
					   AND REPAYMENT_SOURCE_CODE = 'CID')

		SET @HnvCODAmt = (SELECT SUM(FUND_OUT_AMT) FROM CF_CASHIER_BALANCE_DETAIL
					   WHERE REFERENCE_NO = @TransactionNo 
					   AND REPAYMENT_SOURCE_CODE = 'COD')

		SET @HnvAmt = (SELECT SUM(FUND_OUT_AMT) FROM CF_CASHIER_BALANCE_DETAIL
					   WHERE REFERENCE_NO = @TransactionNo)

	END

	IF (@GroupCode = 'DEPOST')
	BEGIN
		SET @DepostCSHAMt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID > 0)

		SET @DepostTRFAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID > 0)

		SET @DepostCSHMINAMt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID < 0)

		SET @DepostTRFMINAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID < 0)

		SET @DepostPLUSAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID > 0)

		SET @DepostMINAMt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID < 0)

	END

	IF (@GroupCode = 'PENALT')
	BEGIN
		SET @PenaltCSHAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID > 0)

		SET @PenaltTRFAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID > 0)

		SET @PenaltCSHMINAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID < 0)

		SET @PenaltTRFMINAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID < 0)

		SET @PenaltPLUSAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID > 0)

		SET @PenaltMINAmt = (SELECT AMOUNT_PAID FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID < 0)

	END


	IF (@GroupCode = 'ACRUAL')
	BEGIN
		SET @TotalAcrualAmt = (SELECT TOTAL_ACRUAL 
							   FROM CF_CONTRACT_PAYMENT_SCHEDULE A
							   WHERE CONTRACT_NO = @ContractNo
							   AND INSTALLMENT_NO = @SeqNoHist
							   AND BRANCH_CODE = @BranchCode
							   AND @SeqNoHist <> '0'
							   AND PAY_STATUS = '3'
							   AND TOTAL_ACRUAL > 0)
	END

	IF (@GroupCode = 'INSTALM')
	BEGIN
		SET @InstalmCSHAMt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID > 0)

		SET @InstalmTRFAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID > 0)

		SET @InstalmCSHMINAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'CAS'
							 AND AMOUNT_PAID < 0)

		SET @InstalmTRFMINAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND REPAYMENT_CHANNEL_CODE = 'TRF'
							 AND AMOUNT_PAID < 0)
							 
		SET @InstalmPLUSAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID > 0)

		SET @InstalmMINAmt = (SELECT SUM(AMOUNT_PAID) FROM CF_REPAYMENT_HISTORY
							 WHERE CONTRACT_NO = @ContractNo
							 AND TRANSACTION_NO = @TransactionNo
							 AND TRANSACTION_CODE = @TransactionCode
							 AND SEQUENCE_NO_HISTORY = @SeqNoHist
							 AND REPAYMENT_TYPE = '5'
							 AND REPAYMENT_SOURCE_CODE <> 'UNK'
			                 AND REVERSE_FLAG <> '1'
							 AND AMOUNT_PAID < 0)
	END

	IF (@GroupCode = 'PDREC')
	BEGIN
		SET @PdRecAmount = (SELECT INSTALLMENT_AMT
							   FROM CF_CONTRACT_PAYMENT_SCHEDULE A
							   WHERE CONTRACT_NO = @ContractNo
								AND MONTH(a.DUE_DATE) = MONTH(@SystemDate) 
								AND YEAR(a.DUE_DATE) = YEAR(@SystemDate)
								AND A.BRANCH_CODE = @BranchCode
								AND A.INSTALLMENT_NO <> '0'
								AND A.PAY_STATUS = '0' 
								OR FORMAT(A.DUE_DATE, 'yyyyMM') < FORMAT(A.PAID_DATE, 'yyyyMM') 
								AND MONTH(a.DUE_DATE) = MONTH(@SystemDate) 
								AND YEAR(a.DUE_DATE) = YEAR(@SystemDate)
								AND A.BRANCH_CODE = @BranchCode
								AND A.INSTALLMENT_NO <> '0'
								AND A.CONTRACT_NO = @ContractNo
								 )
	END

	IF (@GroupCode = 'RC-TERMI')
	BEGIN
		SELECT 
				@RcTermiUnearnedIncome = INTEREST_BALANCE_ADMIN, 
				@RcTermiAccountReceivable = PRINCIPAL_BALANCE_ADMIN, 
				@RcTermiReceivable = RECEIVABLES_BALANCE,
				@RcTermiInstallmentNo = INSTALLMENT_NO
		FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE RCTERMIA
		LEFT JOIN (
				SELECT 
					CONTRACT_NO,
					CAST(TERMINATION_DATE AS DATE) AS TERMINATION_DATE
				FROM CF_TERMINATION_HEADER 
				WHERE [STATUS] = 'CL'
				AND BRANCH_CODE = @BranchCode
				AND TRANSACTION_DATE <= @TransactionDate
				AND JOURNAL_EOD_FLAG = '1'
				AND CONTRACT_NO = @ContractNo
		) AS RCTERMIB ON RCTERMIA.CONTRACT_NO = RCTERMIB.CONTRACT_NO
		WHERE FORMAT(RCTERMIA.DUE_DATE, 'yyyyMM') = FORMAT(RCTERMIB.TERMINATION_DATE, 'yyyyMM')
	END

	--PRINT 'MASUK PENGAMBILAN NILAI'

	--GENERATE JOURNAL TO TEMP TABLE #TEMP1_VER2 FROM #TEMP1
	SELECT *, 
	CASE 
	WHEN (isnull(CRITERIA_ACCT_CODE,'') ='') THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@InsuranceFeePaymentType,'') = '2' THEN ISNULL(@CreditInsurance + @CashInsuranceAmt,0)
	--WHEN (CRITERIA_ACCT_CODE = 'C002') AND ISNULL(@AdminFeePaymentType,'2') = '2' THEN ISNULL(@AdminFee,0)
	--WHEN (CRITERIA_ACCT_CODE = 'C003') AND ISNULL(@FiduciaFeePaymentType,'2') = '2' THEN ISNULL(@Fiducia,0)
    --temp delete
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@CreditInsurance + CASE WHEN @SuppliesType <> '1' THEN ISNULL(@OthersAmount,0) ELSE ISNULL(@CashInsuranceAmt,0) END,0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
    --end temp delete
	WHEN (CRITERIA_CODE = 'C001') AND @GroupCode = 'GOLIVE' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(ISNULL(@CreditInsurance,0) +ISNULL(@CashInsuranceAmt,0)+ CASE WHEN @SuppliesType <> '1' THEN ISNULL(@OthersAmount,0) ELSE 0 END,0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
	
    -- WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@CreditInsurance + ISNULL(@CashInsuranceAmt,0),0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
    --revert
	WHEN (CRITERIA_ACCT_CODE = 'C002') AND ISNULL(@AdminFee,0) > 0  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C003') AND ISNULL(@Fiducia,0) > 0  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND ISNULL(@RepaymentChannel,'') = 'CAS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND ISNULL(@TypeReceive,'') = '1' AND @GroupCode ='UN-KNW'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND @GroupCode = 'PENALT' THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C004') AND @GroupCode = 'PENALT'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C005') AND (@GroupCode = 'AL-UNK' OR @GroupCode = 'UN-KNW' OR @GroupCode = 'REV-INSTAL' OR @GroupCode = 'INSTAL' OR @GroupCode = 'ET-TERMI') THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C005') AND ISNULL(@RepaymentChannel,'') IN ('TRF','CHQ','CAS') AND ISNULL(@RepaymentSource,'') <> 'UNK' AND ISNULL(@RepaymentSource,'') NOT IN ('H2H','UPL') THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C005') /* AND ISNULL(@TypeReceive,'') = '2' */ AND @GroupCode ='UN-KNW'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C006') AND ISNULL(@RepaymentSource,'') IN ('H2H','UPL') THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C007') AND ISNULL(@FirstInstPaymentType,'') = 2 AND ISNULL(@RentPaymentType,'') = 1 THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C007') AND (ISNULL(@RentPaymentType,'') = 0) THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C007') AND (ISNULL(@RentPaymentType,'') = 1) THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C008') AND ISNULL(@DownpaymentAmt,0) > 0 THEN ISNULL(@DownpaymentAmt,0)
    WHEN (CRITERIA_ACCT_CODE = 'C009') AND ISNULL(@RepaymentChannel,'') = 'CRD' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C010') AND ISNULL(@RepaymentChannel,'') = 'CHQ' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C011') AND ISNULL(@RepaymentChannel,'') = 'DS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C012') AND ISNULL(@RepaymentChannel,'') = 'EMB' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C013') AND ISNULL(@RepaymentChannel,'') = 'PEX' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C014') AND ISNULL(@RepaymentChannel,'') = 'VCH' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C015') AND ISNULL(@RepaymentChannel,'') = 'DPS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1  
    WHEN (CRITERIA_ACCT_CODE = 'C016') AND ISNULL(@RepaymentSource,'') = 'UNK' THEN 1  
    WHEN (CRITERIA_ACCT_CODE = 'C017') AND ISNULL(@TransferTo,'') = '2' THEN 1  -- TRANSFER_TO = 2 (Cashier)
    WHEN (CRITERIA_ACCT_CODE = 'C018') AND ISNULL(@TransferTo,'') = '1' THEN 1  -- TRANSFER_TO = 1 (Bank)
	WHEN (CRITERIA_ACCT_CODE = 'C019') AND ISNULL(@ProvisionAmount,'') > 0 THEN 1  
	WHEN (CRITERIA_ACCT_CODE = 'C020') AND @GroupCode = 'GOLIVE' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C020') AND ISNULL(@NotaryAmount,'') > 0 THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C021') AND ISNULL(@OthersAmount,'') > 0 THEN 1  
    END AS VALUE_CRITERIA,
	CASE WHEN (CASE 
	WHEN (isnull(CRITERIA_ACCT_CODE,'') ='') THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@InsuranceFeePaymentType,'') = '2' THEN ISNULL(@CreditInsurance + @CashInsuranceAmt,0)
	--WHEN (CRITERIA_ACCT_CODE = 'C002') AND ISNULL(@AdminFeePaymentType,'2') = '2' THEN ISNULL(@AdminFee,0)
	--WHEN (CRITERIA_ACCT_CODE = 'C003') AND ISNULL(@FiduciaFeePaymentType,'2') = '2' THEN ISNULL(@Fiducia,0)
    --temp delete
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@CreditInsurance + CASE WHEN @SuppliesType <> '1' THEN ISNULL(@OthersAmount,0) ELSE ISNULL(@CashInsuranceAmt,0) END,0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
    --end temp delete
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(@CreditInsurance + ISNULL(@CashInsuranceAmt,0),0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
    WHEN (CRITERIA_CODE = 'C001') AND @GroupCode = 'GOLIVE' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C001') AND ISNULL(ISNULL(@CreditInsurance,0) + ISNULL(@CashInsuranceAmt,0)+ CASE WHEN @SuppliesType <> '1' THEN ISNULL(@OthersAmount,0) ELSE 0 END,0) > 0 THEN 1
	-- WHEN (CRITERIA_ACCT_CODE = 'C001') AND (ISNULL(@CreditInsurance,0) + IIF(ISNULL(@CashInsuranceAmt,0)=0,ISNULL(@OthersAmount,0),0)) > 0 THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C002') AND ISNULL(@AdminFee,0) > 0 THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C003') AND ISNULL(@Fiducia,0) > 0  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND ISNULL(@RepaymentChannel,'') = 'CAS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND ISNULL(@TypeReceive,'') = '1' AND @GroupCode ='UN-KNW'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C004') AND @GroupCode = 'PENALT'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C005') AND (@GroupCode = 'AL-UNK' OR @GroupCode = 'UN-KNW' OR @GroupCode = 'REV-INSTAL' OR @GroupCode = 'INSTAL' OR @GroupCode = 'ET-TERMI') THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C005') AND ISNULL(@RepaymentChannel,'') IN ('TRF' ,'CHQ','CAS') AND ISNULL(@RepaymentSource,'') <> 'UNK' AND ISNULL(@RepaymentSource,'') NOT IN ('H2H','UPL')  THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C005') /* AND ISNULL(@TypeReceive,'') = '2' */ AND @GroupCode ='UN-KNW'  THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C006') AND ISNULL(@RepaymentSource,'') IN ('H2H','UPL') AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
	--WHEN (CRITERIA_ACCT_CODE = 'C007') AND ISNULL(@FirstInstPaymentType,'') = 2 AND ISNULL(@RentPaymentType,'') = 1 THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C007') AND (ISNULL(@RentPaymentType,'') = 0) THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C007') AND (ISNULL(@RentPaymentType,'') = 1) THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C008') AND ISNULL(@DownpaymentAmt,0) > 0 THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C009') AND ISNULL(@RepaymentChannel,'')  = 'CRD' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C010') AND ISNULL(@RepaymentChannel,'')  = 'CHQ' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C011') AND ISNULL(@RepaymentChannel,'')  = 'DS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C012') AND ISNULL(@RepaymentChannel,'')  = 'EMB' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C013') AND ISNULL(@RepaymentChannel,'')  = 'PEX' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C014') AND ISNULL(@RepaymentChannel,'')  = 'VCH' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1
    WHEN (CRITERIA_ACCT_CODE = 'C015') AND ISNULL(@RepaymentChannel,'')  = 'DPS' AND ISNULL(@RepaymentSource,'') <> 'UNK' THEN 1 
	WHEN (CRITERIA_ACCT_CODE = 'C016') AND ISNULL(@RepaymentSource,'') = 'UNK' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C017') AND ISNULL(@TransferTo,'') = '2' THEN 1  -- TRANSFER_TO = 2 (Cashier)
    WHEN (CRITERIA_ACCT_CODE = 'C018') AND ISNULL(@TransferTo,'') = '1' THEN 1  -- TRANSFER_TO = 1 (Bank)
	WHEN (CRITERIA_ACCT_CODE = 'C019') AND ISNULL(@ProvisionAmount,'') > 0 THEN 1  
	WHEN (CRITERIA_ACCT_CODE = 'C020') AND @GroupCode = 'GOLIVE' THEN 1
	WHEN (CRITERIA_ACCT_CODE = 'C020') AND ISNULL(@NotaryAmount,'') > 0 THEN 1 
	WHEN (CRITERIA_ACCT_CODE = 'C021') AND ISNULL(@OthersAmount,'') > 0 THEN 1 
    END) > 0 THEN 1 ELSE 0 END AS VALID_CRITERIA,
	ISNULL(CASE 
	WHEN CRITERIA_CODE = 'AR1' THEN ISNULL(@PrincipalAmt +  @InterestAmt,0)
	--WHEN CRITERIA_CODE = 'INT' AND @GroupCode = 'TDP' THEN ISNULL(@AdminFee,0) + ISNULL(@ProvisionAmount,0)
	WHEN CRITERIA_CODE = 'INT' AND @GroupCode = 'GOLIVE' THEN ISNULL(@InterestAmt,0)
	--WHEN CRITERIA_CODE = 'INT' THEN (ISNULL(@InterestAmt,0) + ISNULL(@UppingRateAmt,0) + ISNULL(@AdminFee,0) + ISNULL(@ProvisionAmount,0))
	WHEN CRITERIA_CODE = 'INS' AND @RentPaymentType = 1 THEN ISNULL(@InstallmentAmt,0)
	--WHEN CRITERIA_CODE = 'ANS' AND (@GroupCode = 'INSTAL' AND POS_JOURNAL = 'C') THEN ISNULL(@SumInstalAmt,0) + ISNULL(@SumPenaltAmt,0) + ISNULL(@SumDepostAmt,0)
	WHEN CRITERIA_CODE = 'ANS' AND (@GroupCode = 'INSTAL' AND POS_JOURNAL = 'C') THEN ISNULL(@SumInstalAmt,0) - ISNULL(ABS(@SumDepostMinAmt),0)
	WHEN CRITERIA_CODE = 'ANS' AND (@GroupCode = 'INSTAL' AND POS_JOURNAL = 'D') THEN ISNULL(@SumInstalTRFAmt,0)
	WHEN CRITERIA_CODE = 'ANS' AND (@GroupCode = 'REV-INSTAL' AND POS_JOURNAL = 'C') THEN ISNULL(ABS(@SumRevInstalTRFAmt),0)
	WHEN CRITERIA_CODE = 'ANS' AND (@GroupCode = 'REV-INSTAL' AND POS_JOURNAL = 'D') THEN ISNULL(ABS(@SumRevInstalAmt),0) - ISNULL(@SumRevDepostPlusAmt,0)
	WHEN CRITERIA_CODE = 'ANS' AND @GroupCode = 'INSTALM' THEN CASE WHEN FIELD_CODE = 'BHQ' THEN IIF(POS_JOURNAL = 'D',ISNULL(@InstalmTRFAmt,0),ABS(ISNULL(@InstalmTRFMINAmt,0))) WHEN FIELD_CODE = 'AK1' THEN IIF(POS_JOURNAL = 'D',ABS(ISNULL(@InstalmMinAmt,0)),ISNULL(@InstalmPLUSAmt,0)) ELSE 0 END 
	WHEN CRITERIA_CODE = 'CSH' AND @GroupCode = 'INSTAL' THEN ISNULL(@SumInstalCSHAmt,0)
	WHEN CRITERIA_CODE = 'CSH' AND @GroupCode = 'REV-INSTAL' THEN ISNULL(ABS(@SumRevInstalCSHAmt),0)
	WHEN CRITERIA_CODE = 'CSH' AND @GroupCode = 'DEPOST' THEN CASE WHEN POS_JOURNAL = 'D' THEN ISNULL(@DepostCSHAMt,0) ELSE ISNULL(@DepostCSHMINAmt,0) END
	WHEN CRITERIA_CODE = 'CSH' AND @GroupCode = 'PENALT' THEN CASE WHEN POS_JOURNAL = 'D' THEN ISNULL(@PenaltCSHAmt,0) ELSE ISNULL(@PenaltCSHMINAmt,0) END
	WHEN CRITERIA_CODE = 'CSH' AND @GroupCode = 'INSTALM' THEN CASE WHEN POS_JOURNAL = 'D' THEN ISNULL(@InstalmCSHAmt,0) ELSE ISNULL(@InstalmCSHMINAmt,0) END
	--WHEN CRITERIA_CODE = 'ANS' THEN ISNULL(@InstallmentAmt,0) 
	WHEN CRITERIA_CODE = 'APD' AND @GroupCode = 'CDPCON' THEN ISNULL(@DownpaymentAmt,0) 
	WHEN CRITERIA_CODE = 'APD' THEN ISNULL(@PayAmount,0) 
	--WHEN CRITERIA_CODE = 'API' THEN ISNULL(@CreditInsurance + CASE WHEN @SuppliesType <> '1' THEN ISNULL(@OthersAmount,0) ELSE ISNULL(@CashInsuranceAmt,0) END,0)
	-- WHEN CRITERIA_CODE = 'API' THEN ISNULL(@CreditInsurance + CASE WHEN ISNULL(@CashInsuranceAmt,0) = 0 THEN ISNULL(@OthersAmount,0) ELSE ISNULL(@CashInsuranceAmt,0) END,0)
	WHEN CRITERIA_CODE = 'API' THEN ISNULL(ISNULL(@CreditInsurance,0) + ISNULL(@CashInsuranceAmt,0)+ CASE WHEN @SuppliesType not in ('1','11') THEN ISNULL(@OthersAmount,0) ELSE 0 END,0)
	--WHEN CRITERIA_CODE = 'APF' THEN ISNULL(@Fiducia,0) 
	WHEN CRITERIA_CODE = 'BLL' THEN ISNULL(@OthersAmount,0)  -- add by haidir
	WHEN CRITERIA_CODE = 'APC' THEN ISNULL(@PayAmount,0)  -- add by haidir
	WHEN CRITERIA_CODE = 'APE' THEN ISNULL(@PayAmount,0)  -- add by haidir
	WHEN CRITERIA_CODE = 'ADM' AND @GroupCode = 'ADMIN'  THEN ISNULL(@AdminFee,0)
	WHEN CRITERIA_CODE = 'ADM' /*AND @ProvisionAmount > 0*/ THEN ISNULL(@AdminFee,0) --+ ISNULL(@ProvisionAmount,0)   
	WHEN CRITERIA_CODE = 'HIST' AND @GroupCode = 'INSTAL' THEN CASE WHEN FIELD_CODE = 'DEN' THEN ISNULL(@SumPenaltAmt,0) ELSE ISNULL(@SumDepostAmt,0) END
	WHEN CRITERIA_CODE = 'HIST' AND @GroupCode = 'REV-INSTAL' THEN CASE WHEN FIELD_CODE = 'DEN' THEN ISNULL(ABS(@SumRevPenaltAmt),0) ELSE ISNULL(ABS(@SumRevDepostAmt),0) END
	WHEN CRITERIA_CODE = 'HIST' AND @GroupCode = 'DEPOST' THEN CASE WHEN FIELD_CODE = 'BHQ' THEN IIF(POS_JOURNAL = 'D',ISNULL(@DepostTRFAmt,0),ABS(ISNULL(@DepostTRFMINAmt,0))) WHEN FIELD_CODE = 'ADV' THEN IIF(POS_JOURNAL = 'D',ABS(ISNULL(@DepostMINAmt,0)),ISNULL(@DepostPLUSAmt,0)) ELSE 0 END
	WHEN CRITERIA_CODE = 'HIST' AND @GroupCode = 'PENALT' THEN CASE WHEN FIELD_CODE = 'BHQ' THEN IIF(POS_JOURNAL = 'D',ISNULL(@PenaltTRFAmt,0),ABS(ISNULL(@PenaltTRFMINAmt,0))) WHEN FIELD_CODE = 'DEN' THEN IIF(POS_JOURNAL = 'D',ABS(ISNULL(@PenaltMINAmt,0)),ISNULL(@PenaltPLUSAmt,0)) ELSE 0 END
	WHEN CRITERIA_CODE = 'UNK' AND @GroupCode = 'AL-UNK' THEN ISNULL(@SumDBAlUnkAmt,0)
	--WHEN CRITERIA_CODE = 'UNK' AND @GroupCode = 'REV-INSTAL' THEN ISNULL(ABS(@RevAmt),0)
	WHEN CRITERIA_CODE = 'UNK' THEN ISNULL(@Amount,0)
	WHEN CRITERIA_CODE = 'AUK' THEN ISNULL(@Amount,0)
	WHEN CRITERIA_CODE = 'UNR' THEN ISNULL(@Unearned,0)
	WHEN CRITERIA_CODE = 'DP1' AND (@businessLine = '02' AND @paymentMethod in ('01','02')) THEN ISNULL(@DownpaymentAmt,0)
	WHEN CRITERIA_CODE = 'DP1' AND (@GroupCode = 'GOLIVE' AND POS_JOURNAL = 'C') THEN CASE WHEN @TDPPaymentType = '1' THEN ISNULL(@DownpaymentAmt,0) ELSE 0 END
	WHEN CRITERIA_CODE = 'APS' THEN ISNULL(@PayAmount,0)
    WHEN CRITERIA_CODE = 'ARNEW' THEN ISNULL(@vNew_AR,0)
    WHEN CRITERIA_CODE = 'AROLD' THEN ISNULL(@vOld_AR,0)
    WHEN CRITERIA_CODE = 'UNNEW' THEN ISNULL(@vNew_Unearn,0)
    WHEN CRITERIA_CODE = 'UNOLD' THEN ISNULL(@vOld_Unearn,0)
	--WHEN CRITERIA_CODE = 'HNOVR' THEN ISNULL(@FundOutAmt,0)
	WHEN CRITERIA_CODE = 'RECEV' THEN ISNULL(@Amount,0)
	WHEN CRITERIA_CODE = 'CHOVR' THEN ISNULL(@Amount,0)
	WHEN CRITERIA_CODE = 'OPRC' THEN ISNULL(@OSPrinAmt,0)
	WHEN CRITERIA_CODE = 'OINT' THEN ISNULL(@OSIntAmt,0)
	WHEN CRITERIA_CODE = 'ODEP' THEN ISNULL(@OSDep,0)
	WHEN CRITERIA_CODE = 'OAR' THEN ISNULL(@OSAR,0)
	WHEN CRITERIA_CODE = 'UNA' THEN ISNULL(@UnearnedLeaseCredit,0)
	--WHEN CRITERIA_CODE = 'NRY' THEN ISNULL(@NotaryAmount,0)
	WHEN CRITERIA_CODE = 'NRY' THEN ISNULL(@NotaryAmount,0) + ISNULL(@Fiducia,0)
	WHEN CRITERIA_CODE = 'PRV' THEN ISNULL(@ProvisionAmount,0)
	--WHEN CRITERIA_CODE = 'BNK' THEN (ISNULL(@PrincipalAmt,0) - ISNULL(@AdminFee,0)) - ISNULL(@OthersAmount,0) - ISNULL(@Fiducia,0) - ISNULL(@CashInsuranceAmt,0) - ISNULL(@ProvisionAmount,0) - ISNULL(@NotaryAmount,0) - ISNULL(@ApplicationFee,0) - (CASE WHEN @RentPaymentType = 1 THEN ISNULL(@InstallmentAmt,0) ELSE 0 END)
    WHEN CRITERIA_CODE = 'BNK' AND (@GroupCode = 'GOLIVE' AND ((@businessline = '01' AND @paymentMethod != '03') OR (@businessLine = '02' AND @paymentMethod != '05') OR (@businessLine = '03' AND @paymentMethod != '04'))) THEN CASE WHEN @TDPPaymentType = '1' THEN ISNULL(@AssetPriceAmt,0) - ISNULL(@DownpaymentAmt,0) ELSE ISNULL(@AssetPriceAmt,0) - ISNULL(@TdpAmt,0) END
  --WHEN CRITERIA_CODE = 'BNK' AND ((@businessline = '01' AND @paymentMethod != '03') OR (@businessLine = '02' AND @paymentMethod != '05') OR (@businessLine = '03' AND @paymentMethod != '04')) THEN (ISNULL(@PrincipalAmt,0) - ISNULL(@AdminFee,0)) - ISNULL(@OthersAmount,0) - ISNULL(@Fiducia,0) - ISNULL(@CashInsuranceAmt,0) - ISNULL(@ProvisionAmount,0) - ISNULL(@NotaryAmount,0) - ISNULL(@ApplicationFee,0) - (CASE WHEN @RentPaymentType = 1 THEN ISNULL(@InstallmentAmt,0) ELSE 0 END)
	--WHEN CRITERIA_CODE = 'APN' AND ((@businessline = '01' AND @paymentMethod = '03') OR (@businessLine = '02' AND @paymentMethod = '05') OR (@businessLine = '03' AND @paymentMethod = '04')) THEN (ISNULL(@PrincipalAmt,0) - ISNULL(@AdminFee,0)) - ISNULL(@OthersAmount,0) - ISNULL(@Fiducia,0) - ISNULL(@CashInsuranceAmt,0) - ISNULL(@ProvisionAmount,0) - ISNULL(@NotaryAmount,0) - ISNULL(@ApplicationFee,0) - (CASE WHEN @RentPaymentType = 1 THEN ISNULL(@InstallmentAmt,0) ELSE 0 END)
	WHEN CRITERIA_CODE = 'APN' AND (@GroupCode = 'GOLIVE' AND ((@businessline = '01' AND @paymentMethod = '03') OR (@businessLine = '02' AND @paymentMethod = '05') OR (@businessLine = '03' AND @paymentMethod = '04'))) THEN CASE WHEN @TDPPaymentType = 1 THEN ISNULL(@AssetPriceAmt,0) - ISNULL(@DownpaymentAmt,0) ELSE ISNULL(@AssetPriceAmt,0) - ISNULL(@TdpAmt,0) END
    WHEN CRITERIA_CODE = 'API2' THEN ISNULL(@PayAmount,0)
	WHEN CRITERIA_CODE = 'APF2' THEN ISNULL(@PayAmount,0)
	WHEN CRITERIA_CODE = 'BN1'  THEN ISNULL(@TotalTermination,0) - ISNULL(@TerminationFeeAmt,0)
	WHEN CRITERIA_CODE = 'PSD'  THEN ISNULL(@PastDueAmt,0) - ISNULL(ABS(@DepostAmt),0)
	WHEN CRITERIA_CODE = 'CRE'  THEN ISNULL(@ConsumerRecAmt,0)
	WHEN CRITERIA_CODE = 'OVP'  THEN ISNULL(@OverduePenaltAmt,0)
	WHEN CRITERIA_CODE = 'PRI'  THEN (ISNULL(@ConsumerRecAmt,0) * 0.05) - ISNULL(ABS(@DiscountTerminationFeeAmt),0)
	WHEN CRITERIA_CODE = 'ACR'  THEN ISNULL(@AccruedInterestAmt,0) + ISNULL(@DiscountTerminationAmt,0)
	WHEN CRITERIA_CODE = 'APP' THEN ISNULL(@ApplicationFee,0)
	WHEN CRITERIA_CODE = 'TDP' THEN ISNULL(@TdpAmt,0)
	WHEN CRITERIA_CODE = 'CSD' AND @GroupCode = 'TDP' THEN ISNULL(@TdpAmt,0)
	WHEN CRITERIA_CODE = 'CSD'  THEN CASE WHEN @TDPPaymentType = '1' THEN ISNULL(@TdpAmt,0) ELSE 0 END
	WHEN CRITERIA_CODE = 'UKANS' THEN ISNULL(@SumUnkInstalAmt,0)
	WHEN CRITERIA_CODE = 'UKHI' THEN CASE WHEN FIELD_CODE = 'DEN' THEN ISNULL(@SumUnkPenaltAmt,0) ELSE ISNULL(@SumUnkDepostAmt,0) END
	WHEN CRITERIA_CODE = 'HISTM' AND @GroupCode = 'INSTAL' THEN ISNULL(ABS(@InstalMinAmt),0)
	WHEN CRITERIA_CODE = 'ANSM' AND @GroupCode = 'INSTAL' THEN ISNULL(ABS(@InstalMinAmt),0)
	WHEN CRITERIA_CODE = 'HISTM' AND @GroupCode = 'ET-TERMI' THEN ISNULL(ABS(@DepostAmt),0)
	WHEN CRITERIA_CODE = 'ANSM' AND @GroupCode = 'ET-TERMI' THEN ISNULL(ABS(@DepostAmt),0)
	WHEN CRITERIA_CODE = 'HISTM' AND @GroupCode = 'REV-INSTAl' THEN ISNULL(@InstalRevPlusAmt,0)
	WHEN CRITERIA_CODE = 'ANSM' AND @GroupCode = 'REV-INSTAL' THEN ISNULL(@InstalRevPlusAmt,0)
	WHEN CRITERIA_CODE = 'HNCID' THEN ISNULL(@HnvCIDAmt,0)
	WHEN CRITERIA_CODE = 'HNCOD' THEN ISNULL(@HnvCODAmt,0)
	WHEN CRITERIA_CODE = 'HNV' THEN ISNULL(@HnvAmt,0)
	WHEN CRITERIA_CODE = 'UNRCR' OR  CRITERIA_CODE = 'FNI' THEN ISNULL(@TotalAcrualAmt,0)
	WHEN CRITERIA_CODE = 'APO' THEN CASE WHEN @SuppliesType in ('1','11') THEN ISNULL(@OthersAmount,0) ELSE 0 END
	WHEN CRITERIA_CODE = 'INSAM' THEN ABS(ISNULL(@PdRecAmount,0))
	WHEN CRITERIA_CODE = 'UNRTR' THEN ABS(ISNULL(@RcTermiUnearnedIncome,0))
	WHEN CRITERIA_CODE = 'FARTR' THEN ABS(ISNULL(@RcTermiAccountReceivable,0))
	WHEN CRITERIA_CODE = 'FNRTR' THEN ABS(ISNULL(@RcTermiReceivable,0))
	END,0) AS VALUE
	INTO #TEMP1_VER2
	FROM #TEMP1

	--PRINT 'SELESAI PENGAMBILAN NILAI'

	--SELECT 'TEMP1_VER2', * FROM #TEMP1_VER2

  SELECT * 
  INTO #TEMP1_VER3
  FROM #TEMP1_VER2 
  WHERE 
  VALID_CRITERIA > 0 
  AND VALUE <> 0  
  AND (
      ISNULL(CRITERIA_ACCT_CODE,'') ='' 
      OR BANK_HO_FLAG = @BankHOFlag 
      OR ISNULL(CRITERIA_ACCT_CODE,'') IS NOT NULL
      )
  AND CROSS_BRANCH_FLAG = @FlagCrossBranch
  AND USER_OUTLET_FLAG = @UserOutletFlag

	-- TEST
  -- SELECT 'TEMP1_VER3', * FROM #TEMP1_VER3


	--READ COA AND INSERT TO #TEMP1_VER2
	SELECT DISTINCT(B.FIELD_CODE),A.ACCOUNT_CODE
	INTO #TEMPCHARTACCOUNT
	FROM CF_MAPPING_ACCOUNT_MST A
	JOIN #TEMP1_VER2 B ON A.FIELD_CODE = B.FIELD_CODE
	WHERE ACTIVE_STATUS = 1
	AND (BRANCH_CODE = CASE WHEN @BankHOFlag ='1' AND ISNULL(ACCOUNT_BANK_NO,'') <> '' THEN '0000' WHEN @FlagCrossBranch='1' THEN @RepaymentBranch ELSE @branch_code END OR BRANCH_CODE IS NULL OR BRANCH_CODE ='')
	AND (BUSINESS_LINE = @businessline OR BUSINESS_LINE IS NULL OR BUSINESS_LINE ='')
	AND (ASSET_CLASSIFICATION_CODE = @ClassificationCode OR ASSET_CLASSIFICATION_CODE IS NULL OR ASSET_CLASSIFICATION_CODE='')
	AND (PAYMENT_METHOD = @paymentMethod OR PAYMENT_METHOD IS NULL OR PAYMENT_METHOD='')
	AND (CURRENCY_CODE  = @ContractCurrency OR CURRENCY_CODE IS NULL OR CURRENCY_CODE='')
	AND (ACCOUNT_BANK_NO = @BankAccount OR ACCOUNT_BANK_NO IS NULL OR ACCOUNT_BANK_NO = '')

	--PRINT 'BANK HO FLAG '+@BankHOFlag
	--PRINT 'FLAG CROSS BRANCH '+@FlagCrossBranch
	--PRINT 'REPAYMENT BRANCH '+@RepaymentBranch
	--PRINT 'BRANCH CODE '+@branch_code
	--PRINT 'BUSINESS LINE '+@businessline
	--PRINT 'CLASSIFICATION CODE '+@ClassificationCode
	--PRINT 'PAYMENT METHOD '+@paymentMethod
	--PRINT 'CONTRACT CURRENCY '+@ContractCurrency
	--PRINT 'BANK ACCOUNT '+@BankAccount

	-- TEST
	-- SELECT 'COA', * FROM #TEMPCHARTACCOUNT


	--READ AND SET FUNCTIONAL_CODE TO VARIABLE

	SET @FunctionalCode = (
	SELECT FUNCTIONAL_CODE
	FROM FUNCTIONAL_ACCT_MST
	WHERE GROUP_CODE = @GroupCode
	AND ISNULL(REPAYMENT_TYPE,'') = ( CASE WHEN ISNULL(REPAYMENT_TYPE,'')<>''  
                        THEN ISNULL(@RepaymentType,'') ELSE ISNULL(REPAYMENT_TYPE,'')
                       END)
	AND ISNULL(REPAYMENT_SOURCE_CODE,'') = ( CASE WHEN ISNULL(REPAYMENT_SOURCE_CODE,'')<>'' 
                        THEN ISNULL(@RepaymentSource,'') ELSE ISNULL(REPAYMENT_SOURCE_CODE,'')
                       END)
	AND ISNULL(REPAYMENT_CHANNEL_CODE,'') = ( CASE WHEN ISNULL(REPAYMENT_CHANNEL_CODE,'')<>'' 
                        THEN ISNULL(@RepaymentChannel,'') ELSE ISNULL(REPAYMENT_CHANNEL_CODE,'')
                       END))

	
	IF ISNULL(@FunctionalCode,'') = '' 
	BEGIN
		SET @FunctionalCode = @GroupCode
	END	

	--INSERT INTO TEMP TABLE CF_JOURNAL_TEMP
  INSERT INTO CF_JOURNAL_TEMP
  	SELECT 
		CASE WHEN B.JOURNAL_HO_FLAG = '1' THEN '0000' WHEN @FlagCrossBranch='1' AND repayment_branch_flag = '1'  THEN @RepaymentBranch ELSE @Branch_Code END AS BRANCH_CODE,
		--CASE WHEN B.JOURNAL_HO_FLAG = '1' THEN '21457' ELSE @Outlet_code END AS OUTLET_CODE,
		CASE WHEN B.JOURNAL_HO_FLAG = '1' THEN '000001' ELSE @Outlet_code END AS OUTLET_CODE,
		ROW_NUMBER() OVER (ORDER BY B.SEQ_NO) AS SEQ_NO,
		ISNULL(@ContractNo,'') AS CONTRACT_NO,
		ISNULL(@TransactionNo,'') AS TRANSACTION_NO,
		CASE WHEN @GroupCode = 'EARN' THEN @SystemDate ELSE @TransactionDate END AS TRANSACTION_DATE,
		@ValueDate AS VALUE_DATE,
		isnull(@businessLine,'') AS BUSINESS_LINE,
		isnull(@GroupCode,'') AS GROUP_CODE,
		C.ACCOUNT_CODE AS ACCOUNT_CODE,
		B.POS_JOURNAL AS POS_JOURNAL,
		'IDR' AS CURRENCY_CODE,
		B.VALUE AS AMOUNT,
		isnull(@ReverseFlag,'') AS REVERSAL_FLAG,
		isnull(@ReferenceNo,'') AS REFERENCE_NO,
		isnull(@FunctionalCode,'') AS FUNCTIONAL_CODE,
		isnull(@RepaymentType,'') AS REPAYMENT_TYPE,
		isnull(@RepaymentSource,'') AS REPAYMENT_SOURCE_CODE,
		isnull(@RepaymentChannel,'') AS REPAYMENT_CHANNEL_CODE,
		B.JOURNAL_HO_FLAG,
		@UserID AS CREATE_USER,
		GETDATE() AS CREATE_DATE
		FROM #TEMP1_VER3 B
		LEFT JOIN #TEMPCHARTACCOUNT C ON B.FIELD_CODE = C.FIELD_CODE 
		ORDER BY B.SEQ_NO ASC

		-- TEST
    -- select 'temp', * from CF_JOURNAL_TEMP 

	--CHECK BALANCE JOURNAL AND SET TO VARIABLE DEBIT/CREDIT
	DECLARE @SumDebet MONEY
	DECLARE @SumKredit MONEY

	SET @SumDebet = 
	(SELECT SUM(AMOUNT) FROM CF_JOURNAL_TEMP
	WHERE GROUP_CODE = @GroupCode AND TRANSACTION_NO = @TransactionNo
	AND POS_JOURNAL = 'D')

	SET @SumKredit = 
	(SELECT SUM(AMOUNT) FROM CF_JOURNAL_TEMP
	WHERE GROUP_CODE = @GroupCode AND TRANSACTION_NO = @TransactionNo
	AND POS_JOURNAL = 'C')

	-- select * from CF_JOURNAL_TEMP
	print @sumDebet
	print @sumkredit

	PRINT '@BranchCode :'+@BranchCode
	PRINT '@GroupCode  :'+@GroupCode
	 IF (ISNULL(@SumDebet,'' ))='' AND (ISNULL(@SumKredit,'' ))=''
	 BEGIN
		PRINT '@SumDebet : 0' + '@SumKredit : 0'
	END
	ELSE
	BEGIN	
	PRINT '@SumDebet : '+convert(nvarchar(15),@SumDebet )
	PRINT '@SumKredit :'+convert(nvarchar(15),@SumKredit )
	END
	PRINT '================================'

	IF(@SumDebet > 0 AND @SumDebet = @SumKredit)
	BEGIN

	DECLARE @MaxSeqID INT
	DECLARE @JournalNo NVARCHAR(60)

	--GET AND SET JOURNAL_NO  
	SET @MaxSeqID = 
	(SELECT ISNULL(SEQ_ID,0) FROM NUMBERING_KEY
	WHERE SUB_SYSTEM_DIVISION = 'ACCT' AND GET_ID_DIVISION = 'TRN' AND
	NUMBERING_KEY = 'ACCT-' + @GroupCode + CONVERT(NVARCHAR(4),YEAR(@TransactionDate))+CONVERT(NVARCHAR(2),MONTH(@TransactionDate)))

	--SET @JournalNo = 'ACCT-' + @GroupCode + CONVERT(NVARCHAR,@TransactionCode,112) + CONVERT(NVARCHAR,@MaxSeqID + 1)
  SET @JournalNo = 'ACCT-' + @GroupCode + CONVERT(NVARCHAR(4),YEAR(@TransactionDate))+CONVERT(NVARCHAR(2),MONTH(@TransactionDate)) +CONVERT(NVARCHAR(10),ISNULL(@MaxSeqID,0) + 1)
  
  PRINT 'JURNAL NO :'+@JournalNo
  
  IF isnull(@MaxSeqID,0) =0
  BEGIN
  INSERT INTO NUMBERING_KEY 
  (SUB_SYSTEM_DIVISION, GET_ID_DIVISION, NUMBERING_KEY, SEQ_ID, CREATE_USER, CREATE_DATE,modify_user,modify_date)
  VALUES('ACCT','TRN','ACCT-' + @GroupCode + CONVERT(NVARCHAR(4),YEAR(@TransactionDate))+CONVERT(NVARCHAR(2),MONTH(@TransactionDate)),isnull(@MaxSeqID,0)+1, @UserID, GETDATE(),@UserID, GETDATE()) 
  END
  ELSE
  BEGIN
	UPDATE NUMBERING_KEY
	SET SEQ_ID = isnull(@MaxSeqID,0) + 1
	WHERE SUB_SYSTEM_DIVISION = 'ACCT' AND GET_ID_DIVISION = 'TRN' AND
	NUMBERING_KEY = 'ACCT-' + @GroupCode + CONVERT(NVARCHAR(4),YEAR(@TransactionDate))+CONVERT(NVARCHAR(2),MONTH(@TransactionDate))
  END
    
	-- Insert into journal info
		INSERT INTO CF_JOURNAL_INFO 
			(BRANCH_CODE,OUTLET_CODE,JOURNAL_NO,SEQ_NO,CONTRACT_NO,TRANSACTION_NO,TRANSACTION_DATE,VALUE_DATE,
			BUSINESS_LINE,GROUP_CODE,ACCOUNT_CODE,POS_JOURNAL,CURRENCY_CODE,AMOUNT,REVERSAL_FLAG,REFERENCE_NO,
			FUNCTIONAL_CODE,REPAYMENT_TYPE,REPAYMENT_SOURCE_CODE, REPAYMENT_CHANNEL_CODE,CREATE_USER,CREATE_DATE,
			MODIFY_USER,MODIFY_DATE,PROCESS_FLAG,JOURNAL_HO_FLAG,NOTES,SOURCE_CODE)
			SELECT 
				BRANCH_CODE,
				OUTLET_CODE,
				@JournalNo AS JOURNAL_NO,
				SEQ_NO,
				ISNULL(CONTRACT_NO,' '),
				ISNULL(TRANSACTION_NO,' '),
				ISNULL(TRANSACTION_DATE,GETDATE()),
				ISNULL(VALUE_DATE,GETDATE()),
				ISNULL(BUSINESS_LINE,' '),
				ISNULL(GROUP_CODE,' '),
				ISNULL(ACCOUNT_CODE,' '),
				ISNULL(POS_JOURNAL,' '),
				ISNULL(CURRENCY_CODE,' '),
				ISNULL(AMOUNT,0),
				ISNULL(REVERSAL_FLAG,'0'),
				isnull(REFERENCE_NO,' '),
				--@ContractNo AS REFERENCE_NO,
				ISNULL(FUNCTIONAL_CODE,' '),
				ISNULL(REPAYMENT_TYPE,' '),
				ISNULL(REPAYMENT_SOURCE_CODE,' '),
				ISNULL(REPAYMENT_CHANNEL_CODE,' '),
				@UserID AS CREATE_USER,
				GETDATE() AS CREATE_DATE, 
				@UserID AS MODIFY_USER,
				GETDATE() AS MODIFY_DATE,
				'0' as process_flag,
				JOURNAL_HO_FLAG,
				(CASE 
					WHEN @GroupCode = 'INSTAL'  or @GroupCode = 'REV-INSTAL' or @GroupCode = 'INSTALM'  THEN @AccountGroupNotes+' '+@CustomerName+' ke '+@InstallmentNotes
					WHEN @GroupCode = 'ET-COST' or @GroupCode = 'ETACCRUE' THEN @AccountGroupNotes+' '+@CustomerName
					WHEN @GroupCode = 'AP-INS' or @GroupCode = 'AP-END' or @GroupCode = 'AP-RFC' THEN @AccountGroupNotes+' '+ISNULL(@InsuranceCustName,'')
					WHEN @GroupCode = 'AP-FID' THEN @AccountGroupNotes+' '+ISNULL(@FiduciaCustName,'')
					WHEN @GroupCode = 'ACRUAL' THEN ISNULL(@CustomerName,'')+' PER : '+CONVERT(VARCHAR,@SeqNoHist)
					WHEN @GroupCode = 'UN-KNW' THEN @AccountGroupNotes+' '+ISNULL(@UnknownName,'')
					WHEN @GroupCode = 'PDREC' THEN ISNULL(@CustomerName,'')+' PER : '+CONVERT(VARCHAR,@SeqNoHist)
					WHEN @GroupCode = 'RC-TERMI' THEN ISNULL(@CustomerName,'')+' PER : '+CONVERT(VARCHAR,@RcTermiInstallmentNo)
					ELSE @AccountGroupNotes+' '+ISNULL(@CustomerName,'')
				 END) AS NOTES,  -- add by Haidir
				 @SourceCode
			FROM CF_JOURNAL_TEMP WHERE GROUP_CODE = @GroupCode AND TRANSACTION_NO = @TransactionNo
			AND CREATE_USER = @UserID
	


	--Update Notes Journal Info Instal
	IF (@GroupCode = 'INSTAL')
	BEGIN
			IF exists (SELECT ACCOUNT_CODE FROM CF_JOURNAL_INFO
					   WHERE CONTRACT_NO = @ContractNo
					   AND TRANSACTION_NO = @TransactionNo
					   AND ACCOUNT_CODE	in ('120301020001',
											'120302020001',
											'120303020001',
											'120301020002',
											'120302020002',
											'120303020002',
											'120101020001',
											'120102020001',
											'120104020001',
											'120105020001',
											'120101020002',
											'120102020002',
											'120104020002',
											'120105020002',
											'120201020001',
											'120204020001',
											'120201020002',
											'120204020002')
						AND POS_JOURNAL = 'C')
			BEGIN
						IF exists (SELECT ACCOUNT_CODE FROM CF_JOURNAL_INFO
								   WHERE CONTRACT_NO = @ContractNo
								   AND TRANSACTION_NO = @TransactionNo
								   AND ACCOUNT_CODE	in ('120301070001',
														'120301070001',
														'120301070002',
														'120302050001',
														'120302050002',
														'120303050001',
														'120303050002',
														'120101070001',
														'120102070001',
														'120104050001',
														'120101070002',
														'120102070002',
														'120104050002',
														'120102070001',
														'120204050001',
														'120102070002',
														'120204050002',
														'120105050001',
														'120105050002')
									AND POS_JOURNAL		= 'C')
						BEGIN
								UPDATE CF_JOURNAL_INFO
								SET NOTES = @AccountGroupNotes+' '+@CustomerName+' ke '+CONVERT(VARCHAR,@InstallmentNoFromPlus)+' - '+CONVERT(VARCHAR,@InstallmentNoToPlus)+'/'+SUBSTRING(CONVERT(VARCHAR,@Credit_Term),1,2)
								WHERE CONTRACT_NO	= @ContractNo
								AND TRANSACTION_NO   = @TransactionNo
								AND SEQ_NO = (SELECT TOP 1 SEQ_NO FROM CF_JOURNAL_INFO WHERE CONTRACT_NO = @ContractNo
											  AND TRANSACTION_NO   = @TransactionNo
											  ORDER BY SEQ_NO DESC)
						END
			END
	END


	--Update Notes Journal Info REV-INSTAL
	IF (@GroupCode = 'REV-INSTAL')
	BEGIN
			IF exists (SELECT ACCOUNT_CODE FROM CF_JOURNAL_INFO
					   WHERE CONTRACT_NO = @ContractNo
					   AND TRANSACTION_NO = @TransactionNo
					   AND GROUP_CODE = 'REV-INSTAL'
					   AND ACCOUNT_CODE	in ('120301020001',
											'120302020001',
											'120303020001',
											'120301020002',
											'120302020002',
											'120303020002',
											'120101020001',
											'120102020001',
											'120104020001',
											'120105020001',
											'120101020002',
											'120102020002',
											'120104020002',
											'120105020002',
											'120201020001',
											'120204020001',
											'120201020002',
											'120204020002')
						AND POS_JOURNAL = 'D')
			BEGIN
						IF exists (SELECT ACCOUNT_CODE FROM CF_JOURNAL_INFO
						WHERE CONTRACT_NO	= @ContractNo
						AND TRANSACTION_NO  = @TransactionNo
						AND GROUP_CODE = 'REV-INSTAL'
						AND ACCOUNT_CODE	in ('120301070001',
												'120301070001',
												'120301070002',
												'120302050001',
												'120302050002',
												'120303050001',
												'120303050002',
												'120101070001',
												'120102070001',
												'120104050001',
												'120101070002',
												'120102070002',
												'120104050002',
												'120102070001',
												'120204050001',
												'120102070002',
												'120204050002',
												'120105050001',
												'120105050002')
						AND POS_JOURNAL		= 'D')
						BEGIN
								UPDATE CF_JOURNAL_INFO
								SET NOTES = @AccountGroupNotes+' '+@CustomerName+' ke '+CONVERT(VARCHAR,@InstallmentNoFromPlus)+' - '+CONVERT(VARCHAR,@InstallmentNoToPlus)+'/'+SUBSTRING(CONVERT(VARCHAR,@Credit_Term),1,2)
								WHERE CONTRACT_NO	= @ContractNo
								AND TRANSACTION_NO   = @TransactionNo
								AND GROUP_CODE = 'REV-INSTAL'
								AND SEQ_NO = (SELECT MAX(SEQ_NO) FROM CF_JOURNAL_INFO 
											  WHERE CONTRACT_NO = @ContractNo
											  AND TRANSACTION_NO = @TransactionNo
											  AND ACCOUNT_CODE in ('120301070001',
																   '120301070001',
																   '120301070002',
																   '120302050001',
																   '120302050002',
																   '120303050001',
																   '120303050002',
																   '120101070001',
																   '120102070001',
																   '120104050001',
																   '120101070002',
																   '120102070002',
																   '120104050002',
																   '120102070001',
																   '120204050001',
																   '120102070002',
																   '120204050002',
																   '120105050001',
																   '120105050002'))
						END
			END
	END






	--- Update Functional Code : UNR For Group : INSTAL
	IF @GroupCode = 'INSTAL'
	BEGIN
		UPDATE Accounting.CF_JOURNAL_INFO SET FUNCTIONAL_CODE = 'PAI'
		where group_code ='INSTAL'
		AND TRANSACTION_DATE = @TransactionDate
		AND TRANSACTION_NO = @TransactionNo
		AND ACCOUNT_CODE IN (SELECT ACCOUNT_CODE FROM CF_MAPPING_ACCOUNT_MST WHERE FIELD_CODE IN ('UNR','EAR'))
	END

	IF @GroupCode = 'REV-INSTAL'
	BEGIN
		UPDATE Accounting.CF_JOURNAL_INFO SET FUNCTIONAL_CODE = 'RAI'
		where group_code ='REV-INSTAL'
		AND TRANSACTION_DATE = @TransactionDate
		AND TRANSACTION_NO = @TransactionNo
		AND ACCOUNT_CODE IN (SELECT ACCOUNT_CODE FROM CF_MAPPING_ACCOUNT_MST WHERE FIELD_CODE IN ('UNR','EAR'))
	END

	SET @Success = 'Y'
  
	  UPDATE EOD_LOG
	  SET  END_TIME = GETDATE(),
		   SUCCESS_FLAG = 'Y',
		   [ERROR_MESSAGE]= 'SUCCESS',
		   MODIFY_USER=@UserID,
		   MODIFY_DATE = GETDATE()
	  WHERE BRANCH_CODE = @BranchCode
	  AND CURRENT_TRANSACTION_DATE = @TransactionDate
	  AND SEQUENCE_NO = @vMaxSeq_GENACC
	  AND EOD_JOB_TYPE = 'JOU'
	  AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'
  
	END
	ELSE 
	BEGIN
	
  
  SELECT @COUNT_FAILD_JOURNAL=COUNT(0)
  FROM CF_JOURNAL_FAILED
  WHERE GROUP_CODE = @GroupCode
  AND TRANSACTION_NO = @TransactionNo
  AND SEQ_NO = 1
  AND TRANSACTION_DATE = CASE WHEN @GroupCode = 'EARN' THEN @SystemDate ELSE @TransactionDate END 
  
	  IF @COUNT_FAILD_JOURNAL=0
		BEGIN
	
  	--INSERT INTO CF_JOURNAL_FAILED
  		INSERT INTO CF_JOURNAL_FAILED
  		SELECT 
  		BRANCH_CODE,
  		OUTLET_CODE,
  		GROUP_CODE,
  		TRANSACTION_DATE,
  		SEQ_NO,
  		CONTRACT_NO,
  		TRANSACTION_NO,
  		FUNCTIONAL_CODE,
  		'ACCOUNTING JOURNAL NOT BALANCED' AS NOTES,
  		CREATE_USER,
  		GETDATE() AS CREATE_DATE,
  		CREATE_USER AS MODIFY_USER,
  		GETDATE() AS MODIFY_DATE
  		FROM
  		CF_JOURNAL_TEMP
  		WHERE SEQ_NO = 1

		SET @Success = 'N'

		IF (ISNULL(@SumDebet,'' ))<>'' OR (ISNULL(@SumKredit,'' ))<>''
		BEGIN
		UPDATE EOD_LOG
		SET  END_TIME = GETDATE(),
		   END_DATE = GETDATE(),
		   SUCCESS_FLAG = @Success,
		   [ERROR_MESSAGE]='ACCOUNTING JOURNAL NOT BALANCED',--'SUCCESS',
		   MODIFY_USER=@UserID,
		   MODIFY_DATE = GETDATE()
		  WHERE BRANCH_CODE = @BranchCode
		  AND CURRENT_TRANSACTION_DATE = @SystemDate
		  AND SEQUENCE_NO = @vMaxSeq_GENACC
		  AND EOD_JOB_TYPE = 'JOU'
		  AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'
		END
		ELSE
		BEGIN
			UPDATE EOD_LOG
	  SET  END_DATE=GETDATE(),
		   END_TIME = GETDATE(),
		   SUCCESS_FLAG = 'Y',
		   [ERROR_MESSAGE]= 'SUCCESS',
		   MODIFY_USER=@UserID,
		   MODIFY_DATE = GETDATE()
	  WHERE BRANCH_CODE = @BranchCode
	  AND CURRENT_TRANSACTION_DATE = @SystemDate
	  AND SEQUENCE_NO = @vMaxSeq_GENACC
	  AND EOD_JOB_TYPE = 'JOU'
	  AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'
		END
  END
  
	END


	DROP TABLE #TEMP1
	DROP TABLE #TEMP1_VER2
    DROP TABLE #TEMP1_VER3
	DROP TABLE #TEMPCHARTACCOUNT
	--DROP TABLE #TEMP2
	
	
	--DELETE FROM CF_JOURNAL_TEMP
	--WHERE GROUP_CODE = @GroupCode AND TRANSACTION_NO = @TransactionNo
	--AND CREATE_USER = @UserID
	

	--DROP TABLE #CF_JOURNAL_TEMP
	--DROP TABLE #CF_JOURNAL_FAILED
	--RETURN 
END TRY

BEGIN CATCH
--SET @FlagSucess = 'N'

SELECT ERROR_NUMBER() AS ErrorNumber
     ,ERROR_SEVERITY() AS ErrorSeverity
     ,ERROR_STATE() AS ErrorState
     ,ERROR_PROCEDURE() AS ErrorProcedure
     ,ERROR_LINE() AS ErrorLine
     ,ERROR_MESSAGE() AS ErrorMessage;
 --print @FlagSucess
 

 SET @Success = 'N'

 UPDATE EOD_LOG
  SET  END_TIME = GETDATE(),
       END_DATE = GETDATE(),
	   SUCCESS_FLAG = @Success,
       [ERROR_MESSAGE]= CONVERT(nvarchar(5),ERROR_LINE())+': '+ERROR_MESSAGE(),
       MODIFY_USER=@UserID,
       MODIFY_DATE = GETDATE()
  WHERE BRANCH_CODE = @BranchCode
  AND CURRENT_TRANSACTION_DATE = @SystemDate
  AND SEQUENCE_NO = @vMaxSeq_GENACC
  AND EOD_JOB_TYPE = 'JOU'
  AND [DESCRIPTION]='GEN_ACCOUNTING_JOURNAL'
 
END CATCH