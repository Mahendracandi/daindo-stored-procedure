DECLARE @MyCursor CURSOR;
DECLARE @coa VARCHAR(50);
DECLARE @output char(1)
BEGIN
    SET @MyCursor = CURSOR FOR
    SELECT COA_CODE FROM COA_MST

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @coa

    WHILE @@FETCH_STATUS = 0
    BEGIN
      
	  --EXEC SP_INSERT_ACCOUNT @AccountCode = @coa, @userID = 'IGLO', @SystemDate = '2020-10-13', @FlagSuccess = @output OUTPUT
	  --Declare @output char(1)
	  --EXEC SP_INSERT_ACCOUNT @AccountCode = '950000000000', @userID = 'IGLO', @SystemDate = '2020-10-12', @FlagSuccess = @output OUTPUT
	  UPDATE AKT.dbo.TRX_SALDO SET
			[BRANCH_CODE] = '0000',
			[YEAR_PERIOD] = '2020',
			[OUTLET_CODE] = '000001',
			[BEGIN_BAL_1] = 0,
			[MUT_VALUE_1] = 0,
			[END_BAL_1] = 0, 
			[BEGIN_BAL_2] = 0,
			[MUT_VALUE_2] = 0,
			[END_BAL_2] = 0, 
			[BEGIN_BAL_3] = 0,
			[MUT_VALUE_3] = 0,
			[END_BAL_3] = 0,
			[BEGIN_BAL_4] = 0,
			[MUT_VALUE_4] = 0,
			[END_BAL_4] = 0,
			[BEGIN_BAL_5] = 0,
			[MUT_VALUE_5] = 0,
			[END_BAL_5] = 0,
			[BEGIN_BAL_6] = 0,
			[MUT_VALUE_6] = 0,
			[END_BAL_6] = 0,
			[BEGIN_BAL_7] = 0,
			[MUT_VALUE_7] = 0,
			[END_BAL_7] = 0,
			[BEGIN_BAL_8] = 0,
			[MUT_VALUE_8] = 0,
			[END_BAL_8] = 0,
			[BEGIN_BAL_9] = 0,
			[MUT_VALUE_9] = 0,
			[END_BAL_9] = 0,
			[BEGIN_BAL_10] = 0,
			[MUT_VALUE_10] = 0,
			[END_BAL_10] = 0,
			[BEGIN_BAL_11] = 0,
			[MUT_VALUE_11] = 0,
			[END_BAL_11] = 0,
			[BEGIN_BAL_12] = 0,
			[MUT_VALUE_12] = 0,
			[END_BAL_12] = 0,
			[DB_1] = 0,
			[CR_1] = 0,
			[DB_2] = 0,
			[CR_2] = 0,
			[DB_3] = 0,
			[CR_3] = 0,
			[DB_4] = 0,
			[CR_4] = 0,
			[DB_5] = 0,
			[CR_5] = 0,
			[DB_6] = 0,
			[CR_6] = 0,
			[DB_7] = 0,
			[CR_7] = 0,
			[DB_8] = 0,
			[CR_8] = 0,
			[DB_9] = 0,
			[CR_9] = 0,
			[DB_10] = 0,
			[CR_10] = 0,
			[DB_11] = 0,
			[CR_11] = 0,
			[DB_12] = 0,
			[CR_12] = 0,
			[MODIFY_DATE] = '2020-10-30',
			[MODIFY_USER] = NULL
		WHERE [COA_CODE] = @coa

      FETCH NEXT FROM @MyCursor 
      INTO @coa
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;

--SELECT * FROM AKT.dbo.TRX_SALDO 
--WHERE CREATE_DATE = '2020-10-13'

--WHERE  = '999999999999'

--SELECT * FROM COA_MST WHERE COA_CODE = '110011100000'

