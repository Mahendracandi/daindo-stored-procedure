USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[CLOSING_EOD_FISCAL_OPEN]    Script Date: 12/8/2020 14:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_CLOSING_EOD_ACTIVE_BACKDATE] @BranchCode nvarchar(30),
@SystemDate date,
@UserID nvarchar(24),
@FlagSuccess char(1) OUTPUT
AS
BEGIN
	BEGIN TRY
        SET NOCOUNT ON;
        DECLARE @vMaxSeq_ACC int,
                @vCutoffPeriodCollection int;
        SELECT
            @vMaxSeq_ACC = ISNULL(MAX(SEQUENCE_NO), 0) + 1
        FROM EOD_LOG
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD_ACTIVE_BACKDATE ';
        IF @vMaxSeq_ACC = 1
        BEGIN
            INSERT INTO EOD_LOG (BRANCH_CODE,
            CURRENT_TRANSACTION_DATE,
            SEQUENCE_NO,
            EOD_JOB_TYPE,
            [DESCRIPTION],
            START_TIME,
            END_TIME,
            [ERROR_MESSAGE],
            CREATE_USER,
            CREATE_DATE,
            START_DATE,
            SR_NO)
                VALUES (@BranchCode, @SystemDate, @vMaxSeq_ACC, 'SAL', 'SP_CLOSING_EOD_ACTIVE_BACKDATE', GETDATE(), NULL, NULL, 'EOD', GETDATE(), GETDATE(), 16);
        END;
        ELSE
        BEGIN
            UPDATE EOD_LOG
            SET 
                START_DATE = GETDATE(),
                START_TIME = GETDATE(),
                END_DATE = NULL,
                END_TIME = NULL,
                MODIFY_USER = 'EOD',
                MODIFY_DATE = GETDATE(),
                SEQUENCE_NO = @vMaxSeq_ACC,
                SR_NO = 6
            WHERE BRANCH_CODE = @BranchCode
            AND CURRENT_TRANSACTION_DATE = @SystemDate
            AND EOD_JOB_TYPE = 'SAL'
            AND SEQUENCE_NO = @vMaxSeq_ACC - 1
            AND [DESCRIPTION] = 'SP_CLOSING_EOD_ACTIVE_BACKDATE';
        END;


        DECLARE @RowCount int;
        DECLARE @BranchCode1 nvarchar(30),
                @OutletCode nvarchar(30),
                @JournalNo nvarchar(30),
                @TransactionDate date,
                @AccountCode nvarchar(30),
                @PosJournal nvarchar(1),
                @CurrencyCode nvarchar(3),
                @Amount numeric(15, 2),
                @SeqNo numeric(4),
                @MonthPeriod numeric(2),
                @YearPeriod nvarchar(4);
        DECLARE @MutValueToday numeric(15, 2),
                @DebitValueToday numeric(15, 2),
                @CreditValueToday numeric(15, 2),
                @MutValueEx numeric(15, 2),
                @DebitValueEx numeric(15, 2),
                @CreditValueEx numeric(15, 2),
                @ColumnBeginEx numeric(15, 2),
                @ColumnBegin nvarchar(max),
                @ColumnEnd nvarchar(max),
                @ColumnMut nvarchar(max),
                @ColumnDebit nvarchar(max),
                @ColumnCredit nvarchar(max),
                @MutValue numeric(15, 2),
                @EndBal numeric(15, 2),
                @DebitValue numeric(15, 2),
                @CreditValue numeric(15, 2);
        DECLARE @AccountCode2 nvarchar(30),
                @Level nvarchar(2),
                @CoaAcum nvarchar(30),
                @CoaAcumLevel nvarchar(2);
        DECLARE @SqlStmt nvarchar(max),
                @ParmDefinition nvarchar(max),
                @sql nvarchar(max);
        DECLARE @EndBalDtl numeric(15, 2),
                @MutValueDtl numeric(15, 2),
                @DebitValueDtl numeric(15, 2),
                @CreditValueDtl numeric(15, 2);
        DECLARE @ColumnBeginBal nvarchar(100),
                @ColumnEndBal nvarchar(100),
                @ColumnMutValue nvarchar(100),
                @ColumnDebitValue nvarchar(100),
                @ColumnCreditValue nvarchar(100);
        DECLARE @Count numeric(5);
        DECLARE @NextBal numeric(10),
                @AccountCode1 nvarchar(30),
                @pendapatanValue numeric(15, 2),
                @bebanValue numeric(15, 2),
                @labaValue numeric(15, 2),
                @accountLaba nvarchar(30),
                @currMonth numeric(2),
                @currYear numeric(4);
        DECLARE @pendapatanDebit numeric(15, 2),
                @pendapatanCredit numeric(15, 2),
                @bebanDebit numeric(15, 2),
                @bebanCredit numeric(15, 2);
        DECLARE @StartDate date,
                @EndDate date;
        DECLARE @PLDebit numeric(15,2),
                @PLCredit numeric(15,2),
                @PLEndBal numeric(15,2);
        DECLARE @incomeTaxMut numeric(15, 2),
                @incomeTaxDebit numeric(15, 2),
                @incomeTaxCredit numeric(15, 2);
        DECLARE @fromDate date;
        DECLARE @toDate date;
        DECLARE @totalMutValue numeric(15,2),
                @totalEndBal numeric(15,2),
                @totalDB numeric(15,2),
                @totalCR numeric(15,2)
        DECLARE @coaCurrentYearEarnings VARCHAR (30) = '220009000001';
        DECLARE @coaRetainedEarnings VARCHAR (30) = '220008000001';
        DECLARE @coaPLSummary VARCHAR(30) = '910000000000';
        DECLARE @mutValueCurrentYearEarnings numeric(15,2);

		print '-----------------------------------'
        print '## START ACTIVE BACKDATE SYSTEM ##'
		print '-----------------------------------'

        IF OBJECT_ID('tempdb..#ActiveCoaValue') IS NOT NULL
        BEGIN
            DROP TABLE #ActiveCoaValue
        END

        IF OBJECT_ID('tempdb..#ActiveCoaSum') IS NOT NULL
        BEGIN
            DROP TABLE #ActiveCoaSum
        END

        IF OBJECT_ID('tempdb..#ActiveCoaJournal') IS NOT NULL
        BEGIN
            DROP TABLE #ActiveCoaJournal
        END

        CREATE TABLE #ActiveCoaValue
        (
            [COA_CODE] VARCHAR(20),
            MUT_VALUE numeric(15,2),
            DEBIT_VALUE numeric(15,2),
            CREDIT_VALUE numeric(15,2),
            TRANSACTION_DATE date,
            BRANCH_CODE nvarchar(30),
            OUTLET_CODE nvarchar(30)
        )

        CREATE TABLE #ActiveCoaSum
        (
            [COA_CODE] VARCHAR(20),
            MUT_VALUE numeric(15,2),
            DEBIT_VALUE numeric(15,2),
            CREDIT_VALUE numeric(15,2),
            TRANSACTION_DATE date,
            BRANCH_CODE nvarchar(30),
            OUTLET_CODE nvarchar(30)
        )

        CREATE TABLE #ActiveCoaJournal
        (
            ACCOUNT_CODE VARCHAR(20),
            BRANCH_CODE nvarchar(30),
            OUTLET_CODE nvarchar(30),
            JOURNAL_NO nvarchar(30),
            SEQ_NO numeric(4)
        )

        SELECT
            @currMonth = [MONTH],
            @currYear = [YEAR]
        FROM AKT.dbo.CALENDAR_FISCAL_MST
        WHERE [STATUS] = '1'

        DECLARE c_journal CURSOR LOCAL FORWARD_ONLY FOR
        SELECT
            A.BRANCH_CODE,
            A.JOURNAL_NO,
            A.OUTLET_CODE,
            A.VALUE_DATE,
            A.ACCOUNT_CODE AS ACCOUNT_CODE,
            A.POS_JOURNAL,
            A.CURRENCY_CODE,
            A.AMOUNT,
            A.SEQ_NO,
            B.[MONTH],
            B.[YEAR],
            (CASE
                WHEN A.POS_JOURNAL = 'D' THEN A.AMOUNT
                ELSE 0
            END) AS DEBIT,
            (CASE
                WHEN A.POS_JOURNAL = 'C' THEN A.AMOUNT
                ELSE 0
            END) AS CREDIT
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE < @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('1')
        ORDER BY A.TRANSACTION_DATE ASC;

        OPEN c_journal
        WHILE 1 = 1
        BEGIN
            FETCH c_journal INTO @BranchCode1, @JournalNo, @OutletCode, @TransactionDate,
            @AccountCode, @PosJournal, @CurrencyCode, @Amount, @SeqNo, @MonthPeriod,
            @YearPeriod, @DebitValueToday, @CreditValueToday

            IF @@fetch_status = -1
                BREAK;

            INSERT INTO #ActiveCoaJournal
            SELECT
                @AccountCode, @BranchCode1, @OutletCode, @JournalNo, @SeqNo

            IF @PosJournal = 'D'
            BEGIN
                -- amount using positive number'
                SET @MutValueToday = @Amount;
            END;
            ELSE
            BEGIN
                -- amount using negative number'
                SET @MutValueToday = -1 * @Amount;
            END;

            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVEL_
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @AccountCode
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL_ + 1 AS LEVELS
            FROM COA_MST,
                 COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 1)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;
                
                IF @@FETCH_STATUS = -1
                    BREAK

                INSERT INTO #ActiveCoaValue
                SELECT
                    @AccountCode2, @MutValueToday, @DebitValueToday, @CreditValueToday, @TransactionDate, @BranchCode1, @OutletCode

                SET @CoaAcumLevel = NULL -- restart variable
                SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'
                
                IF (@CoaAcumLevel = '1')
                BEGIN
                    
                    SELECT
                        @AccountCode1 = ISNULL(COA_CODE, ' ')
                    FROM COA_MST
                    WHERE COA_CODE = @CoaAcum
                    AND ACTIVE_STATUS = 1;

                    IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
                    BEGIN

                        INSERT INTO #ActiveCoaValue
                        SELECT  @AccountCode1, @MutValueToday, @DebitValueToday, @CreditValueToday, @TransactionDate, @BranchCode1, @OutletCode
                    END
                END

            -- end looping coa level
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL

        -- end looping jurnal
        END
        CLOSE c_journal
        DEALLOCATE c_journal

        IF (EXISTS (SELECT 1 FROM #ActiveCoaValue))
        BEGIN

            INSERT INTO #ActiveCoaSum
            SELECT 
                COA_CODE,
                SUM(MUT_VALUE) AS MUT_VALUE,
                SUM(DEBIT_VALUE) AS DEBIT_VALUE,
                SUM(CREDIT_VALUE) AS CREDIT_VALUE,
                TRANSACTION_DATE,
                BRANCH_CODE,
                OUTLET_CODE
                FROM #ActiveCoaValue
                GROUP BY COA_CODE, TRANSACTION_DATE, BRANCH_CODE, OUTLET_CODE

            -- SELECT 'ActiveCoaValue' AS OPEN_COA_VALUE, * FROM #ActiveCoaValue
            -- SELECT 'ActiveCoaSum' AS OPEN_COA_VALUE, * FROM #ActiveCoaSum

            -- get account laba profit/loss
            BEGIN
                CREATE TABLE #ActiveProfitLoss
                (
                    [COA_CODE] VARCHAR(20)
                )
                
                SELECT
                    @accountLaba = VALUE
                FROM GLOBAL_PARAMETER_MST
                WHERE CONDITION = 'ACCOUNT_TEMP'
                AND SUB_CONDITION = '002';

                DECLARE C_COA_LEVEL CURSOR LOCAL FOR
                WITH COA_LEVEL AS (SELECT
                    COA_MST.COA_CODE,
                    COA_MST.LEVEL,
                    COA_MST.COA_ACCUM,
                    1 AS LEVELS
                FROM COA_MST
                WHERE COA_MST.COA_CODE = @accountLaba
                AND ACTIVE_STATUS = 1
                UNION ALL
                SELECT
                    COA_MST.COA_CODE,
                    COA_MST.LEVEL,
                    COA_MST.COA_ACCUM,
                    COA_LEVEL.LEVEL + 1 AS LEVELS
                FROM COA_MST,
                        COA_LEVEL
                WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
                AND ACTIVE_STATUS = 1
                AND COA_MST.LEVEL > 0)

                SELECT DISTINCT
                    COA_LEVEL.COA_CODE,
                    COA_LEVEL.LEVEL,
                    COA_LEVEL.COA_ACCUM
                FROM COA_LEVEL
                WHERE COA_LEVEL.LEVEL > 1
                ORDER BY COA_LEVEL.LEVEL DESC;

                OPEN C_COA_LEVEL
                WHILE 1 = 1
                BEGIN
                    FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                    IF @@FETCH_STATUS = -1
                        BREAK

                    INSERT INTO #ActiveProfitLoss
                    SELECT  @AccountCode2

                    SET @CoaAcumLevel = NULL -- restart variable
                    SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                    WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'
                    
                    IF (@CoaAcumLevel = '1')
                    BEGIN
                        SELECT
                            @AccountCode1 = ISNULL(COA_CODE, ' ')
                        FROM COA_MST
                        WHERE COA_CODE = @CoaAcum
                        AND ACTIVE_STATUS = 1;

                        IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
                        BEGIN
                            INSERT INTO #ActiveProfitLoss
                            SELECT  @AccountCode1
                        END
                    END
                END
                CLOSE C_COA_LEVEL
                DEALLOCATE C_COA_LEVEL
            -- end get account laba profit/loss
            END

            -- update active trx_saldo_dtl
            BEGIN

                SET @StartDate = DATEADD(DAY, 1, EOMONTH(@SystemDate, -1)); -- get first day of month
                SET @EndDate = @SystemDate; -- get current date

                PRINT 'Start update active trx_saldo_dtl. StartDate: ' + CAST(@StartDate AS VARCHAR) + ' EndDate: ' + CAST(@EndDate AS VARCHAR) 

                WHILE (@StartDate <= @EndDate)
                BEGIN
                    -- update active month TRX_SALDO_DTL
                    PRINT 'Execute StartDate: ' + CAST(@StartDate AS VARCHAR)
                    BEGIN 
                        PRINT 'update active trx_saldo_dtl. Saldo date: ' + CAST(@StartDate AS VARCHAR)
                        UPDATE DTL SET
                            BEGIN_BAL = (
                                SELECT END_BAL
                                FROM 
                                    AKT.dbo.TRX_SALDO_DTL 
                                WHERE
                                    COA_CODE = DTL.COA_CODE AND
                                    SALDO_DATE = DATEADD(DAY, -1, @StartDate) AND
                                    OUTLET_CODE = DTL.OUTLET_CODE AND BRANCH_CODE = DTL.BRANCH_CODE
                            ),
                            MUT_VALUE = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (DTL.MUT_VALUE + ISNULL(T.MUT_VALUE, 0))
                                    ELSE DTL.MUT_VALUE
                                END
                            ), 
                            END_BAL =  (
                                (
                                    -- get end bal previous day
                                    SELECT END_BAL
                                    FROM 
                                        AKT.dbo.TRX_SALDO_DTL 
                                    WHERE
                                        COA_CODE = DTL.COA_CODE AND
                                        SALDO_DATE = DATEADD(DAY, -1, @StartDate) AND
                                        OUTLET_CODE = DTL.OUTLET_CODE AND BRANCH_CODE = DTL.BRANCH_CODE
                                ) + (
                                    -- add by start date mut value plus mut value from jurnal status open
                                    CASE 
                                        WHEN DTL.COA_CODE = T.COA_CODE THEN (DTL.MUT_VALUE + ISNULL(T.MUT_VALUE, 0))
                                        ELSE DTL.MUT_VALUE
                                    END
                                )
                            ),
                            DB = (
                                CASE
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN ((DTL.DB + ISNULL(T.DEBIT_VALUE, 0)))
                                ELSE DTL.DB
                                END
                            ),
                            CR = (
                                CASE
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN ((DTL.CR + ISNULL(T.CREDIT_VALUE, 0)))
                                ELSE DTL.CR
                                END
                            ),
                            MODIFY_USER = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN ('BACK-SYSTEM')
                                    ELSE DTL.MODIFY_USER
                                END
                            ),
                            MODIFY_DATE = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (SELECT GETDATE()) 
                                    ELSE DTL.MODIFY_DATE
                                END
                            )
                        FROM AKT.dbo.TRX_SALDO_DTL DTL
                        LEFT JOIN #ActiveCoaSum T ON DTL.COA_CODE = T.COA_CODE 
                        AND DTL.BRANCH_CODE = T.BRANCH_CODE
                        AND DTL.OUTLET_CODE = T.OUTLET_CODE 
                        AND DTL.SALDO_DATE = T.TRANSACTION_DATE
                        WHERE 
                        DTL.SALDO_DATE = @StartDate
                    
                    -- end update active month TRX_SALDO_DTL
                    END

                    -- update active month trx_detail PROFIT/LOSS
                    IF EXISTS(
                            SELECT COA_CODE 
                            FROM #ActiveCoaSum 
                            WHERE TRANSACTION_DATE = @StartDate 
                                AND (COA_CODE LIKE '3%' OR COA_CODE LIKE '4%' OR COA_CODE LIKE '5%')
                    )
                    BEGIN 
                        PRINT 'Prepare profit/loss active trx_saldo_dtl. Saldo date: ' + CAST(@StartDate AS VARCHAR)
                        SELECT
                            @pendapatanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                            @pendapatanDebit = ISNULL(SUM(A.DB), 0),
                            @pendapatanCredit = ISNULL(SUM(A.CR), 0)
                        FROM AKT.dbo.TRX_SALDO_DTL AS A,
                            COA_MST AS B
                        WHERE A.BRANCH_CODE = @BranchCode
                        AND A.OUTLET_CODE = @OutletCode
                        AND A.SALDO_DATE = @StartDate
                        AND A.COA_CODE = B.COA_CODE
                        AND B.HEADER_DETAIL = 'D'
                        AND B.ACTIVE_STATUS = 1
                        AND B.COA_CODE LIKE '3%'

                        SELECT
                            @bebanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                            @bebanDebit = ISNULL(SUM(A.DB), 0),
                            @bebanCredit = ISNULL(SUM(A.CR), 0)
                        FROM AKT.dbo.TRX_SALDO_DTL AS A,
                            COA_MST AS B
                        WHERE A.BRANCH_CODE = @BranchCode
                        AND A.OUTLET_CODE = @OutletCode
                        AND A.SALDO_DATE = @StartDate
                        AND A.COA_CODE = B.COA_CODE
                        AND B.HEADER_DETAIL = 'D'
                        AND B.ACTIVE_STATUS = 1
                        AND B.COA_CODE LIKE '4%'

                        BEGIN
                            SELECT
                                @incomeTaxMut = ISNULL(SUM(A.MUT_VALUE), 0),
                                @incomeTaxDebit = ISNULL(SUM(A.DB), 0),
                                @incomeTaxCredit = ISNULL(SUM(A.CR), 0)
                            FROM AKT.dbo.TRX_SALDO_DTL AS A,
                                COA_MST AS B
                            WHERE A.BRANCH_CODE = @BranchCode
                            AND A.OUTLET_CODE = @OutletCode
                            AND A.SALDO_DATE = @StartDate
                            AND A.COA_CODE = B.COA_CODE
                            AND B.HEADER_DETAIL = 'D'
                            AND B.ACTIVE_STATUS = 1
                            AND B.COA_CODE LIKE '5%'
                        END

                        SET @labaValue = @pendapatanValue + @bebanValue + @incomeTaxMut;
                        SET @PLDebit = @pendapatanDebit + @bebanDebit + @incomeTaxDebit;
                        SET @PLCredit = @pendapatanCredit + @bebanCredit + @incomeTaxCredit;

                        PRINT 'Update profit/loss active trx_saldo_dtl. Saldo date: ' + CAST(@StartDate AS VARCHAR)
                        UPDATE DTL SET
                            BEGIN_BAL = (
                                SELECT END_BAL
                                FROM 
                                    AKT.dbo.TRX_SALDO_DTL 
                                WHERE
                                    COA_CODE = DTL.COA_CODE AND
                                    SALDO_DATE = DATEADD(DAY, -1, @StartDate) AND
                                    OUTLET_CODE = DTL.OUTLET_CODE AND BRANCH_CODE = DTL.BRANCH_CODE
                            ),
                            MUT_VALUE = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (ISNULL(@labaValue, 0))
                                    ELSE DTL.MUT_VALUE
                                END
                            ), 
                            END_BAL =  (
                                (
                                    -- get end bal previous day
                                    SELECT END_BAL
                                    FROM 
                                        AKT.dbo.TRX_SALDO_DTL 
                                    WHERE
                                        COA_CODE = DTL.COA_CODE AND
                                        SALDO_DATE = DATEADD(DAY, -1, @StartDate) AND
                                        OUTLET_CODE = DTL.OUTLET_CODE AND BRANCH_CODE = DTL.BRANCH_CODE
                                )  
                                -- add by start date mut value plus mut value from jurnal status open
                                + (
                                    CASE 
                                        WHEN DTL.COA_CODE = T.COA_CODE THEN (ISNULL(@labaValue, 0))
                                        ELSE DTL.MUT_VALUE
                                    END
                                )
                            ),
                            DB = (
                                CASE
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (ISNULL(@PLDebit, 0))
                                ELSE DTL.DB
                                END
                            ),
                            CR = (
                                CASE
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (ISNULL(@PLCredit, 0))
                                ELSE DTL.CR
                                END
                            ),
                            MODIFY_USER = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN ('BACK-SYSTEM')
                                    ELSE DTL.MODIFY_USER
                                END
                            ),
                            MODIFY_DATE = (
                                CASE 
                                    WHEN DTL.COA_CODE = T.COA_CODE THEN (SELECT GETDATE()) 
                                    ELSE DTL.MODIFY_DATE
                                END
                            )
                        FROM AKT.dbo.TRX_SALDO_DTL DTL
                        INNER JOIN #ActiveProfitLoss T ON DTL.COA_CODE = T.COA_CODE 
                        WHERE DTL.SALDO_DATE = @StartDate

                        PRINT 'Finish updating profit/loss active trx_saldo_dtl. Saldo date: ' + CAST(@StartDate AS VARCHAR)
                    -- end update active month trx_detail PROFIT/LOSS
                    END

                    SET @StartDate = DATEADD(DAY, 1, @StartDate);
                -- end looping active date
                END

                
                PRINT 'Finish updating active trx_saldo_dtl.'
            -- end update active trx_saldo_dtl
            END

            -- update active trx_saldo
            BEGIN 

                SET @fromDate = DATEADD(DAY, 1, EOMONTH(@SystemDate, -1)); -- get first day of month
                -- SET @toDate = DATEADD(DAY, -1, @SystemDate) -- get current date -1
                SET @toDate = @SystemDate -- get current date

                -- PRINT 'update active trx_saldo. ' + CAST(@fromDate AS VARCHAR) + ' ' + CAST(@toDate AS VARCHAR)
                -- PRINT 'StartDate: ' + CAST(@StartDate AS VARCHAR)

                PRINT 'Prepare update active trx_saldo. Get sum trx_saldo_dtl from saldo_date: ' + CAST(@fromDate AS VARCHAR) + ' to saldo_date: ' + CAST(@toDate AS VARCHAR) 

                SET @MonthPeriod = MONTH(@SystemDate); 

                PRINT 'Set MonthPeriod: ' + CAST(@MonthPeriod AS VARCHAR)

                SET @ColumnBegin = 'BEGIN_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
                SET @ColumnEnd = 'END_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
                SET @ColumnMut = 'MUT_VALUE_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
                SET @ColumnDebit = 'DB_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
                SET @ColumnCredit = 'CR_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), ''); 

                PRINT 'Start update active trx_saldo.'
                SET @sql = 
                    'UPDATE TRX SET
                        ' + @ColumnBegin + ' = (
                            ISNULL(T.BEGIN_BAL, 0)
                        ),
                        ' + @ColumnMut + ' = (
                            ISNULL(T.MUT_VALUE, 0)
                        ),
                        ' + @ColumnEnd + ' = (
                            ISNULL(T.END_BAL, 0)
                        ),
                        ' + @ColumnDebit + ' = (
                            ISNULL(T.DB, 0)
                        ),
                        ' + @ColumnCredit + ' = (
                            ISNULL(T.CR, 0)
                        )
                    FROM AKT.dbo.TRX_SALDO TRX
                    LEFT JOIN (
                        SELECT DTL.COA_CODE, A.BEGIN_BAL, SUM(DTL.MUT_VALUE) AS MUT_VALUE, B.END_BAL, SUM(DTL.DB) AS DB, SUM(DTL.CR) AS CR
                        FROM AKT.dbo.TRX_SALDO_DTL DTL
                        INNER JOIN (
                            SELECT COA_CODE, BEGIN_BAL FROM AKT.dbo.TRX_SALDO_DTL 
                            WHERE SALDO_DATE = DATEADD(DAY, 1, EOMONTH(@toDate, -1))
                        ) AS A ON DTL.COA_CODE = A.COA_CODE
                        INNER JOIN (
                            SELECT COA_CODE, END_BAL FROM AKT.dbo.TRX_SALDO_DTL 
                            WHERE SALDO_DATE = @toDate
                        ) AS B ON DTL.COA_CODE = B.COA_CODE 
                        WHERE SALDO_DATE BETWEEN @fromDate AND @toDate
                        GROUP BY DTL.COA_CODE, B.END_BAL, A.BEGIN_BAL
                    ) AS T ON TRX.COA_CODE = T.COA_CODE
                    WHERE TRX.YEAR_PERIOD = CONVERT(varchar(4), YEAR(@toDate))';

                SET @ParmDefinition = N'@StartDate date, @fromDate date, @toDate date';
                EXEC sp_executesql @sql, @ParmDefinition, @StartDate = @StartDate, @fromDate = @fromDate,@toDate = @toDate;

                PRINT 'Finish updating active trx_saldo.'
            -- end update active trx_saldo
            END

            -- update process flag
            BEGIN
                UPDATE CF 
                SET PROCESS_FLAG = 1,
                    MODIFY_USER = @UserID
                FROM CF_JOURNAL_INFO AS CF
                INNER JOIN #ActiveCoaJournal T ON 
                    CF.ACCOUNT_CODE = T.ACCOUNT_CODE AND
                    CF.BRANCH_CODE = T.BRANCH_CODE AND
                    CF.OUTLET_CODE = T.OUTLET_CODE AND
                    CF.JOURNAL_NO = T.JOURNAL_NO AND
                    CF.SEQ_NO = T.SEQ_NO
                WHERE CF.PROCESS_FLAG = 0
                -- AND BRANCH_CODE = @BranchCode
                -- AND OUTLET_CODE = @OutletCode
                -- AND JOURNAL_NO = @JournalNo
                -- AND ACCOUNT_CODE = @AccountCode
                -- AND SEQ_NO = @SeqNo; 

            -- end update process flag
            END


            IF OBJECT_ID('tempdb..#ActiveProfitLoss') IS NOT NULL
            BEGIN
                DROP TABLE #ActiveProfitLoss
            END

            IF OBJECT_ID('tempdb..#ActiveCoaJournal') IS NOT NULL
            BEGIN
                DROP TABLE #ActiveCoaJournal
            END
            
        -- end exists condition
        END

        DROP TABLE #ActiveCoaValue
        DROP TABLE #ActiveCoaSum

        SET @FlagSuccess = 'Y';

        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = 'Y',
            [ERROR_MESSAGE] = 'SUCCESS',
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD_ACTIVE_BACKDATE';

    END TRY
    BEGIN CATCH
        SET @FlagSuccess = 'N';

        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = @FlagSuccess,
            [ERROR_MESSAGE] = CONVERT(nvarchar(5), ERROR_LINE()) + ': ' + ERROR_MESSAGE(),
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD_ACTIVE_BACKDATE';

        SELECT
            ERROR_NUMBER() number,
            ERROR_LINE() line,
            ERROR_MESSAGE() msg
    END CATCH

    print '-----------------------------------'
    print '## FINISH ACTIVE BACKDATE SYSTEM ##'
    print '-----------------------------------'

END
