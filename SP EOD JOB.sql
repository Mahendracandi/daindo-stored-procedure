USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[SP_EOD_JOB]    Script Date: 2/12/2021 10:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_EOD_JOB] @pBatch NVARCHAR(10),
								   @pJobId NCHAR(1)
AS
    BEGIN
        PRINT '1'

        DECLARE @vFlagAging NVARCHAR(1), @CEOD_BRANCH_CODE NVARCHAR(10), @c1_CURRENT_TRANSACTION_DATE DATE, @c1_START_COLLECTION_DATE DATE, @c1_END_COLLECTION_DATE DATE, @pUserId NVARCHAR(12), @vMaxSeq_EOD INT, @vEOD_Job NVARCHAR(10), @vEOD_Type NVARCHAR(10), @vFlag_Handling NVARCHAR(1), @vFlag_LPK NVARCHAR(1), @vFlag_WL NVARCHAR(1), @vFlag_CashierBalance NVARCHAR(1), @vFlag_Unearn NVARCHAR(1), @vEODFin INT, @vCurrent_Transaction_Date DATE, @vFlag_Accounting NVARCHAR(1), @vFlag_Journal NVARCHAR(1), @vFlag_Contract_Balance NVARCHAR(1), @vFlag_FPD NVARCHAR(1), @vFlag_Recap_Monthly NVARCHAR(1), @vFlag_Recap_Daily NVARCHAR(1), @vFlag_Print_Receipt NVARCHAR(1), @vFlag_Moved_Collection NVARCHAR(1), @vFlag_Return_Daily NVARCHAR(1), @vFlag_Daily_Collection NVARCHAR(1), @vFlag_BadCustomer NVARCHAR(1), @vFlag_Balance_Postcode NVARCHAR(1), @flag_eom NVARCHAR(1), @flag_rpt_monthly_recap_repayment NVARCHAR(1), @flag_RFD NVARCHAR(1), @vREGION_CODE NVARCHAR(10), @vOUTLET_CODE NVARCHAR(10), @vHari INT, @DLAST_EOD_DATE DATETIME, @vSeq_EOD INT, 
		@flag_MColl nvarchar(1);
        SET @vHari = DATEPART(dw, GETDATE());
        SET @pUserId = 'EOD-SYSTEM';
        DECLARE C1_BRANCH_EOD CURSOR
        FOR
            SELECT a.BRANCH_CODE,
                    a.CURRENT_TRANSACTION_DATE,
                    a.START_COLLECTION_DATE,
                    a.END_COLLECTION_DATE
            --INTO #C1_BRANCH_EOD
            FROM BRANCH_EOD_MST a,
                 EOD_BATCH_MST b
            WHERE a.BRANCH_CODE = b.BRANCH_CODE
                AND a.EOD_READY_FLAG = 0
                AND b.BATCH_NO = @pBatch
                 AND b.JOB_ID = @pJobId
            ORDER BY b.SEQ_NO DESC;
        SET @vEOD_Job = 'EOD-START';
        SET @vEOD_Type = 'EOD';
        
        PRINT '2'

        -- ADD BARU 26-Juli-2017 Danu
        DECLARE @vBatchNoBom nvarchar(2), @vBatchNoEom nvarchar(2)
        SELECT @vBatchNoEom = VALUE 
        FROM GLOBAL_PARAMETER_MST
        WHERE CONDITION = 'BATCH_EOM'
        AND ACTIVE_STATUS ='1'
         
        SELECT @vBatchNoBom = VALUE 
        FROM GLOBAL_PARAMETER_MST
        WHERE CONDITION = 'BATCH_BOM'
        AND ACTIVE_STATUS ='1'
         
        PRINT '3'
        OPEN C1_BRANCH_EOD;
        FETCH NEXT FROM C1_BRANCH_EOD INTO @CEOD_BRANCH_CODE, @c1_CURRENT_TRANSACTION_DATE, @c1_START_COLLECTION_DATE, @c1_END_COLLECTION_DATE;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            PRINT '4'

            DECLARE @vCurrentTransactionDatePlus date
            SET @vCurrentTransactionDatePlus = DATEADD(DAY,1,@c1_CURRENT_TRANSACTION_DATE)
                
            --add by yoga
            --force all user to logout
            DELETE USER_LOGIN_INFO;

            UPDATE BRANCH_EOD_MST
                SET
                    EOD_READY_FLAG = 1,
                    MODIFY_USER = @pUserId,
                    MODIFY_DATE = GETDATE()
            WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                    AND EOD_READY_FLAG = 0
                    AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE;
            SELECT @vSeq_EOD = ISNULL(MAX(SEQUENCE_NO), 0) + 1
            FROM EOD_LOG
            WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                    AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE
                    AND EOD_JOB_TYPE = @vEOD_Type
                    AND [DESCRIPTION] = @vEOD_Job;

            PRINT '5'

            IF @vSeq_EOD = 1
            BEGIN
                PRINT '6'

                INSERT INTO EOD_LOG
                    (BRANCH_CODE,
                    CURRENT_TRANSACTION_DATE,
                    SEQUENCE_NO,
                    EOD_JOB_TYPE,
                    [DESCRIPTION],
                    START_TIME,
                    END_TIME,
                    [ERROR_MESSAGE],
                    CREATE_USER,
                    CREATE_DATE,
                    START_DATE,
                    SR_NO
                    )
                    VALUES
                    (@CEOD_BRANCH_CODE,
                    @c1_CURRENT_TRANSACTION_DATE,
                    @vSeq_EOD,
                    @vEOD_Type,
                    @vEOD_Job,
                    GETDATE(),
                    NULL,
                    NULL,
                    'EOD',
                    GETDATE(),
                    GETDATE(),
                    0
                    );
            END;
            ELSE
            BEGIN
                PRINT '7'
                UPDATE EOD_LOG
                    SET
                        START_DATE = GETDATE(),
                        START_TIME = GETDATE(),
                        END_TIME = NULL,
                        MODIFY_USER = 'EOD',
                        MODIFY_DATE = GETDATE(),
                        SEQUENCE_NO = @vSeq_EOD,
                        SR_NO = 0
                WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                        AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE
                        AND EOD_JOB_TYPE = @vEOD_Type
                        AND SEQUENCE_NO = @vSeq_EOD - 1
                        AND [DESCRIPTION] = '@vEOD_Job';
            END;
            
            PRINT '8'

            DELETE
            FROM USER_LOGIN_INFO
            WHERE [USER_ID] IN
            (
                SELECT [USER_ID]
                FROM LEASE_USER
                WHERE EMPLOYEE_ID IN
                (
                    SELECT EMPLOYEE_ID
                    FROM EMPLOYEE_MST
                    WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                )
            );

            PRINT '8.1'
            
            EXECUTE dbo.SP_COLLECTION_AGING
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @pUserId,
                    @vFlagAging OUTPUT;
            
            PRINT '9'
            
            IF @vFlagAging = 'Y'
            BEGIN
                PRINT '10'

                EXECUTE dbo.SP_EOD_COLLECTION_BUCKET
                        @c1_CURRENT_TRANSACTION_DATE,
                        @CEOD_BRANCH_CODE,
                        @vFlag_LPK OUTPUT;
                EXECUTE dbo.SP_EOD_HANDLING_COLLECTION
                        @c1_CURRENT_TRANSACTION_DATE,
                        @CEOD_BRANCH_CODE,
                        @vFlag_Handling OUTPUT;
                    --EXECUTE dbo.SP_EOD_WARNING_LETTER
                    --        @c1_CURRENT_TRANSACTION_DATE,
                    --        @CEOD_BRANCH_CODE,
                    --        @vFlag_WL OUTPUT;
                --MOBILE COLLECTION
                --IF(@CEOD_BRANCH_CODE = '1002')
                --BEGIN
                --EXECUTE dbo.SP_MOBILE_COLLECTOR
                --		@c1_CURRENT_TRANSACTION_DATE,
                --		@CEOD_BRANCH_CODE,
                --		@flag_MColl OUTPUT;
                --END;
                --EXECUTE dbo.SP_EOD_INTERNAL_BAD_CUSTOMER @CEOD_BRANCH_CODE, @c1_CURRENT_TRANSACTION_DATE,@vFlag_BadCustomer  output
            END;
            
            PRINT '11'

            EXECUTE dbo.SP_EOD_CASHIER_BALANCE
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @vFlag_CashierBalance OUTPUT;
            
            PRINT '12'

            IF @c1_CURRENT_TRANSACTION_DATE = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @c1_CURRENT_TRANSACTION_DATE) + 1, 0))
            --EOMONTH(@c1_CURRENT_TRANSACTION_DATE)
            BEGIN
                PRINT '13'
                EXECUTE SP_UNEARN
                        @c1_CURRENT_TRANSACTION_DATE,
                        @CEOD_BRANCH_CODE,
                        @pUserId,
                        @vFlag_Unearn OUTPUT;
            END;

                        
            EXECUTE SP_EOD_ACCOUNTING_JOURNAL
                    @CEOD_BRANCH_CODE,
                    @c1_CURRENT_TRANSACTION_DATE,
                    @pUserId,
                    @vFlag_Accounting OUTPUT;

            -- CREATE REPORT
            /*   EXEC SP_RPT_EOD_CONTRACT_BALANCE
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @pUserId,
                --     @vFlag_Contract_Balance OUTPUT;*/
            PRINT '14'

            SET @DLAST_EOD_DATE = CONVERT(DATETIME, SUBSTRING(CONVERT(NVARCHAR(10), @c1_CURRENT_TRANSACTION_DATE), 6, 2)+'/'+SUBSTRING(CONVERT(NVARCHAR(10), @c1_CURRENT_TRANSACTION_DATE), 9, 2)+'/'+SUBSTRING(CONVERT(NVARCHAR(10), @c1_CURRENT_TRANSACTION_DATE), 1, 4)+' '+CONVERT(NVARCHAR(10), GETDATE(), 108));
            EXEC SP_RPT_AR_BALANCE_POSTCODE
                @c1_CURRENT_TRANSACTION_DATE,
                @CEOD_BRANCH_CODE,
                NULL,
                NULL,
                NULL,
                NULL,
                @pUserId,
                'Y',
                @DLAST_EOD_DATE,
                @vFlag_Balance_Postcode OUTPUT;
            
            PRINT '15'

            EXECUTE SP_RPT_DAILY_RECAP_REPAYMENT
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @vFlag_Recap_Daily OUTPUT;

            --remove filter by outlet
            --             DECLARE C_OUTLET CURSOR
            --             FOR
            --                 SELECT REGION_CODE,
            --                        COMPANY_CODE
            --                 FROM COMPANY_MST
            --                 WHERE BELONGS_TO = @CEOD_BRANCH_CODE
            --                       AND LEVEL_NO = '2';
            --             OPEN C_OUTLET;
            --             FETCH NEXT FROM C_OUTLET INTO @vREGION_CODE, @vOUTLET_CODE;
            --             WHILE @@FETCH_STATUS = 0
            --                 BEGIN
            --                     FETCH NEXT FROM C_OUTLET INTO @vREGION_CODE, @vOUTLET_CODE;
            --                 END;
            --             CLOSE C_OUTLET;
            --             DEALLOCATE C_OUTLET;

            PRINT '16'

            EXECUTE SP_RPT_CASHIER_MUTATION
                    @c1_CURRENT_TRANSACTION_DATE,
                    @c1_CURRENT_TRANSACTION_DATE,
                    @vREGION_CODE,
                    NULL,
                    NULL,
                    @CEOD_BRANCH_CODE,
                    NULL,
                    @pUserId,
                    'Y',
                    @DLAST_EOD_DATE;
            
            PRINT '17'
            
            EXEC SP_MONITORING_PRINT_RECEIPT
                @c1_CURRENT_TRANSACTION_DATE,
                @DLAST_EOD_DATE,
                @CEOD_BRANCH_CODE,
                @vFlag_Print_Receipt OUTPUT;

            PRINT '18'

            EXEC SP_RPT_RETURN_DAILY
                @c1_CURRENT_TRANSACTION_DATE,
                @c1_CURRENT_TRANSACTION_DATE,
                NULL,
                @CEOD_BRANCH_CODE,
                NULL,
                @vOUTLET_CODE,
                @vREGION_CODE,
                @pUserId,
                'Y',
                @DLAST_EOD_DATE,
                @vFlag_Return_Daily OUTPUT;
            
            PRINT '19'
            
            /*  EXEC SP_RPT_DAILY_MONITORING_COLLECTION
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @vFlag_Daily_Collection OUTPUT;  */ 	
                                    

            /*
                * Store Prosedure 
                *		[END OF DAY]
                *		[END OF MONTH]
                *		[END OF YEAR]
                * By Haidir : 16 JULY 2019
                */
            PRINT '20'
            PRINT EOMONTH(@c1_CURRENT_TRANSACTION_DATE)
            PRINT @c1_CURRENT_TRANSACTION_DATE 
            
            PRINT '21'

            IF  EOMONTH(@c1_CURRENT_TRANSACTION_DATE) = @c1_CURRENT_TRANSACTION_DATE
            BEGIN
                IF MONTH(@c1_CURRENT_TRANSACTION_DATE) = 12
                BEGIN
                    PRINT 'MASUK EOY';
                    PRINT @c1_CURRENT_TRANSACTION_DATE;
                    EXECUTE SP_CLOSING_EOY
                        @CEOD_BRANCH_CODE,
                        @c1_CURRENT_TRANSACTION_DATE,
                        @pUserId,
                        @vFlag_Accounting OUTPUT;
                END
                ELSE
                BEGIN
                    PRINT 'MASUK EOM';
                    EXECUTE SP_CLOSING_EOM
                            @CEOD_BRANCH_CODE,
                            @c1_CURRENT_TRANSACTION_DATE,
                            @pUserId,
                            @vFlag_Accounting OUTPUT;
                END
            END
            --ELSE IF @c1_CURRENT_TRANSACTION_DATE = DateAdd(mm, 11,  EOMONTH(@c1_END_COLLECTION_DATE))
            --	BEGIN
            --		PRINT 'MASUK EOY';
            --		PRINT @c1_CURRENT_TRANSACTION_DATE;
            --		EXECUTE SP_CLOSING_EOY
            --			@CEOD_BRANCH_CODE,
            --			@c1_CURRENT_TRANSACTION_DATE,
            --			@pUserId,
            --			@vFlag_Accounting OUTPUT;
            --	END
            ELSE
            BEGIN
                
                PRINT '22'

                PRINT 'MASUK EOD';
                EXECUTE SP_CLOSING_EOD
                    @CEOD_BRANCH_CODE,
                    @c1_CURRENT_TRANSACTION_DATE,
                    @pUserId,
                    @vFlag_Accounting OUTPUT;
            END
    
            --END REPORT
            
            --IF EOM
            
            PRINT '24'
            

            IF @c1_CURRENT_TRANSACTION_DATE = @c1_END_COLLECTION_DATE
            BEGIN
                PRINT '25'
                EXEC SP_EOM_AGING_COLLECTION
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @flag_eom OUTPUT;
                PRINT '26'
                EXEC SP_RPT_MONTHLY_RECAP_REPAYMENT
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    @flag_rpt_monthly_recap_repayment OUTPUT;
                PRINT '27'
                EXEC SP_RPT_FPD
                    @c1_CURRENT_TRANSACTION_DATE,
                    @CEOD_BRANCH_CODE,
                    'EOD',
                    @flag_RFD OUTPUT;
                PRINT '28'   
                -- ADD BARU 26-Juli-2017 Danu
                IF @vCurrentTransactionDatePlus = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @c1_CURRENT_TRANSACTION_DATE) + 1, 0))
                --EOMONTH(@c1_CURRENT_TRANSACTION_DATE) 
                OR @vCurrentTransactionDatePlus = @c1_END_COLLECTION_DATE
                    BEGIN
                        PRINT 'UPDATE EOD BATCH EOM : ' + @CEOD_BRANCH_CODE
                        UPDATE EOD_BATCH_MST SET BATCH_NO = @vBatchNoEom
                        WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                    END
                PRINT '29'
                UPDATE BRANCH_EOD_MST
                SET
                    START_COLLECTION_DATE = DATEADD(month, 1, START_COLLECTION_DATE),
                    END_COLLECTION_DATE = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, DATEADD(month, 1, END_COLLECTION_DATE)) + 1, 0)),
                    --EOMONTH(DATEADD(month, 1, END_COLLECTION_DATE)),
                    MODIFY_USER = @pUserId,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                    AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE
                    AND EOD_READY_FLAG = 1;
            END;

            PRINT '30'

            -- test
            -- SELECT @vFlagAging AS '@vFlagAging', @vFlag_Handling AS '@vFlag_Handling', @vFlag_LPK AS '@vFlag_LPK'

            IF @vFlagAging = 'Y'
            AND @vFlag_Handling = 'Y'
            AND @vFlag_LPK = 'Y'
            BEGIN
                PRINT '31'
                -- ADD BARU 26-Juli-2017 Danu
                IF @c1_CURRENT_TRANSACTION_DATE = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, @c1_CURRENT_TRANSACTION_DATE) + 1, 0))
                --EOMONTH(@c1_CURRENT_TRANSACTION_DATE) 
                OR @c1_CURRENT_TRANSACTION_DATE = @c1_END_COLLECTION_DATE
                BEGIN
                    PRINT 'UPDATE EOD BATCH BOM : ' + @CEOD_BRANCH_CODE
                    UPDATE EOD_BATCH_MST SET BATCH_NO = @vBatchNoBom
                    WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                END
                    
                PRINT '32'

                --  PRINT'UPDATE BRANCH_EOD_MST'
                UPDATE BRANCH_EOD_MST
                SET
                    LAST_EOD_DATE = @c1_CURRENT_TRANSACTION_DATE,
                    CURRENT_TRANSACTION_DATE = DATEADD(day, 1, @c1_CURRENT_TRANSACTION_DATE),
                    NEXT_EOD_DATE = DATEADD(day, 2, @c1_CURRENT_TRANSACTION_DATE),
                    EOD_READY_FLAG = 0,
                    MODIFY_USER = @pUserId,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                    AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE
                    AND EOD_READY_FLAG = 1;
                
                PRINT '33'
            END;

            PRINT '34'
            UPDATE EOD_LOG
            SET
                END_DATE = GETDATE(),
                END_TIME = GETDATE(),
                SUCCESS_FLAG = 'Y',
                [ERROR_MESSAGE] = 'SUCCESS',
                MODIFY_USER = 'EOD',
                MODIFY_DATE = GETDATE()
            WHERE BRANCH_CODE = @CEOD_BRANCH_CODE
                AND CURRENT_TRANSACTION_DATE = @c1_CURRENT_TRANSACTION_DATE
                AND SEQUENCE_NO = @vSeq_EOD
                AND EOD_JOB_TYPE = @vEOD_Type
                AND [DESCRIPTION] = @vEOD_Job;
            
            SET @vCurrentTransactionDatePlus = NULL
            
            FETCH NEXT FROM C1_BRANCH_EOD INTO @CEOD_BRANCH_CODE, @c1_CURRENT_TRANSACTION_DATE, @c1_START_COLLECTION_DATE, @c1_END_COLLECTION_DATE;
            SET @vMaxSeq_EOD = 0;
        END;   ---- End cursor #C1_BRANCH_EOD
        CLOSE C1_BRANCH_EOD;
        DEALLOCATE C1_BRANCH_EOD;
    END;
    PRINT '35'
