-- MONTHLY SUB GL --
-- TEST LAYOUT 2 --


--DECLARE @contract VARCHAR(20);
----DECLARE '202010' VARCHAR(10);

----SET '202010' = '202010';

--DECLARE c__contract CURSOR LOCAL FORWARD_ONLY FOR
--SELECT CONTRACT_NO FROM LAMP_2018.dbo.CF_CONTRACT_INFO A 
--ORDER BY CONTRACT_NO DESC
--OFFSET 0 ROW 
--FETCH FIRST 200 ROW ONLY

--OPEN c__contract
--WHILE 1 = 1
--BEGIN
	
--	FETCH c__contract INTO @contract

--	IF @@fetch_status = -1
--	BEGIN
--		BREAK;
--	END

--	print @contract;
	SELECT 
		CAST(C.CUSTOMER_NAME AS VARCHAR(MAX)) AS customerName,
		CAST(ISNULL(A.GOLIVE_DATE,'') AS VARCHAR(MAX)) AS exeDate,
		CAST(ISNULL(A.MATURITY_DATE,'') AS VARCHAR(MAX)) AS expDate,
		CAST(A.CONTRACT_NO AS VARCHAR(MAX))AS contractNo,
		ISNULL((
			SELECT 
			CASE
				WHEN (EXISTS(
						SELECT INSTALLMENT_AMT_PAID FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
						WHERE INSTALLMENT_NO <> 0 AND INSTALLMENT_AMT_PAID = '0.00' AND CONTRACT_NO = A.CONTRACT_NO)) 
				THEN (
					SELECT ISNULL(SUM(INSTALLMENT_AMT),0) 
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
					WHERE CONTRACT_NO =  A.CONTRACT_NO
						AND FORMAT(DUE_DATE, 'yyyyMM') > '202010'
				) ELSE (
					SELECT (
						SELECT IIF('202010' >= FORMAT(Y.DUE_DATE, 'yyyyMM'), 0, ISNULL(SUM(INSTALLMENT_AMT_PAID),0))
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
						WHERE CONTRACT_NO = Y.CONTRACT_NO
							AND FORMAT(DUE_DATE, 'yyyyMM') > '202010'
					)
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Y
					WHERE Y.CONTRACT_NO =  A.CONTRACT_NO
						AND Y.INSTALLMENT_NO <> 0 AND Y.INSTALLMENT_AMT <> '0.00'
					ORDER BY DUE_DATE DESC
					OFFSET 0 ROW 
					FETCH FIRST 1 ROW ONLY
				)
			END 
		),0) AS notDueRec,
		(
			SELECT DISTINCT 
				(SELECT ISNULL(SUM(INSTALLMENT_AMT),0)
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
					WHERE (
						PAY_STATUS = 0 AND 
						DAY_OVERDUE > 0 AND 
						CONTRACT_NO=A.CONTRACT_NO AND 
						FORMAT(DUE_DATE, 'yyyyMM') <= '202010')
					OR (
						PAY_STATUS = 3 AND 
						DAY_OVERDUE > 0 AND 
						CONTRACT_NO=A.CONTRACT_NO AND 
						FORMAT(DUE_DATE, 'yyyyMM') <= '202010' AND
						FORMAT(PAID_DATE, 'yyyyMM') > '202010')
				) - 
				ISNULL ( G.DEPOSIT_AMOUNT, 0 ) 
				+ ISNULL ((SELECT 
					CASE 
						WHEN MAX(FORMAT(DUE_DATE, 'yyyyMM')) = MAX(FORMAT(PAID_DATE, 'yyyyMM')) OR MAX(FORMAT(DUE_DATE, 'yyyyMM')) < MAX(FORMAT(PAID_DATE, 'yyyyMM')) 
						THEN 0 ELSE 
						SUM(INSTALLMENT_AMT) 
					END 
				FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
				WHERE 
					PAY_STATUS = 3 AND 
					CONTRACT_NO=A.CONTRACT_NO AND
					FORMAT(DUE_DATE, 'yyyyMM') > '202010' AND 
					PAID_DATE >= (
						SELECT DATEADD(DAY, DAY_OVERDUE, DUE_DATE)
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
						WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND
						FORMAT(DUE_DATE, 'yyyyMM') > FORMAT(PAID_DATE, 'yyyyMM')
						AND CONTRACT_NO=A.CONTRACT_NO AND 
						INSTALLMENT_NO <> 0
					) 
					AND PAID_DATE <= (
						SELECT DATEADD(DAY, DAY_OVERDUE, DUE_DATE)
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
						WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND
						FORMAT(DUE_DATE, 'yyyyMM') > FORMAT(PAID_DATE, 'yyyyMM')
						AND CONTRACT_NO=A.CONTRACT_NO AND 
						INSTALLMENT_NO <> 0
					)
				), 0) 
				- ISNULL((SELECT 
							CASE 
								WHEN MAX(FORMAT(XX.DUE_DATE, 'yyyyMM')) = MAX(FORMAT(XX.PAID_DATE, 'yyyyMM')) OR MAX(FORMAT(XX.DUE_DATE, 'yyyyMM')) > MAX(FORMAT(XX.PAID_DATE, 'yyyyMM')) THEN SUM(XX.INSTALLMENT_AMT)  ELSE 0
							END 
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE XX
						WHERE 
							XX.PAY_STATUS = 3 AND 
							FORMAT(XX.PAID_DATE, 'yyyyMM') = '202010' AND 
							MONTH(XX.PAID_DATE) <> MONTH(XX.DUE_DATE) AND 
							XX.CONTRACT_NO=A.CONTRACT_NO
				), 0)
			FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE AS CCP
			WHERE CCP.PAY_STATUS = 0 AND CCP.DAY_OVERDUE > 0 OR CCP.PAY_STATUS = 3
		) AS pastDueRec,
		ISNULL(MIN(B.MONTH_END_UI)*-1,0) AS monthEndUi,
		0 as DoubtfullAccAll,
		ISNULL(SUM(B.DEPOSIT_AMT),0) as advRentPayment,
		CAST(ISNULL(ctd.TRANSACTION_AMOUNT,0) as numeric(15,2)) as CustomerAccRec,
		CAST
		(
			-- not due rec
			(
				SELECT 
				CASE
					WHEN (EXISTS(
							SELECT INSTALLMENT_AMT_PAID FROM CF_CONTRACT_PAYMENT_SCHEDULE 
							WHERE INSTALLMENT_NO <> 0 AND INSTALLMENT_AMT_PAID = '0.00' AND CONTRACT_NO = A.CONTRACT_NO)) 
					THEN (
						SELECT ISNULL(SUM(INSTALLMENT_AMT),0) 
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
						WHERE CONTRACT_NO =  A.CONTRACT_NO
							AND FORMAT(DUE_DATE, 'yyyyMM') > '202010'
					) ELSE (
						SELECT (
							SELECT IIF('202010' >= FORMAT(Y.DUE_DATE, 'yyyyMM'), 0, ISNULL(SUM(INSTALLMENT_AMT_PAID),0))
							FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
							WHERE CONTRACT_NO = Y.CONTRACT_NO
								AND FORMAT(DUE_DATE, 'yyyyMM') > '202010'
						)
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Y
						WHERE Y.CONTRACT_NO =  A.CONTRACT_NO
							AND Y.INSTALLMENT_NO <> 0 AND Y.INSTALLMENT_AMT <> '0.00'
						ORDER BY DUE_DATE DESC
						OFFSET 0 ROW 
						FETCH FIRST 1 ROW ONLY
					)
				END 
			) +
			-- past due rec
			(
				SELECT DISTINCT 
					(SELECT ISNULL(SUM(INSTALLMENT_AMT),0)
								FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
								WHERE (
									PAY_STATUS = 0 AND 
									DAY_OVERDUE > 0 AND 
									CONTRACT_NO=A.CONTRACT_NO AND 
									FORMAT(DUE_DATE, 'yyyyMM') <= '202010')
								OR (
									PAY_STATUS = 3 AND 
									DAY_OVERDUE > 0 AND 
									CONTRACT_NO=A.CONTRACT_NO AND 
									FORMAT(DUE_DATE, 'yyyyMM') <= '202010' AND
									FORMAT(PAID_DATE, 'yyyyMM') > '202010')
					) - 
					ISNULL ( G.DEPOSIT_AMOUNT, 0 ) 
					+ ISNULL ((SELECT 
						CASE 
							WHEN MAX(FORMAT(DUE_DATE, 'yyyyMM')) = MAX(FORMAT(PAID_DATE, 'yyyyMM')) OR MAX(FORMAT(DUE_DATE, 'yyyyMM')) < MAX(FORMAT(PAID_DATE, 'yyyyMM')) 
							THEN 0 ELSE 
							SUM(INSTALLMENT_AMT) 
						END 
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
					WHERE 
						PAY_STATUS = 3 AND 
						CONTRACT_NO=A.CONTRACT_NO AND
						FORMAT(DUE_DATE, 'yyyyMM') > '202010' AND 
						PAID_DATE >= (
							SELECT DATEADD(DAY, DAY_OVERDUE, DUE_DATE)
							FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
							WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND
							FORMAT(DUE_DATE, 'yyyyMM') > FORMAT(PAID_DATE, 'yyyyMM')
							AND CONTRACT_NO=A.CONTRACT_NO AND 
							INSTALLMENT_NO <> 0
						) 
						AND PAID_DATE <= (
							SELECT DATEADD(DAY, DAY_OVERDUE, DUE_DATE)
							FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
							WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND
							FORMAT(DUE_DATE, 'yyyyMM') > FORMAT(PAID_DATE, 'yyyyMM')
							AND CONTRACT_NO=A.CONTRACT_NO AND 
							INSTALLMENT_NO <> 0
						)
					), 0) 
					- ISNULL((SELECT 
								CASE 
									WHEN MAX(FORMAT(XX.DUE_DATE, 'yyyyMM')) = MAX(FORMAT(XX.PAID_DATE, 'yyyyMM')) OR MAX(FORMAT(XX.DUE_DATE, 'yyyyMM')) > MAX(FORMAT(XX.PAID_DATE, 'yyyyMM')) THEN SUM(XX.INSTALLMENT_AMT)  ELSE 0
								END 
							FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE XX
							WHERE 
								XX.PAY_STATUS = 3 AND 
								FORMAT(XX.PAID_DATE, 'yyyyMM') = '202010' AND 
								MONTH(XX.PAID_DATE) <> MONTH(XX.DUE_DATE) AND 
								XX.CONTRACT_NO=A.CONTRACT_NO
					), 0)
				FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE AS CCP
				WHERE CCP.PAY_STATUS = 0 AND CCP.DAY_OVERDUE > 0 OR CCP.PAY_STATUS = 3
			) + 
			-- month end ui
			ISNULL(SUM(B.MONTH_END_UI)*-1,0) +
			-- doubtfull
			0 +
			-- deposit amount
			ISNULL(SUM(B.DEPOSIT_AMT),0) +
			-- CustomerAccRec
			CAST(ISNULL(ctd.TRANSACTION_AMOUNT,0) as numeric(15,2))
			AS numeric(15,2)
		) as investment,
		CAST(ISNULL(SUM(B.TOTAL_ACRUAL)*-1,0) as numeric(15,2)) as accruedIncome,
		CAST(ISNULL(H.AMOUNT_PAID * -1, 0) as numeric(15,2)) as penaltyIncome,
		ISNULL((
			CASE
				WHEN (
					EXISTS( SELECT *
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
					WHERE
						PAY_STATUS = 0 AND
						CONTRACT_NO = A.CONTRACT_NO AND
						FORMAT(DUE_DATE, 'yyyyMM') <= '202010')
				) THEN (
					SELECT 
						CASE
							WHEN NOT EXISTS(
								SELECT 
									INSTALLMENT_NO
								FROM LAMP_2018.dbo.CF_DEPOSIT_HISTORY WHERE FORMAT(PAID_DATE, 'yyyyMM') <= '202010'
								AND CONTRACT_NO = OSS.CONTRACT_NO
							)
							THEN (OSS.PRINCIPAL_BALANCE_ADMIN)
							WHEN (
								EXISTS (
									SELECT PAID_DATE FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
									WHERE 
										PAY_STATUS = OSS.PAY_STATUS AND
										CONTRACT_NO = OSS.CONTRACT_NO AND
										FORMAT(PAID_DATE, 'yyyyMM') = '202010')
							)
							THEN (
								SELECT
									(yyy.PRINCIPAL_BALANCE_ADMIN) - IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > y.INTEREST_AMT_ADMIN, ISNULL(G.DEPOSIT_AMOUNT, 0) - y.INTEREST_AMT_ADMIN, 0)
								FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE y
								INNER JOIN (
									SELECT 
										ISNULL(MAX(INSTALLMENT_NO), -1) AS INSTALMENT_NO
									FROM LAMP_2018.dbo.CF_DEPOSIT_HISTORY WHERE FORMAT(PAID_DATE, 'yyyyMM') = '202010'
									AND CONTRACT_NO = OSS.CONTRACT_NO
								) yy ON y.INSTALLMENT_NO = INSTALMENT_NO
								INNER JOIN (
									SELECT CONTRACT_NO, PRINCIPAL_BALANCE_ADMIN, PAID_DATE
									FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
									WHERE CONTRACT_NO = OSS.CONTRACT_NO AND 
									PAY_STATUS = OSS.PAY_STATUS AND
									FORMAT(PAID_DATE, 'yyyyMM') = '202010'
									ORDER BY PAID_DATE DESC
									OFFSET 0 ROW 
									FETCH FIRST 1 ROW ONLY
								) yyy ON y.CONTRACT_NO = yyy.CONTRACT_NO
								WHERE y.PAY_STATUS = OSS.PAY_STATUS AND
									y.CONTRACT_NO = OSS.CONTRACT_NO AND
									y.INSTALLMENT_NO = yy.INSTALMENT_NO
							) ELSE ( 
								(OSS.PRINCIPAL_BALANCE_ADMIN) - IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > OSX.INTEREST_AMT_ADMIN, ISNULL(G.DEPOSIT_AMOUNT, 0) - OSX.INTEREST_AMT_ADMIN, 0)
							)
						END
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE OSS
					INNER JOIN (
						SELECT * 
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
						WHERE
							PAY_STATUS = 0 AND
							CONTRACT_NO = A.CONTRACT_NO
						ORDER BY INSTALLMENT_NO ASC
						OFFSET 0 ROW 
						FETCH FIRST 1 ROW ONLY
					) OSX ON OSS.CONTRACT_NO = OSX.CONTRACT_NO
					WHERE
						OSS.PAY_STATUS = 3 AND
						OSS.CONTRACT_NO = A.CONTRACT_NO 
					ORDER BY OSS.INSTALLMENT_NO DESC
					OFFSET 0 ROW 
					FETCH FIRST 1 ROW ONLY
				)
				ELSE (
					SELECT 
						CASE
							WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') < FORMAT(Z.PAID_DATE, 'yyyyMM')
							THEN (
								SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
								WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') <= FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
								ORDER BY CCP.INSTALLMENT_NO DESC
								OFFSET 0 ROW 
								FETCH FIRST 1 ROW ONLY
							)
							WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') = FORMAT(Z.PAID_DATE, 'yyyyMM')
							THEN (
								SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
								WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') = FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
								ORDER BY CCP.INSTALLMENT_NO DESC
								OFFSET 0 ROW 
								FETCH FIRST 1 ROW ONLY
							)
							WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') > FORMAT(Z.PAID_DATE, 'yyyyMM')
							THEN (
								SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
								WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') <= FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
								ORDER BY CCP.INSTALLMENT_NO DESC
								OFFSET 0 ROW 
								FETCH FIRST 1 ROW ONLY
							)
							WHEN FORMAT(Z.PAID_DATE, 'yyyyMM') IS NULL AND Z.INSTALLMENT_NO = 0
							THEN (
								Z.PRINCIPAL_BALANCE_ADMIN
							)
						END
					FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Z
					WHERE 
						Z.PAY_STATUS = 3 AND
						Z.CONTRACT_NO = A.CONTRACT_NO AND
						FORMAT(Z.DUE_DATE, 'yyyyMM') <= '202010'
					ORDER BY Z.INSTALLMENT_NO DESC
					OFFSET 0 ROW 
					FETCH FIRST 1 ROW ONLY
				)
			END
		), 0) AS osPrincipal, 
		(
			SELECT 
				CAST(ISNULL(
					SUM(Y.PRINCIPAL_AMT_ADMIN) - 
					ISNULL(
						(SELECT IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > INTEREST_AMT_ADMIN, ISNULL(G.DEPOSIT_AMOUNT, 0) - INTEREST_AMT_ADMIN, 0) 
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
						WHERE CONTRACT_NO = A.CONTRACT_NO and FORMAT(paid_date, 'yyyyMM') > '202010' and FORMAT(DUE_DATE, 'yyyyMM') <= '202010' and INSTALLMENT_NO <> 0 
						or CONTRACT_NO = A.CONTRACT_NO and FORMAT(paid_date, 'yyyyMM') is null and FORMAT(DUE_DATE, 'yyyyMM') <= '202010' and INSTALLMENT_NO <> 0
						ORDER BY INSTALLMENT_NO ASC
						OFFSET 0 ROW 
						FETCH FIRST 1 ROW ONLY)
					, 0)
				,0) AS NUMERIC(15,2))
			FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Y
			WHERE Y.CONTRACT_NO = A.CONTRACT_NO and FORMAT(Y.paid_date, 'yyyyMM') > '202010' and FORMAT(Y.DUE_DATE, 'yyyyMM') <= '202010' and Y.INSTALLMENT_NO <> 0 
			or Y.CONTRACT_NO = A.CONTRACT_NO and FORMAT(Y.paid_date, 'yyyyMM') is null and FORMAT(Y.DUE_DATE, 'yyyyMM') <= '202010' and Y.INSTALLMENT_NO <> 0
		) as osPrincipalPjt,
		(
			ISNULL((
				CASE
					WHEN (
						EXISTS( SELECT *
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
						WHERE
							PAY_STATUS = 0 AND
							CONTRACT_NO = A.CONTRACT_NO AND
							FORMAT(DUE_DATE, 'yyyyMM') <= '202010')
					) THEN (
						SELECT 
							CASE
								WHEN NOT EXISTS(
									SELECT 
										INSTALLMENT_NO
									FROM LAMP_2018.dbo.CF_DEPOSIT_HISTORY WHERE FORMAT(PAID_DATE, 'yyyyMM') <= '202010'
									AND CONTRACT_NO = OSS.CONTRACT_NO
								)
								THEN (OSS.PRINCIPAL_BALANCE_ADMIN)
								WHEN (
									EXISTS (
										SELECT PAID_DATE FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
										WHERE 
											PAY_STATUS = OSS.PAY_STATUS AND
											CONTRACT_NO = OSS.CONTRACT_NO AND
											FORMAT(PAID_DATE, 'yyyyMM') = '202010')
								)
								THEN (
									SELECT
										(yyy.PRINCIPAL_BALANCE_ADMIN) - IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > y.INTEREST_AMT_ADMIN, ISNULL(G.DEPOSIT_AMOUNT, 0) - y.INTEREST_AMT_ADMIN, 0)
									FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE y
									INNER JOIN (
										SELECT 
											ISNULL(MAX(INSTALLMENT_NO), -1) AS INSTALMENT_NO
										FROM LAMP_2018.dbo.CF_DEPOSIT_HISTORY WHERE FORMAT(PAID_DATE, 'yyyyMM') = '202010'
										AND CONTRACT_NO = OSS.CONTRACT_NO
									) yy ON y.INSTALLMENT_NO = INSTALMENT_NO
									INNER JOIN (
										SELECT CONTRACT_NO, PRINCIPAL_BALANCE_ADMIN, PAID_DATE
										FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
										WHERE CONTRACT_NO = OSS.CONTRACT_NO AND 
										PAY_STATUS = OSS.PAY_STATUS AND
										FORMAT(PAID_DATE, 'yyyyMM') = '202010'
										ORDER BY PAID_DATE DESC
										OFFSET 0 ROW 
										FETCH FIRST 1 ROW ONLY
									) yyy ON y.CONTRACT_NO = yyy.CONTRACT_NO
									WHERE y.PAY_STATUS = OSS.PAY_STATUS AND
										y.CONTRACT_NO = OSS.CONTRACT_NO AND
										y.INSTALLMENT_NO = yy.INSTALMENT_NO
								) ELSE ( 
									(OSS.PRINCIPAL_BALANCE_ADMIN) - IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > OSX.INTEREST_AMT_ADMIN, ISNULL(G.DEPOSIT_AMOUNT, 0) - OSX.INTEREST_AMT_ADMIN, 0)
								)
							END
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE OSS
						INNER JOIN (
							SELECT * 
							FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
							WHERE
								PAY_STATUS = 0 AND
								CONTRACT_NO = A.CONTRACT_NO
							ORDER BY INSTALLMENT_NO ASC
							OFFSET 0 ROW 
							FETCH FIRST 1 ROW ONLY
						) OSX ON OSS.CONTRACT_NO = OSX.CONTRACT_NO
						WHERE
							OSS.PAY_STATUS = 3 AND
							OSS.CONTRACT_NO = A.CONTRACT_NO 
						ORDER BY OSS.INSTALLMENT_NO DESC
						OFFSET 0 ROW 
						FETCH FIRST 1 ROW ONLY
					)
					ELSE (
						SELECT 
							CASE
								WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') < FORMAT(Z.PAID_DATE, 'yyyyMM')
								THEN (
									SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
									WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') <= FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
									ORDER BY CCP.INSTALLMENT_NO DESC
									OFFSET 0 ROW 
									FETCH FIRST 1 ROW ONLY
								)
								WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') = FORMAT(Z.PAID_DATE, 'yyyyMM')
								THEN (
									SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
									WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') = FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
									ORDER BY CCP.INSTALLMENT_NO DESC
									OFFSET 0 ROW 
									FETCH FIRST 1 ROW ONLY
								)
								WHEN FORMAT(Z.DUE_DATE, 'yyyyMM') > FORMAT(Z.PAID_DATE, 'yyyyMM')
								THEN (
									SELECT CCP.PRINCIPAL_BALANCE_ADMIN FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE CCP
									WHERE FORMAT(CCP.PAID_DATE, 'yyyyMM') <= FORMAT(Z.DUE_DATE, 'yyyyMM') AND CCP.CONTRACT_NO = Z.CONTRACT_NO
									ORDER BY CCP.INSTALLMENT_NO DESC
									OFFSET 0 ROW 
									FETCH FIRST 1 ROW ONLY
								)
								WHEN FORMAT(Z.PAID_DATE, 'yyyyMM') IS NULL AND Z.INSTALLMENT_NO = 0
								THEN (
									Z.PRINCIPAL_BALANCE_ADMIN
								)
							END
						FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Z
						WHERE 
							Z.PAY_STATUS = 3 AND
							Z.CONTRACT_NO = A.CONTRACT_NO AND
							FORMAT(Z.DUE_DATE, 'yyyyMM') <= '202010'
						ORDER BY Z.INSTALLMENT_NO DESC
						OFFSET 0 ROW 
						FETCH FIRST 1 ROW ONLY
					)
				END
			), 0) - 
			( CASE 
				WHEN F.ASSET_TYPE = 3 THEN ISNULL(A.ASSET_PRICE_AMT,0)
				ELSE
					(
						CASE 
							WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 6  THEN 1
							WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 12 THEN 0.8
							WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 18 THEN 0.6
							WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 24 THEN 0.4
							WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 30 THEN 0.2
							ELSE 0
						END
					) * ISNULL(A.ASSET_PRICE_AMT,0)
			 END
			)
		) AS [osPrincipal-collateral],
		(
			SELECT 
				CAST(ISNULL(
					IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) > Y.INTEREST_AMT_ADMIN, 0, IIF(ISNULL(G.DEPOSIT_AMOUNT, 0) <= Y.INTEREST_AMT_ADMIN, Y.INTEREST_AMT_ADMIN - ISNULL(G.DEPOSIT_AMOUNT, 0), 0)) + 
					ISNULL((
							SELECT SUM(Z.INTEREST_AMT_ADMIN) 
							FROM (
								SELECT INTEREST_AMT_ADMIN
								FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
								WHERE CONTRACT_NO = A.CONTRACT_NO and FORMAT(paid_date, 'yyyyMM') > '202010' and FORMAT(DUE_DATE, 'yyyyMM') <= '202010' and INSTALLMENT_NO <> 0 
								or CONTRACT_NO = A.CONTRACT_NO and FORMAT(paid_date, 'yyyyMM') is null and FORMAT(DUE_DATE, 'yyyyMM') <= '202010' and INSTALLMENT_NO <> 0
								ORDER BY INSTALLMENT_NO
								OFFSET 1 ROW 
							) AS Z)
					, 0)
				,0) AS NUMERIC(15,2))
			FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE Y
			WHERE Y.CONTRACT_NO = A.CONTRACT_NO and FORMAT(Y.paid_date, 'yyyyMM') > '202010' and FORMAT(Y.DUE_DATE, 'yyyyMM') <= '202010' and Y.INSTALLMENT_NO <> 0 
			or Y.CONTRACT_NO = A.CONTRACT_NO and FORMAT(Y.paid_date, 'yyyyMM') is null and FORMAT(Y.DUE_DATE, 'yyyyMM') <= '202010' and Y.INSTALLMENT_NO <> 0
			ORDER BY INSTALLMENT_NO ASC
			OFFSET 0 ROW 
			FETCH FIRST 1 ROW ONLY
		) as osInterestPjt,
		( CASE 
			WHEN F.ASSET_TYPE = 3 THEN ISNULL(A.ASSET_PRICE_AMT,0)
			ELSE
				(
					CASE 
						WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 6  THEN 1
						WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 12 THEN 0.8
						WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 18 THEN 0.6
						WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 24 THEN 0.4
						WHEN DATEDIFF(M,A.GOLIVE_DATE,CONVERT(date, CONCAT('202010','01'))) <= 30 THEN 0.2
						ELSE 0
					END
				) * ISNULL(A.ASSET_PRICE_AMT,0)
		 END
		) AS collateralAmt,
		CAST(ISNULL(A.CONTRACT_CURRENCY,'') AS VARCHAR(MAX)) AS currency,
		CAST(A.RENT_PAYMENT_TYPE AS VARCHAR) AS RENT_PAYMENT_TYPE,
		CAST(ISNULL(B.INSTALLMENT_NO, 999) AS VARCHAR) AS INSTALLMENT_NO,
		CAST(CASE
			WHEN (A.BUSINESS_LINE = '01') THEN
				(SELECT CODE_NAME FROM CODE_MASTER WHERE CODE_TYPE = '764' AND CODE_ID = A.PAYMENT_METHOD AND COUNTRY_ID = '62')
			WHEN (A.BUSINESS_LINE = '02') THEN
				(SELECT CODE_NAME FROM CODE_MASTER WHERE CODE_TYPE = '765' AND CODE_ID = A.PAYMENT_METHOD AND COUNTRY_ID = '62')
			WHEN (A.BUSINESS_LINE = '03') THEN
				(SELECT CODE_NAME FROM CODE_MASTER WHERE CODE_TYPE = '766' AND CODE_ID = A.PAYMENT_METHOD AND COUNTRY_ID = '62')
			ELSE ''
			END
		AS VARCHAR(MAX)) AS paymentMethodName
	FROM LAMP_2018.dbo.CF_CONTRACT_INFO A
		LEFT JOIN LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE B ON A.CONTRACT_NO = B.CONTRACT_NO AND B.INSTALLMENT_NO = (
			SELECT MAX(INSTALLMENT_NO) FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
			WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND CONTRACT_NO = A.CONTRACT_NO
		)
		LEFT  JOIN LAMP_2018.dbo.CUSTOMER_MST C ON A.CUSTOMER_CODE = C.CUSTOMER_CODE
		INNER JOIN LAMP_2018.dbo.COMPANY_MST D ON A.BRANCH_CODE = D.COMPANY_CODE AND D.LEVEL_NO = '1'
		INNER JOIN LAMP_2018.dbo.REGION_MST E  ON D.REGION_CODE = E.REGION_CODE
		LEFT  JOIN LAMP_2018.dbo.CF_TERMINATION_HEADER as cth 
				ON cth.CONTRACT_NO = A.CONTRACT_NO 
				AND cth.STATUS = 'CL' AND cth.REPAYMENT_TRANSACTION_NO IS NULL
		LEFT  JOIN LAMP_2018.dbo.CF_TERMINATION_DETAIL ctd 
				ON cth.CONTRACT_NO = ctd.CONTRACT_NO 
				AND cth.SEQUENCE_NO = ctd.SEQUENCE_NO 
				AND ctd.TRANSACTION_CODE = 'T015' 
		LEFT  JOIN LAMP_2018.dbo.INDUSTRY_MST idm
				ON C.TYPE_OF_INDUSTRY_CODE = idm.TYPE_OF_INDUSTRY_CD
		INNER JOIN LAMP_2018.dbo.CF_CONTRACT_ASSET cas
				ON A.CONTRACT_NO = cas.CONTRACT_NO
		LEFT  JOIN LAMP_2018.dbo.PRODUCT_CATEGORY_MST pcm
				ON cas.CATEGORY_CODE = pcm.CATEGORY_CODE AND cas.CLASSIFICATION_CODE = pcm.CLASSIFICATION_CODE  
				AND cas.SPECIFICATION_CODE = pcm.SPECIFICATION_CODE  AND cas.ASSET_TYPE = pcm.SUPPLIES_TYPE
		INNER JOIN LAMP_2018.dbo.CF_CONTRACT_ASSET F ON A.CONTRACT_NO = F.CONTRACT_NO AND F.ASSET_NO = '1'
		LEFT JOIN (
				SELECT CONTRACT_NO, SUM(DEPOSIT_AMOUNT) AS DEPOSIT_AMOUNT
				FROM LAMP_2018.dbo.CF_DEPOSIT_HISTORY WHERE FORMAT(PAID_DATE, 'yyyyMM') <= '202010'
				GROUP BY CONTRACT_NO
		) AS G ON G.CONTRACT_NO = A.CONTRACT_NO
		LEFT JOIN CF_REPAYMENT_HISTORY H ON A.CONTRACT_NO = H.CONTRACT_NO AND FORMAT(H.REPAYMENT_DATE, 'yyyyMM') = '202010' AND H.TRANSACTION_CODE = 'T002'
	WHERE 
		FORMAT(A.GOLIVE_DATE, 'yyyyMM') <= '202010' AND
		A.CONTRACT_NO --= '21711185'
		BETWEEN '0100002000001' --'0200001900002' 
		AND '22010114' --'0210012000001'
		--AND '0100002000001' --'0210012000001'
		AND E.REGION_CODE	in ('EXT','R01','R02','R03','R04','R05','R06','R07','R08','R09','R10','R11','R12','R23','R13','R233','d34') 
		AND D.COMPANY_CODE	in ('0000')
		AND A.OUTLET_CODE	in ('000001')
		AND A.BUSINESS_LINE = '01' --'02' 
		AND A.CONTRACT_STATUS in ('02', '09', '10', '03', '11')
		OR
		FORMAT(B.DUE_DATE, 'yyyyMM') = '202010' AND 
		FORMAT(B.PAID_DATE, 'yyyyMM') <= '202010' AND
		--FORMAT(A.GOLIVE_DATE, 'yyyyMM') <= '202010' AND
		A.CONTRACT_NO --= '21711185'
		BETWEEN '0100002000001' --'0200001900002' 
		AND '22010114' --'0210012000001'
		--AND '0100002000001' --'0210012000001'
		AND E.REGION_CODE	in ('EXT','R01','R02','R03','R04','R05','R06','R07','R08','R09','R10','R11','R12','R23','R13','R233','d34') 
		AND D.COMPANY_CODE	in ('0000')
		AND A.OUTLET_CODE	in ('000001')
		AND A.BUSINESS_LINE = '01' --'02' 
		AND A.CONTRACT_STATUS in ('06')
	GROUP BY 
		C.CUSTOMER_NAME,
		A.GOLIVE_DATE,
		A.MATURITY_DATE,
		A.CONTRACT_NO,
		A.COLLECTION_CLASSIFICATION_CODE,
		A.DOWNPAYMENT_AMT,
		A.PRINCIPAL_AMT+A.INTEREST_AMT,
		A.RENT_PAYMENT_TYPE,
		ctd.TRANSACTION_AMOUNT,
		B.PENALTY_AMT,
		A.CONTRACT_CURRENCY, 
		A.RENT_PAYMENT_TYPE,
		B.INSTALLMENT_NO,
 		A.ASSET_PRICE_AMT, 
	   F.ASSET_TYPE,  
		B.PENALTY_PAID,
		B.MONTH_END_UI,
		G.DEPOSIT_AMOUNT,
		A.PAYMENT_METHOD,
		A.BUSINESS_LINE,
		H.AMOUNT_PAID
	ORDER BY A.CONTRACT_NO

--END
--CLOSE c__contract
--DEALLOCATE c__contract


	--SELECT DISTINCT 
	--	(SELECT ISNULL(SUM(INSTALLMENT_AMT),0)
	--			FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
	--			WHERE  PAY_STATUS = 0 AND DAY_OVERDUE > 0 and CONTRACT_NO=@contract
	--				AND FORMAT(DUE_DATE, 'yyyyMM') <= '202010') - 
	--	(SELECT ISNULL(SUM(DEPOSIT_AMT),0)
	--			FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE 
	--			WHERE  CONTRACT_NO='21811008') + 
	--	ISNULL ((SELECT 
	--			CASE
	--				 WHEN MONTH(DUE_DATE) < MONTH(PAID_DATE) THEN INSTALLMENT_AMT ELSE 0
	--			END
	--	from LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
	--	where 
	--		PAY_STATUS = 3 and 
	--		CONTRACT_NO=@contract AND
	--		PAID_DATE = (
	--				SELECT DATEADD(DAY, DAY_OVERDUE, DUE_DATE)
	--				FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
	--				WHERE FORMAT(DUE_DATE, 'yyyyMM') = '202010' AND CONTRACT_NO=@contract AND INSTALLMENT_NO <> 0
	--			) 
	--	), 0) -
	--	(select ISNULL(SUM(INSTALLMENT_AMT),0) from LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE
	--							where PAY_STATUS = 3 And FORMAT(PAID_DATE, 'yyyyMM') = '202010' and MONTH(PAID_DATE) <> MONTH(DUE_DATE) and CONTRACT_NO=@contract)
	--	FROM LAMP_2018.dbo.CF_CONTRACT_PAYMENT_SCHEDULE AS A 
	--	WHERE PAY_STATUS = 0 AND DAY_OVERDUE > 0 OR A.PAY_STATUS = 3
	
--END

--SELECT MONTH('2021-01-01')