USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[SP_CLOSING_EOD]    Script Date: 10/14/2020 16:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SP_CLOSING_EOD] @BranchCode nvarchar(30),
@SystemDate date,
@UserID nvarchar(24),
@FlagSuccess char(1) OUTPUT
AS
BEGIN
    BEGIN TRY

        SET NOCOUNT ON;
        DECLARE @vMaxSeq_ACC int,
                @vCutoffPeriodCollection int;
        SELECT
            @vMaxSeq_ACC = ISNULL(MAX(SEQUENCE_NO), 0) + 1
        FROM EOD_LOG
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD ';
        IF @vMaxSeq_ACC = 1
        BEGIN
            INSERT INTO EOD_LOG (BRANCH_CODE,
            CURRENT_TRANSACTION_DATE,
            SEQUENCE_NO,
            EOD_JOB_TYPE,
            [DESCRIPTION],
            START_TIME,
            END_TIME,
            [ERROR_MESSAGE],
            CREATE_USER,
            CREATE_DATE,
            START_DATE,
            SR_NO)
                VALUES (@BranchCode, @SystemDate, @vMaxSeq_ACC, 'SAL', 'SP_CLOSING_EOD', GETDATE(), NULL, NULL, 'EOD', GETDATE(), GETDATE(), 16);
        END;
        ELSE
        BEGIN
            UPDATE EOD_LOG
            SET 
                START_DATE = GETDATE(),
                START_TIME = GETDATE(),
                END_DATE = NULL,
                END_TIME = NULL,
                MODIFY_USER = 'EOD',
                MODIFY_DATE = GETDATE(),
                SEQUENCE_NO = @vMaxSeq_ACC,
                SR_NO = 6
            WHERE BRANCH_CODE = @BranchCode
            AND CURRENT_TRANSACTION_DATE = @SystemDate
            AND EOD_JOB_TYPE = 'SAL'
            AND SEQUENCE_NO = @vMaxSeq_ACC - 1
            AND [DESCRIPTION] = 'SP_CLOSING_EOD';
        END;

        DECLARE @RowCount int;
        DECLARE @BranchCode1 nvarchar(30),
                @OutletCode nvarchar(30),
                @JournalNo nvarchar(30),
                @TransactionDate date,
                @AccountCode nvarchar(30),
                @PosJournal nvarchar(1),
                @CurrencyCode nvarchar(3),
                @Amount numeric(15, 2),
                @SeqNo numeric(4),
                @MonthPeriod numeric(2),
                @YearPeriod nvarchar(4);
        DECLARE @MutValueToday numeric(15, 2),
                @DebitValueToday numeric(15, 2),
                @CreditValueToday numeric(15, 2),
                @MutValueEx numeric(15, 2),
                @DebitValueEx numeric(15, 2),
                @CreditValueEx numeric(15, 2),
                @ColumnBeginEx numeric(15, 2),
                @ColumnBegin nvarchar(max),
                @ColumnEnd nvarchar(max),
                @ColumnMut nvarchar(max),
                @ColumnDebit nvarchar(max),
                @ColumnCredit nvarchar(max),
                @MutValue numeric(15, 2),
                @EndBal numeric(15, 2),
                @DebitValue numeric(15, 2),
                @CreditValue numeric(15, 2);
        DECLARE @AccountCode2 nvarchar(30),
                @Level nvarchar(2),
                @CoaAcum nvarchar(30),
                @CoaAcumLevel nvarchar(2);
        DECLARE @SqlStmt nvarchar(max),
                @ParmDefinition nvarchar(max);
        DECLARE @EndBalDtl numeric(15, 2),
                @MutValueDtl numeric(15, 2),
                @DebitValueDtl numeric(15, 2),
                @CreditValueDtl numeric(15, 2);
        DECLARE @ColumnBeginBal nvarchar(100),
                @ColumnEndBal nvarchar(100),
                @ColumnMutValue nvarchar(100),
                @ColumnDebitValue nvarchar(100),
                @ColumnCreditValue nvarchar(100);
        DECLARE @Count numeric(5);
        DECLARE @NextBal numeric(10),
                @AccountCode1 nvarchar(30),
                @pendapatanValue numeric(15, 2),
                @bebanValue numeric(15, 2),
                @labaValue numeric(15, 2),
                @accountLaba nvarchar(30),
                @currMonth numeric(2),
                @currYear numeric(4);
        DECLARE @pendapatanDebit numeric(15, 2),
                @pendapatanCredit numeric(15, 2),
                @bebanDebit numeric(15, 2),
                @bebanCredit numeric(15, 2);
        DECLARE @incomeTaxMut numeric(15, 2),
                @incomeTaxDebit numeric(15, 2),
                @incomeTaxCredit numeric(15, 2);
            
        print 'START EOD: ' + CONVERT(VARCHAR, @SystemDate)

        -- check jurnal in open month
        SELECT A.JOURNAL_NO
        INTO #CoaStatusOpen
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE <= @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('3')
        ORDER BY A.TRANSACTION_DATE ASC;

        IF (EXISTS(SELECT 1 FROM #CoaStatusOpen))
		BEGIN
			print 'FOUND JOURNAL IN OPEN MONTH'
            EXECUTE [dbo].[SP_CLOSING_EOD_FISCAL_OPEN] 
			   @BranchCode
			  ,@SystemDate
			  ,@UserID
			  ,@FlagSuccess OUTPUT
		END

        DROP TABLE #CoaStatusOpen

        -- update to closed if any fiscal month has open status
        IF (EXISTS(SELECT * FROM AKT.dbo.CALENDAR_FISCAL_MST WHERE [STATUS] = '3'))
		BEGIN
            print 'CLOSE OPEN FISCAL MONTH'
	        UPDATE AKT.dbo.CALENDAR_FISCAL_MST SET [STATUS] = '2' WHERE [STATUS] = '3'
		END

        -- check jurnal in active month but before system date
        SELECT A.JOURNAL_NO
        INTO #CoaActiveBackdate
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE < @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('1')
        ORDER BY A.TRANSACTION_DATE ASC;

        IF (EXISTS(SELECT 1 FROM #CoaActiveBackdate))
		BEGIN
            print 'FOUND JOURNAL IN ACTIVE MONTH'
            EXECUTE [dbo].[SP_CLOSING_EOD_ACTIVE_BACKDATE] 
			   @BranchCode
			  ,@SystemDate
			  ,@UserID
			  ,@FlagSuccess OUTPUT
		END

        DROP TABLE #CoaActiveBackdate

        -- check if present month and year are status active (status 1)
        SELECT
            @currMonth = [MONTH],
            @currYear = [YEAR]
        FROM AKT.dbo.CALENDAR_FISCAL_MST
        WHERE [STATUS] = 1
        
        -- READ AND SET #TEMPC0
        DECLARE c_journal CURSOR LOCAL FORWARD_ONLY FOR
        SELECT
            A.BRANCH_CODE,
            A.JOURNAL_NO,
            A.OUTLET_CODE,
            A.VALUE_DATE,
            A.ACCOUNT_CODE AS ACCOUNT_CODE,
            A.POS_JOURNAL,
            A.CURRENCY_CODE,
            A.AMOUNT,
            A.SEQ_NO,
            B.[MONTH],
            B.[YEAR],
            (CASE
                WHEN A.POS_JOURNAL = 'D' THEN A.AMOUNT
                ELSE 0
            END) AS DEBIT,
            (CASE
                WHEN A.POS_JOURNAL = 'C' THEN A.AMOUNT
                ELSE 0
            END) AS CREDIT
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE = @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('1')
        ORDER BY A.TRANSACTION_DATE ASC;

        OPEN c_journal
        WHILE 1 = 1
        BEGIN
            FETCH c_journal INTO @BranchCode1, @JournalNo, @OutletCode, @TransactionDate,
            @AccountCode, @PosJournal, @CurrencyCode, @Amount, @SeqNo, @MonthPeriod,
            @YearPeriod, @DebitValueToday, @CreditValueToday

            IF @@fetch_status = -1
                BREAK;

            print '## COA NUMBER ## ' + @AccountCode;
            -- set field name based on month of transaction #SetField
            SET @ColumnBegin = 'BEGIN_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnEnd = 'END_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnMut = 'MUT_VALUE_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnDebit = 'DB_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnCredit = 'CR_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');

            -- Set mut_value based on amount in cf_journal_info #SetMutValueToday'
            IF @PosJournal = 'D'
            BEGIN
                -- amount using positive number'
                SET @MutValueToday = @Amount;
            END;
            ELSE
            BEGIN
                -- amount using negative number'
                SET @MutValueToday = -1 * @Amount;
            END;
            
            -- get coa parent or grandparent (coa header) #listCoaLevel'
            -- query below will result something like this:
            --
            -- COA_CODE    | LEVEL | COA_ACCUM
            -- 110201000001	  7	     110200000000
            -- 110200000000	  3	     110000000000
            -- 110000000000	  2	     100000000000

            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVEL_
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @AccountCode
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL_ + 1 AS LEVELS
            FROM COA_MST,
                 COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 1)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            -- iterate coa based on #ListCoaLevel'
            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;
                
                IF @@FETCH_STATUS = -1
                    BREAK
                
                -- ## UPDATE TRX SALDO ##'
                SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
											@MutValueEx NUMERIC(20,4) OUTPUT,
											@DebitValueEx NUMERIC(15,2) OUTPUT,
											@CreditValueEx NUMERIC(15,2) OUTPUT';

                -- get existing value of field BEGIN_BAL, MUTA_VAL, END_BAL, DEB_, CRE_  from trx_saldo.
                -- based on field in #SetField. lets call it #prepareValueCoa                
                SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
									 @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
									 FROM AKT.dbo.TRX_SALDO
									 WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
									   AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
									   AND COA_CODE=''' + @AccountCode2 + ''' 
									   AND OUTLET_CODE=''' + @OutletCode + '''';

				

                EXECUTE sp_executesql @SqlStmt,
                                      @ParmDefinition,
                                      @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                      @MutValueEx = @MutValueEx OUTPUT,
                                      @DebitValueEx = @DebitValueEx OUTPUT,
                                      @CreditValueEx = @CreditValueEx OUTPUT;

                -- SET NEW VALUE FOR ALL LEVEL COA EXCEPT LEVEL 1.
                -- set existing mut_value, end_balance, debit_value, credit_value #setValueCoa
                SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@MutValueToday, 0);
                SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);
                SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@DebitValueToday, 0);
                SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@CreditValueToday, 0);

                -- prepare update dynamic query to trx_saldo based on month
                -- ColumnMut -> field name check from #SetField
                -- MutValue -> value from #setValueCoa
                SET @SqlStmt =
                N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ', ' +
                ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
							MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
						WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
						  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode2 + ''' 
						  AND OUTLET_CODE=''' + @OutletCode + '''';
                
				print @SqlStmt;

                -- execute update coa query'
                EXECUTE sp_executesql @SqlStmt;

                -- ## UPDATE TRX_SALDO DETAIL ##'
                -- get trx_saldo detail with day -1 from date transaction
                -- and set to endBalDtl, mutValueDtl, debitValueDtl, creaditValueDtl
                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate) -- get day before
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- execute update trx saldo detail query'
                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                    MUT_VALUE = MUT_VALUE + ISNULL(@MutValueToday,0),
                    END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@MutValueToday,0)),
                    DB = (DB + ISNULL(@DebitValueToday, 0)),
                    CR = (CR + ISNULL(@CreditValueToday, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = @SystemDate
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT 'Bukan LVL 1 TRX_SALDO_DTL CUYYYYY ', * FROM AKT.dbo.TRX_SALDO_DTL  WHERE BRANCH_CODE = @BranchCode1
                -- AND SALDO_DATE = @SystemDate
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode

                -- ## UPDATE BEGIN BALANCE FOR NEXT MONTH ##'
                -- check if this transaction is on december
                -- if true, then nextBal = null
                IF CAST(@MonthPeriod AS numeric(2)) = 12
                BEGIN
                    print '-- NO NEED TO UPDATE BEGIN BALANCE'
                    SET @NextBal = NULL
                END
                ELSE
                BEGIN
                    -- if not in december
                    -- update beginBalace, mutValue, endBalance for next month
                    SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                    SET @COLUMNBEGINBAL = 'BEGIN_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    SET @COLUMNMUTVALUE = 'MUT_VALUE_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    SET @COLUMNENDBAL = 'END_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    --SET @COLUMNDEBITVALUE = 'DB_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');
                    --SET @COLUMNCREDITVALUE = 'CR_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');

                    SET @SQLSTMT =
                    N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                    ISNULL(@COLUMNBEGINBAL, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
									WHERE BRANCH_CODE=''' + @BRANCHCODE1 + ''' 
									  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YEARPERIOD) + ' 
									  AND COA_CODE=''' + @ACCOUNTCODE2 + ''' 
									  AND OUTLET_CODE=''' + @OutletCode + '''';

                    -- execute begin balcance for next month'
                    EXECUTE SP_EXECUTESQL @SQLSTMT;
                END

                -- debug
                -- SELECT 'BUKAN LEVEL 1 ' + CONVERT(VARCHAR, @SystemDate), * FROM AKT.dbo.TRX_SALDO Z
                -- WHERE Z.COA_CODE = @AccountCode2 OR Z.COA_CODE = '100000000000';

                -- CHECK IF COA HAS COA ACUM LEVEL 1?'
                SET @CoaAcumLevel = NULL -- restart variable
                SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
	            WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'
                
                IF (@CoaAcumLevel = '1')
                BEGIN
                    -- THIS IS COA LEVEL 1'
                    SELECT
                        @AccountCode1 = ISNULL(COA_CODE, ' ')
                    FROM COA_MST
                    WHERE COA_CODE = @CoaAcum -- CoaAcum -> coa parent from #ListCoaLevel
                    AND ACTIVE_STATUS = 1;

                    -- if account level 1 found, execute this script'
                    -- else skip this script
                    IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
                    BEGIN
                        SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(15,2) OUTPUT,
													@MutValueEx NUMERIC(15,2) OUTPUT,
													@DebitValueEx NUMERIC(15,2) OUTPUT,
													@CreditValueEx NUMERIC(15,2) OUTPUT';

                        -- debug
                        -- SELECT 'INIT COA LEVEL 1 ' + CONVERT(VARCHAR, @SystemDate), * FROM AKT.dbo.TRX_SALDO Z
                        -- WHERE Z.COA_CODE = @AccountCode1;

                        -- get existing value of field BEGIN_BAL, MUTA_VAL, END_BAL, DEB_, CRE_  from trx_saldo. 
                        -- based on field in #SetField. lets call it #prepareValueCoaLevel1
                        SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
													@DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
											 FROM AKT.dbo.TRX_SALDO
											 WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
											   AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
											   AND OUTLET_CODE=''' + @OutletCode + '''';
                        
                        -- execute #prepareValueCoaLevel1'
                        EXECUTE sp_executesql @SqlStmt,
                                              @ParmDefinition,
                                              @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                              @MutValueEx = @MutValueEx OUTPUT,
                                              @DebitValueEx = @DebitValueEx OUTPUT,
                                              @CreditValueEx = @CreditValueEx OUTPUT;

                        -- SET NEW VALUE FOR COA LEVEL 1. #setValueCoaLevel1
                        SET @MutValue = @MutValueEx + @MutValueToday;
                        SET @EndBal = @ColumnBeginEx + @MutValue;
                        SET @DebitValue = @DebitValueEx + @DebitValueToday;
                        SET @CreditValue = @CreditValueEx + @CreditValueToday;

                        SET @SqlStmt =
                        N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                        ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                        ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                        ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                        ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
									MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
								WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
								  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
								  AND COA_CODE=''' + @AccountCode1 + ''' 
								  AND OUTLET_CODE=''' + @OutletCode + '''';

                        -- execute value for coa level 1'
                        EXECUTE sp_executesql @SqlStmt;

                        -- ## UPDATE TRX_SALDO DETAIL FOR COA LEVEL 1 ##'
                        SELECT
                            @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                            @MutValueDtl = ISNULL(MUT_VALUE, 0),
                            @DebitValueDtl = ISNULL(DB, 0),
                            @CreditValueDtl = ISNULL(CR, 0)
                        FROM AKT.dbo.TRX_SALDO_DTL
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode

                        print '-- execute update trx saldo detil coa level 1'
                        UPDATE AKT.dbo.TRX_SALDO_DTL
                        SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                            MUT_VALUE = MUT_VALUE + ISNULL(@MutValueToday,0),
                            END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@MutValueToday,0)),
                            DB = (DB + ISNULL(@DebitValueToday, 0)),
                            CR = (CR + ISNULL(@CreditValueToday, 0)),
                            MODIFY_USER = @UserID,
                            MODIFY_DATE = GETDATE()
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = @SystemDate
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        -- debug
                        -- SELECT 'TRX_SALDO_DTL YANG ASSET CUYYYYY ', * FROM AKT.dbo.TRX_SALDO_DTL  WHERE BRANCH_CODE = @BranchCode1
                        -- AND SALDO_DATE = @SystemDate
                        -- AND COA_CODE = @AccountCode1
                        -- AND OUTLET_CODE = @OutletCode

                        -- UPDATE BEGIN BALANCE FOR COA LEVEL 1'
                        IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                        BEGIN
                            SET @NextBal = NULL
                        END
                        ELSE
                        BEGIN
                            SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                            SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                            --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                            SET @SqlStmt =
                            N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                            ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
											WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
											  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
											  AND OUTLET_CODE=''' + @OutletCode + '''';

                            print '-- execute update begin balance coa level 1'
                            EXECUTE sp_executesql @SqlStmt;

                            --debug
                            -- SELECT 'COA LEVEL 1 ' + CONVERT(VARCHAR, @SystemDate), * FROM AKT.dbo.TRX_SALDO Z
                            -- WHERE Z.COA_CODE = @AccountCode1;
                        END
                    END
                END
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL

            -- Update Process Flag
            -- so coa from c_journal will cannot to EOD again
            print ''
            print '-- update coa proses flag to 1'
            UPDATE CF_JOURNAL_INFO
            SET PROCESS_FLAG = 1,
                MODIFY_USER = @UserID
            WHERE PROCESS_FLAG = 0
            AND BRANCH_CODE = @BranchCode
            AND OUTLET_CODE = @OutletCode
            AND JOURNAL_NO = @JournalNo
            AND ACCOUNT_CODE = @AccountCode
            AND SEQ_NO = @SeqNo; 
            print '-- finish COA LEVEL for ACCOUNT CODE: ' + @AccountCode
        END
        CLOSE c_journal
        DEALLOCATE c_journal

        -- SET PROFIT & LOSS SUMMARY
        BEGIN
            -- Acoount Laba
            print '-- cek account laba'
            SELECT
                @accountLaba = VALUE
            FROM GLOBAL_PARAMETER_MST
            WHERE CONDITION = 'ACCOUNT_TEMP'
            AND SUB_CONDITION = '002';
            print '-- account laba is ' + @accountLaba
            print ''
            -- Laba

            --Pendapatan 
            -- print '-- cek pendapatan ' + @BranchCode + ' ' +  @OutletCode + ' ' + CONVERT(VARCHAR, @TransactionDate)
            BEGIN
                SELECT
                    @pendapatanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                    @pendapatanDebit = ISNULL(SUM(A.DB), 0),
                    @pendapatanCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                    COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '3%'
            END

            -- print '-- pendapatan is ' + CONVERT(NVARCHAR(MAX), @pendapatanValue)
            -- print ''

            --Beban
            -- print '-- cek beban'
            BEGIN
                SELECT
                    @bebanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                    @bebanDebit = ISNULL(SUM(A.DB), 0),
                    @bebanCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                    COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '4%'
            END

            -- cek income tax, coa nomor 5
            BEGIN
                SELECT
                    @incomeTaxMut = ISNULL(SUM(A.MUT_VALUE), 0),
                    @incomeTaxDebit = ISNULL(SUM(A.DB), 0),
                    @incomeTaxCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                    COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '5%'
            END

            SET @labaValue = @pendapatanValue + @bebanValue + @incomeTaxMut;
            SET @DebitValueToday = @pendapatanDebit + @bebanDebit + @incomeTaxDebit;
            SET @CreditValueToday = @pendapatanCredit + @bebanCredit + @incomeTaxCredit;

            -- print '-- pendapatanDebit: ' + CONVERT(VARCHAR, @pendapatanDebit)
            -- print '-- pendapatanCredit: ' + CONVERT(VARCHAR, @pendapatanCredit)
            -- print '-- bebanDebit: ' + CONVERT(VARCHAR, @bebanDebit)
            -- print '-- bebanCredit: ' + CONVERT(VARCHAR, @bebanCredit)
            -- print '-- DebitValueToday: ' + CONVERT(VARCHAR, @DebitValueToday)
            -- print '-- CreditValueToday: ' + CONVERT(VARCHAR, @CreditValueToday)

            -- print '-- laba value is ' + CONVERT(NVARCHAR(MAX), @labaValue)
            -- print ''
            
            print '-- cek coa parent or grandparents for coa accountLaba: ' + @accountLaba
            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVELS
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @accountLaba
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL + 1 AS LEVELS
            FROM COA_MST,
                    COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 0)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                print '-- COA: ' + @AccountCode2 + ' LEVEL: ' + @Level + ' COA ACUM: ' + @CoaAcum;
                print ''
                print '-- processing update trx saldo, update begin balance next month trx saldo'
                print ''

                IF @@FETCH_STATUS = -1
                    BREAK

                SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
                                                    @MutValueEx NUMERIC(20,4) OUTPUT,
                                                    @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                    @CreditValueEx NUMERIC(15,2) OUTPUT';

                SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                                @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                                FROM AKT.dbo.TRX_SALDO
                                                WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                                AND COA_CODE=''' + @AccountCode2 + ''' 
                                                AND OUTLET_CODE=''' + @OutletCode + '''';

                EXECUTE sp_executesql @SqlStmt,
                                    @ParmDefinition,
                                    @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                    @MutValueEx = @MutValueEx OUTPUT,
                                    @DebitValueEx = @DebitValueEx OUTPUT,
                                    @CreditValueEx = @CreditValueEx OUTPUT;

                -- print '-- update profit and loss'
                -- print '-- ColumnBeginEx: ' + CONVERT(VARCHAR(MAX), @ColumnBeginEx);
                -- print '-- MutValueEx: ' + CONVERT(VARCHAR(MAX), @MutValueEx);
                -- print '-- DebitValueEx: ' + CONVERT(VARCHAR(MAX), @DebitValueEx);
                -- print '-- CreditValueEx: ' + CONVERT(VARCHAR(MAX), @CreditValueEx);
                -- print ''

                SET @MutValue = @MutValueEx + @labaValue;
                SET @EndBal = @ColumnBeginEx + @MutValue;
                SET @DebitValue = @DebitValueEx + @DebitValueToday;
                SET @CreditValue = @CreditValueEx + @CreditValueToday;

                SET @SqlStmt =
                N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
                                    MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                                WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                AND COA_CODE=''' + @AccountCode2 + ''' 
                                AND OUTLET_CODE=''' + @OutletCode + '''';

                -- print 'query udate coa profit and loss';
                -- print @SqlStmt;
                EXECUTE sp_executesql @SqlStmt;

                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;


                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                    MUT_VALUE = (MUT_VALUE + @labaValue),
                    -- END_BAL = (ISNULL(@EndBalDtl, 0) + (MUT_VALUE + @MutValueToday)),
                    END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@labaValue,0)),
                    DB = (DB + ISNULL(@DebitValueToday, 0)),
                    CR = (CR + ISNULL(@CreditValueToday, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = @SystemDate
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                BEGIN
                    SET @NextBal = NULL
                END
                ELSE
                BEGIN
                    SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                    SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                    --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                    SET @SqlStmt =
                    N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                    ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
                                            WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                            AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode2 + '''
                                            AND OUTLET_CODE=''' + @OutletCode + '''';

                    EXECUTE sp_executesql @SqlStmt;
                END

                -- check if #ListCoaLevel in level 2
                -- else skip this script
                print '-- PROFIT CHECK IF COA HAS COA ACUM LEVEL 1?'
                print '-- COA: ' + @AccountCode2 + ' LEVEL: ' + @Level + ' COA ACUM: ' + @CoaAcum;
                print '-- COA LEVEL: ' + @Level

                -- debug
                -- SELECT 'PROFIT BUKAN LEVEL 1 ' + CONVERT(VARCHAR, @SystemDate), * FROM AKT.dbo.TRX_SALDO Z
                -- WHERE Z.COA_CODE = @AccountCode2 OR Z.COA_CODE = '100000000000';

                SET @CoaAcumLevel = NULL -- restart variable
                SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'
                
                print '-- CoaAcumLevel is ' + @CoaAcumLevel

                IF (@CoaAcumLevel = '1')
                BEGIN
                    SELECT
                        @AccountCode1 = ISNULL(COA_CODE, ' ')
                    FROM COA_MST
                    WHERE COA_CODE = @CoaAcum
                    AND ACTIVE_STATUS = 1;

                    IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
                    BEGIN
                        SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(15,2) OUTPUT,
                                                            @MutValueEx NUMERIC(15,2) OUTPUT,
                                                            @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                            @CreditValueEx NUMERIC(15,2) OUTPUT';

                        SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                                            @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                                    FROM AKT.dbo.TRX_SALDO
                                                    WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                    AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
                                                    AND OUTLET_CODE=''' + @OutletCode + '''';

                        EXECUTE sp_executesql @SqlStmt,
                                            @ParmDefinition,
                                            @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                            @MutValueEx = @MutValueEx OUTPUT,
                                            @DebitValueEx = @DebitValueEx OUTPUT,
                                            @CreditValueEx = @CreditValueEx OUTPUT;


                        SET @MutValue = @MutValueEx + @labaValue;
                        SET @EndBal = @ColumnBeginEx + @MutValue;
                        SET @DebitValue = @DebitValueEx + @DebitValueToday;
                        SET @CreditValue = @CreditValueEx + @CreditValueToday;

                        SET @SqlStmt =
                        N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                        ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                        ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                        ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                        ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
                                            MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                                        WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                        AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                        AND COA_CODE=''' + @AccountCode1 + ''' 
                                        AND OUTLET_CODE=''' + @OutletCode + '''';

                        EXECUTE sp_executesql @SqlStmt;


                        SELECT
                            @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                            @MutValueDtl = ISNULL(MUT_VALUE, 0),
                            @DebitValueDtl = ISNULL(DB, 0),
                            @CreditValueDtl = ISNULL(CR, 0)
                        FROM AKT.dbo.TRX_SALDO_DTL
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;


                        UPDATE AKT.dbo.TRX_SALDO_DTL
                        SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                            MUT_VALUE = MUT_VALUE + ISNULL(@labaValue, 0),
                            END_BAL = (ISNULL(@EndBalDtl, 0) + (MUT_VALUE + @labaValue)),
                            DB = (DB + ISNULL(@DebitValueToday, 0)),
                            CR = (CR + ISNULL(@CreditValueToday, 0)),
                            MODIFY_USER = @UserID,
                            MODIFY_DATE = GETDATE()
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = @SystemDate
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                        BEGIN
                            SET @NextBal = NULL
                        END
                        ELSE
                        BEGIN
                            SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1

                            SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                            --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                            SET @SqlStmt =
                            N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                            ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ' 
                                                    WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                    AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
                                                    AND OUTLET_CODE=''' + @OutletCode + '''';


                            EXECUTE sp_executesql @SqlStmt;

                            -- debug
                            -- SELECT 'PROFIT LEVEL 1 ' + CONVERT(VARCHAR, @SystemDate), * FROM AKT.dbo.TRX_SALDO Z
                            -- WHERE Z.COA_CODE = @AccountCode1;

                        END
                    END
                END
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL
        
        -- end profit & summary
        END

        -- delete and insert trx_saldo_detail only works if fiscal status of present month and year active (status = 1)
        print '-- delete saldo detail'
        -- DELETE SALDO DTL 
        DELETE AKT.dbo.TRX_SALDO_DTL
        WHERE BRANCH_CODE = @BranchCode
            AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
            AND COA_CODE IN (SELECT
                COA_CODE
            FROM AKT.dbo.TRX_SALDO
            WHERE BRANCH_CODE = @BranchCode
            AND YEAR_PERIOD = @currYear);

        print '-- insert saldo detail'
        --INSERT SALDO DTL
        INSERT AKT.dbo.TRX_SALDO_DTL (BRANCH_CODE,
        OUTLET_CODE,
        SALDO_DATE,
        COA_CODE,
        BEGIN_BAL,
        MUT_VALUE,
        END_BAL,
        DB,
        CR,
        CREATE_USER,
        CREATE_DATE)
            SELECT
                @BranchCode,
                A.OUTLET_CODE,
                DATEADD(DAY, +1, @SystemDate),
                A.COA_CODE,
                ISNULL((SELECT
                    B.END_BAL
                FROM AKT.dbo.TRX_SALDO_DTL AS B
                WHERE B.BRANCH_CODE = @BranchCode
                AND B.SALDO_DATE = CONVERT(varchar, @SystemDate)
                AND B.COA_CODE = A.COA_CODE
                AND B.OUTLET_CODE = A.OUTLET_CODE), 0),
                0,
                ISNULL((SELECT
                    B2.END_BAL
                FROM AKT.dbo.TRX_SALDO_DTL AS B2
                WHERE B2.BRANCH_CODE = @BranchCode
                AND B2.SALDO_DATE = CONVERT(varchar, @SystemDate)
                AND B2.COA_CODE = A.COA_CODE
                AND B2.OUTLET_CODE = A.OUTLET_CODE), 0),
                0,
                0,
                @UserID,
                @SystemDate
            FROM AKT.dbo.TRX_SALDO AS A
            WHERE A.BRANCH_CODE = @BranchCode
            AND A.YEAR_PERIOD = @currYear;

        SET @FlagSuccess = 'Y';

        print '-- update eod log'
        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = 'Y',
            [ERROR_MESSAGE] = 'SUCCESS',
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD';
        print 'END EOD'
    END TRY
    BEGIN CATCH
        SET @FlagSuccess = 'N';

        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = @FlagSuccess,
            [ERROR_MESSAGE] = CONVERT(nvarchar(5), ERROR_LINE()) + ': ' + ERROR_MESSAGE(),
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOD';

        SELECT
            ERROR_NUMBER() number,
            ERROR_LINE() line,
            ERROR_MESSAGE() msg
    END CATCH
END