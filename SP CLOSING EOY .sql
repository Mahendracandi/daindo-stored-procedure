USE [LAMP_2018]
GO
/****** Object:  StoredProcedure [dbo].[SP_CLOSING_EOY]    Script Date: 10/30/2020 14:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_CLOSING_EOY] @BranchCode nvarchar(30),
@SystemDate date,
@UserID nvarchar(24),
@FlagSuccess char(1) OUTPUT
AS
BEGIN
    BEGIN TRY
        SET NOCOUNT ON;
        DECLARE @vMaxSeq_ACC int,
                @vCutoffPeriodCollection int;
        SELECT
            @vMaxSeq_ACC = ISNULL(MAX(SEQUENCE_NO), 0) + 1
        FROM EOD_LOG
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOY ';
        IF @vMaxSeq_ACC = 1
        BEGIN
            INSERT INTO EOD_LOG (BRANCH_CODE,
            CURRENT_TRANSACTION_DATE,
            SEQUENCE_NO,
            EOD_JOB_TYPE,
            [DESCRIPTION],
            START_TIME,
            END_TIME,
            [ERROR_MESSAGE],
            CREATE_USER,
            CREATE_DATE,
            START_DATE,
            SR_NO)
                VALUES (@BranchCode, @SystemDate, @vMaxSeq_ACC, 'SAL', 'SP_CLOSING_EOY', GETDATE(), NULL, NULL, 'EOY', GETDATE(), GETDATE(), 16);
        END;
        ELSE
        BEGIN
            UPDATE EOD_LOG
            SET 
                START_DATE = GETDATE(),
                START_TIME = GETDATE(),
                END_DATE = NULL,
                END_TIME = NULL,
                MODIFY_USER = 'EOY',
                MODIFY_DATE = GETDATE(),
                SEQUENCE_NO = @vMaxSeq_ACC,
                SR_NO = 6
            WHERE BRANCH_CODE = @BranchCode
            AND CURRENT_TRANSACTION_DATE = @SystemDate
            AND EOD_JOB_TYPE = 'SAL'
            AND SEQUENCE_NO = @vMaxSeq_ACC - 1
            AND [DESCRIPTION] = 'SP_CLOSING_EOY';
        END;

        DECLARE @RowCount int;
        DECLARE @BranchCode1 nvarchar(30),
                @OutletCode nvarchar(30),
                @JournalNo nvarchar(30),
                @TransactionDate date,
                @AccountCode nvarchar(30),
                @PosJournal nvarchar(1),
                @CurrencyCode nvarchar(3),
                @Amount numeric(15, 2),
                @SeqNo numeric(4),
                @MonthPeriod numeric(2),
                @YearPeriod nvarchar(4);
        DECLARE @MutValueToday numeric(15, 2),
                @DebitValueToday numeric(15, 2),
                @CreditValueToday numeric(15, 2),
                @MutValueEx numeric(15, 2),
                @DebitValueEx numeric(15, 2),
                @CreditValueEx numeric(15, 2),
                @ColumnBeginEx numeric(15, 2),
                @ColumnBegin nvarchar(max),
                @ColumnEnd nvarchar(max),
                @ColumnMut nvarchar(max),
                @ColumnDebit nvarchar(max),
                @ColumnCredit nvarchar(max),
                @MutValue numeric(15, 2),
                @EndBal numeric(15, 2),
                @DebitValue numeric(15, 2),
                @CreditValue numeric(15, 2);
        DECLARE @AccountCode2 nvarchar(30),
                @Level nvarchar(2),
                @CoaAcum nvarchar(30),
                @CoaAcumLevel nvarchar(2);
        DECLARE @SqlStmt nvarchar(max),
                @ParmDefinition nvarchar(max);
        DECLARE @EndBalDtl numeric(15, 2),
                @MutValueDtl numeric(15, 2),
                @DebitValueDtl numeric(15, 2),
                @CreditValueDtl numeric(15, 2);
        DECLARE @ColumnBeginBal nvarchar(100),
                @ColumnEndBal nvarchar(100),
                @ColumnMutValue nvarchar(100),
                @ColumnDebitValue nvarchar(100),
                @ColumnCreditValue nvarchar(100);
        DECLARE @Count numeric(5);
        DECLARE @NextBal numeric(10),
                @AccountCode1 nvarchar(30),
                @pendapatanValue numeric(15, 2),
                @bebanValue numeric(15, 2),
                @labaValue numeric(15, 2),
                @accountLaba nvarchar(30),
                @currMonth numeric(2),
                @currYear numeric(4);
        DECLARE @pendapatanDebit numeric(15, 2),
                @pendapatanCredit numeric(15, 2),
                @bebanDebit numeric(15, 2),
                @bebanCredit numeric(15, 2);
        DECLARE @incomeTaxMut numeric(15, 2),
                @incomeTaxDebit numeric(15, 2),
                @incomeTaxCredit numeric(15, 2);

        DECLARE @coaCurrentYearEarnings VARCHAR (30) = '220009000001';
        DECLARE @coaRetainedEarnings VARCHAR (30) = '220008000001';
        DECLARE @coaPLSummary VARCHAR(30) = '910000000000';
        DECLARE @endBalPLSummary numeric (15,2);
        DECLARE @debitPLSummary numeric (15,2);
        DECLARE @creditPLSummary numeric (15,2);
        DECLARE @mutValueCurrentYearEarnings numeric (15,2);
        DECLARE @endBalCurrentYearEarnings numeric (15,2);

		DECLARE @beginBalFirstMonth NUMERIC(15,2);
        DECLARE @mutValFirstMonth NUMERIC(15,2);
        DECLARE @endBalFirstMonth NUMERIC(15,2);

        -- check jurnal in open month
        SELECT A.JOURNAL_NO
        INTO #CoaStatusOpen
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE <= @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('3')
        ORDER BY A.TRANSACTION_DATE ASC;

        IF (EXISTS(SELECT 1 FROM #CoaStatusOpen))
        BEGIN
            print 'FOUND JOURNAL IN OPEN MONTH'
            EXECUTE [dbo].[SP_CLOSING_EOD_FISCAL_OPEN] 
			   @BranchCode
			  ,@SystemDate
			  ,@UserID
			  ,@FlagSuccess OUTPUT
        END
        DROP TABLE #CoaStatusOpen

        -- update to closed if any fiscal month has open status
        IF (EXISTS(SELECT * FROM AKT.dbo.CALENDAR_FISCAL_MST WHERE [STATUS] = '3'))
        BEGIN
            print 'CLOSE OPEN FISCAL MONTH'
	        UPDATE AKT.dbo.CALENDAR_FISCAL_MST SET [STATUS] = '2' WHERE [STATUS] = '3'
        END

        -- check jurnal in active month but before system date
        SELECT A.JOURNAL_NO
        INTO #CoaActiveBackdate
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE < @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('1')
        ORDER BY A.TRANSACTION_DATE ASC;

        IF (EXISTS(SELECT 1 FROM #CoaActiveBackdate))
        BEGIN
            print 'FOUND JOURNAL IN ACTIVE MONTH'
            EXECUTE [dbo].[SP_CLOSING_EOD_ACTIVE_BACKDATE] 
			   @BranchCode
			  ,@SystemDate
			  ,@UserID
			  ,@FlagSuccess OUTPUT
        END
        DROP TABLE #CoaActiveBackdate

        -- check if present month and year are status active (status 1)
        SELECT
            @currMonth = [MONTH],
            @currYear = [YEAR]
        FROM AKT.dbo.CALENDAR_FISCAL_MST
        WHERE [STATUS] = 1

        PRINT 'START EOY: ' + CONVERT(varchar, @SystemDate)
        -- READ AND SET #TEMPC0
        DECLARE c_journal CURSOR LOCAL FORWARD_ONLY FOR
        SELECT
            A.BRANCH_CODE,
            A.JOURNAL_NO,
            A.OUTLET_CODE,
            A.VALUE_DATE,
            A.ACCOUNT_CODE AS ACCOUNT_CODE,
            A.POS_JOURNAL,
            A.CURRENCY_CODE,
            A.AMOUNT,
            A.SEQ_NO,
            B.[MONTH],
            B.[YEAR],
            (CASE
                WHEN A.POS_JOURNAL = 'D' THEN A.AMOUNT
                ELSE 0
            END) AS DEBIT,
            (CASE
                WHEN A.POS_JOURNAL = 'C' THEN A.AMOUNT
                ELSE 0
            END) AS CREDIT
        FROM CF_JOURNAL_INFO AS A,
             AKT.dbo.CALENDAR_FISCAL_MST AS B
        WHERE MONTH(A.TRANSACTION_DATE) = B.[MONTH]
        AND YEAR(A.TRANSACTION_DATE) = B.[YEAR]
        AND A.BRANCH_CODE = @BranchCode
        AND A.TRANSACTION_DATE = @SystemDate
        AND A.PROCESS_FLAG = 0
        AND B.[STATUS] IN ('1')
        ORDER BY A.TRANSACTION_DATE ASC;

        OPEN c_journal
        WHILE 1 = 1
        BEGIN
            FETCH c_journal INTO @BranchCode1, @JournalNo, @OutletCode, @TransactionDate,
            @AccountCode, @PosJournal, @CurrencyCode, @Amount, @SeqNo, @MonthPeriod,
            @YearPeriod, @DebitValueToday, @CreditValueToday

            IF @@fetch_status = -1
                BREAK;

            PRINT '## COA NUMBER ## ' + @AccountCode;
            -- set field name based on month of transaction #SetField
            SET @ColumnBegin = 'BEGIN_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnEnd = 'END_BAL_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnMut = 'MUT_VALUE_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnDebit = 'DB_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');
            SET @ColumnCredit = 'CR_' + ISNULL(CAST(@MonthPeriod AS varchar(max)), '');

            -- Set mut_value based on amount in cf_journal_info #SetMutValueToday'
            IF @PosJournal = 'D'
            BEGIN
                -- amount using positive number'
                SET @MutValueToday = @Amount;
            END;
            ELSE
            BEGIN
                -- amount using negative number'
                SET @MutValueToday = -1 * @Amount;
            END;

            -- get coa parent or grandparent (coa header) #listCoaLevel'
            -- query below will result something like this:
            --
            -- COA_CODE    | LEVEL | COA_ACCUM
            -- 110201000001	  7	     110200000000
            -- 110200000000	  3	     110000000000
            -- 110000000000	  2	     100000000000

            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVEL_
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @AccountCode
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL_ + 1 AS LEVELS
            FROM COA_MST,
                 COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 1)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            -- iterate coa based on #ListCoaLevel'
            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                IF @@FETCH_STATUS = -1
                    BREAK

                -- ## UPDATE TRX SALDO ##'
                SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
											@MutValueEx NUMERIC(20,4) OUTPUT,
											@DebitValueEx NUMERIC(15,2) OUTPUT,
											@CreditValueEx NUMERIC(15,2) OUTPUT';

                -- get existing value of field BEGIN_BAL, MUTA_VAL, END_BAL, DEB_, CRE_  from trx_saldo.
                -- based on field in #SetField. lets call it #prepareValueCoa                
                SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
									 @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
									 FROM AKT.dbo.TRX_SALDO
									 WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
									   AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
									   AND COA_CODE=''' + @AccountCode2 + ''' 
									   AND OUTLET_CODE=''' + @OutletCode + '''';

                EXECUTE sp_executesql @SqlStmt,
                                      @ParmDefinition,
                                      @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                      @MutValueEx = @MutValueEx OUTPUT,
                                      @DebitValueEx = @DebitValueEx OUTPUT,
                                      @CreditValueEx = @CreditValueEx OUTPUT;

                -- SET NEW VALUE FOR ALL LEVEL COA EXCEPT LEVEL 1.
                -- set existing mut_value, end_balance, debit_value, credit_value #setValueCoa
                SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@MutValueToday, 0);
                SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);
                SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@DebitValueToday, 0);
                SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@CreditValueToday, 0);

                -- prepare update dynamic query to trx_saldo based on month
                -- ColumnMut -> field name check from #SetField
                -- MutValue -> value from #setValueCoa
                SET @SqlStmt =
                N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ', ' +
                ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
							MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
						WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
						  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode2 + ''' 
						  AND OUTLET_CODE=''' + @OutletCode + '''';

                -- execute update coa query'
                EXECUTE sp_executesql @SqlStmt;

                -- ## UPDATE TRX_SALDO DETAIL ##'
                -- get trx_saldo detail with day -1 from date transaction
                -- and set to endBalDtl, mutValueDtl, debitValueDtl, creaditValueDtl
                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate) -- get day before
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- execute update trx saldo detail query'
                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                    MUT_VALUE = MUT_VALUE + ISNULL(@MutValueToday, 0),
                    END_BAL = ISNULL(@EndBalDtl, 0) + (MUT_VALUE + ISNULL(@MutValueToday, 0)),
                    DB = (DB + ISNULL(@DebitValueToday, 0)),
                    CR = (CR + ISNULL(@CreditValueToday, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = @SystemDate
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT
                --     'Bukan LVL 1 TRX_SALDO_DTL CUYYYYY ',
                --     *
                -- FROM AKT.dbo.TRX_SALDO_DTL
                -- WHERE BRANCH_CODE = @BranchCode1
                -- AND SALDO_DATE = @SystemDate
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode

                -- ## UPDATE BEGIN BALANCE FOR NEXT MONTH ##'
                -- check if this transaction is on december
                -- if true, then nextBal = null
                IF CAST(@MonthPeriod AS numeric(2)) = 12
                BEGIN
                    PRINT '-- NO NEED TO UPDATE BEGIN BALANCE'
                    PRINT ''
                    SET @NextBal = NULL
                END
                ELSE
                BEGIN
                    -- if not in december
                    -- update beginBalace, mutValue, endBalance for next month
                    SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                    SET @COLUMNBEGINBAL = 'BEGIN_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    SET @COLUMNMUTVALUE = 'MUT_VALUE_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    SET @COLUMNENDBAL = 'END_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                    --SET @COLUMNDEBITVALUE = 'DB_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');
                    --SET @COLUMNCREDITVALUE = 'CR_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');

                    SET @SQLSTMT =
                    N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                    ISNULL(@COLUMNBEGINBAL, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
									WHERE BRANCH_CODE=''' + @BRANCHCODE1 + ''' 
									  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YEARPERIOD) + ' 
									  AND COA_CODE=''' + @ACCOUNTCODE2 + ''' 
									  AND OUTLET_CODE=''' + @OutletCode + '''';

                    -- execute begin balcance for next month'
                    EXECUTE SP_EXECUTESQL @SQLSTMT;
                END

                -- debug
                -- SELECT
                --     'BUKAN LEVEL 1 ' + CONVERT(varchar, @SystemDate),
                --     *
                -- FROM AKT.dbo.TRX_SALDO Z
                -- WHERE Z.COA_CODE = @AccountCode2
                -- OR Z.COA_CODE = '100000000000';

                -- CHECK IF COA HAS COA ACUM LEVEL 1?'
                SET @CoaAcumLevel = NULL -- restart variable
                SELECT
                    @CoaAcumLevel = [LEVEL]
                FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum
                AND [LEVEL] = '1'

                IF (@CoaAcumLevel = '1')
                BEGIN
                    -- THIS IS COA LEVEL 1'
                    SELECT
                        @AccountCode1 = ISNULL(COA_CODE, ' ')
                    FROM COA_MST
                    WHERE COA_CODE = @CoaAcum -- CoaAcum -> coa parent from #ListCoaLevel
                    AND ACTIVE_STATUS = 1;

                    -- if account level 1 found, execute this script'
                    -- else skip this script
                    IF @AccountCode1 <> ' '
                        OR @AccountCode1 IS NOT NULL
                    BEGIN
                        SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(15,2) OUTPUT,
													@MutValueEx NUMERIC(15,2) OUTPUT,
													@DebitValueEx NUMERIC(15,2) OUTPUT,
													@CreditValueEx NUMERIC(15,2) OUTPUT';

                        -- debug
                        -- SELECT
                        --     'INIT COA LEVEL 1 ' + CONVERT(varchar, @SystemDate),
                        --     *
                        -- FROM AKT.dbo.TRX_SALDO Z
                        -- WHERE Z.COA_CODE = @AccountCode1;

                        -- get existing value of field BEGIN_BAL, MUTA_VAL, END_BAL, DEB_, CRE_  from trx_saldo. 
                        -- based on field in #SetField. lets call it #prepareValueCoaLevel1
                        SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
													@DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
											 FROM AKT.dbo.TRX_SALDO
											 WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
											   AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
											   AND OUTLET_CODE=''' + @OutletCode + '''';

                        -- execute #prepareValueCoaLevel1'
                        EXECUTE sp_executesql @SqlStmt,
                                              @ParmDefinition,
                                              @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                              @MutValueEx = @MutValueEx OUTPUT,
                                              @DebitValueEx = @DebitValueEx OUTPUT,
                                              @CreditValueEx = @CreditValueEx OUTPUT;

                        -- SET NEW VALUE FOR COA LEVEL 1. #setValueCoaLevel1
                        SET @MutValue = @MutValueEx + @MutValueToday;
                        SET @EndBal = @ColumnBeginEx + @MutValue;
                        SET @DebitValue = @DebitValueEx + @DebitValueToday;
                        SET @CreditValue = @CreditValueEx + @CreditValueToday;

                        SET @SqlStmt =
                        N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                        ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                        ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                        ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                        ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
									MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
								WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
								  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
								  AND COA_CODE=''' + @AccountCode1 + ''' 
								  AND OUTLET_CODE=''' + @OutletCode + '''';

                        -- execute value for coa level 1'
                        EXECUTE sp_executesql @SqlStmt;

                        -- ## UPDATE TRX_SALDO DETAIL FOR COA LEVEL 1 ##'
                        SELECT
                            @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                            @MutValueDtl = ISNULL(MUT_VALUE, 0),
                            @DebitValueDtl = ISNULL(DB, 0),
                            @CreditValueDtl = ISNULL(CR, 0)
                        FROM AKT.dbo.TRX_SALDO_DTL
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode

                        PRINT '-- execute update trx saldo detil coa level 1'
                        UPDATE AKT.dbo.TRX_SALDO_DTL
                        SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                            MUT_VALUE = MUT_VALUE + ISNULL(@MutValueToday, 0),
                            END_BAL = ISNULL(@EndBalDtl, 0) + (MUT_VALUE + ISNULL(@MutValueToday, 0)),
                            DB = (DB + ISNULL(@DebitValueToday, 0)),
                            CR = (CR + ISNULL(@CreditValueToday, 0)),
                            MODIFY_USER = @UserID,
                            MODIFY_DATE = GETDATE()
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = @SystemDate
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        -- debug
                        -- SELECT
                        --     'TRX_SALDO_DTL YANG ASSET CUYYYYY ',
                        --     *
                        -- FROM AKT.dbo.TRX_SALDO_DTL
                        -- WHERE BRANCH_CODE = @BranchCode1
                        -- AND SALDO_DATE = @SystemDate
                        -- AND COA_CODE = @AccountCode1
                        -- AND OUTLET_CODE = @OutletCode

                        -- UPDATE BEGIN BALANCE FOR COA LEVEL 1'
                        IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                        BEGIN
                            SET @NextBal = NULL
                        END
                        ELSE
                        BEGIN
                            SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                            SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                            --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                            SET @SqlStmt =
                            N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                            ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
											WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
											  AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
											  AND OUTLET_CODE=''' + @OutletCode + '''';

                            PRINT '-- execute update begin balance coa level 1'
                            EXECUTE sp_executesql @SqlStmt;

                            --debug
                            -- SELECT
                            --     'COA LEVEL 1 ' + CONVERT(varchar, @SystemDate),
                            --     *
                            -- FROM AKT.dbo.TRX_SALDO Z
                            -- WHERE Z.COA_CODE = @AccountCode1;
                        END
                    END
                END
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL

            -- Update Process Flag
            -- so coa from c_journal will cannot to EOD again
            PRINT ''
            PRINT '-- update coa proses flag to 1'
            UPDATE CF_JOURNAL_INFO
            SET PROCESS_FLAG = 1,
                MODIFY_USER = @UserID
            WHERE PROCESS_FLAG = 0
            AND BRANCH_CODE = @BranchCode
            AND OUTLET_CODE = @OutletCode
            AND JOURNAL_NO = @JournalNo
            AND ACCOUNT_CODE = @AccountCode
            AND SEQ_NO = @SeqNo;
            PRINT '-- finish COA LEVEL for ACCOUNT CODE: ' + @AccountCode
        END
        CLOSE c_journal
        DEALLOCATE c_journal

        -- SET PROFIT & LOSS SUMMARY
        BEGIN
            -- Acoount Laba
            PRINT '-- cek account laba'
            SELECT
                @accountLaba = VALUE
            FROM GLOBAL_PARAMETER_MST
            WHERE CONDITION = 'ACCOUNT_TEMP'
            AND SUB_CONDITION = '002';
            PRINT '-- account laba is ' + @accountLaba
            PRINT ''
            -- Laba

            --Pendapatan 
            -- PRINT '-- cek pendapatan ' + @BranchCode + ' ' + @OutletCode + ' ' + CONVERT(varchar, @TransactionDate)
            BEGIN
                SELECT
                    @pendapatanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                    @pendapatanDebit = ISNULL(SUM(A.DB), 0),
                    @pendapatanCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                     COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '3%'
            END

            -- PRINT '-- pendapatan is ' + CONVERT(nvarchar(max), @pendapatanValue)
            -- PRINT ''

            --Beban
            PRINT '-- cek beban'
            BEGIN
                SELECT
                    @bebanValue = ISNULL(SUM(A.MUT_VALUE), 0),
                    @bebanDebit = ISNULL(SUM(A.DB), 0),
                    @bebanCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                     COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '4%'
            END

            BEGIN
                SELECT
                    @incomeTaxMut = ISNULL(SUM(A.MUT_VALUE), 0),
                    @incomeTaxDebit = ISNULL(SUM(A.DB), 0),
                    @incomeTaxCredit = ISNULL(SUM(A.CR), 0)
                FROM AKT.dbo.TRX_SALDO_DTL AS A,
                    COA_MST AS B
                WHERE A.BRANCH_CODE = @BranchCode
                AND A.OUTLET_CODE = @OutletCode
                -- AND A.SALDO_DATE = @TransactionDate
                AND A.SALDO_DATE = @SystemDate
                AND A.COA_CODE = B.COA_CODE
                AND B.HEADER_DETAIL = 'D'
                AND B.ACTIVE_STATUS = 1
                AND B.COA_CODE LIKE '5%'
            END

            SET @labaValue = @pendapatanValue + @bebanValue + @incomeTaxMut;
            SET @DebitValueToday = @pendapatanDebit + @bebanDebit + @incomeTaxDebit;
            SET @CreditValueToday = @pendapatanCredit + @bebanCredit + @incomeTaxCredit;

            -- PRINT '-- pendapatanDebit: ' + CONVERT(varchar, @pendapatanDebit)
            -- PRINT '-- pendapatanCredit: ' + CONVERT(varchar, @pendapatanCredit)
            -- PRINT '-- bebanDebit: ' + CONVERT(varchar, @bebanDebit)
            -- PRINT '-- bebanCredit: ' + CONVERT(varchar, @bebanCredit)
            -- PRINT '-- DebitValueToday: ' + CONVERT(varchar, @DebitValueToday)
            -- PRINT '-- CreditValueToday: ' + CONVERT(varchar, @CreditValueToday)

            -- PRINT '-- laba value is ' + CONVERT(nvarchar(max), @labaValue)
            -- PRINT ''

            PRINT '-- cek coa parent or grandparents for coa accountLaba: ' + @accountLaba
            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVELS
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @accountLaba
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL + 1 AS LEVELS
            FROM COA_MST,
                 COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 0)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;
            PRINT '-- end cek coa parents accountLaba'
            PRINT ''

            PRINT '-- start looping'
            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                PRINT '-- COA: ' + @AccountCode2 + ' LEVEL: ' + @Level + ' COA ACUM: ' + @CoaAcum;
                PRINT ''
                PRINT '-- processing update trx saldo, update begin balance next month trx saldo'
                PRINT ''

                IF @@FETCH_STATUS = -1
                    BREAK

                SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
                                                    @MutValueEx NUMERIC(20,4) OUTPUT,
                                                    @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                    @CreditValueEx NUMERIC(15,2) OUTPUT';

                SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                                @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                                FROM AKT.dbo.TRX_SALDO
                                                WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                                AND COA_CODE=''' + @AccountCode2 + ''' 
                                                AND OUTLET_CODE=''' + @OutletCode + '''';

                EXECUTE sp_executesql @SqlStmt,
                                      @ParmDefinition,
                                      @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                      @MutValueEx = @MutValueEx OUTPUT,
                                      @DebitValueEx = @DebitValueEx OUTPUT,
                                      @CreditValueEx = @CreditValueEx OUTPUT;

                -- PRINT '-- update profit and loss'
                -- PRINT '-- ColumnBeginEx: ' + CONVERT(varchar(max), @ColumnBeginEx);
                -- PRINT '-- MutValueEx: ' + CONVERT(varchar(max), @MutValueEx);
                -- PRINT '-- DebitValueEx: ' + CONVERT(varchar(max), @DebitValueEx);
                -- PRINT '-- CreditValueEx: ' + CONVERT(varchar(max), @CreditValueEx);
                -- PRINT ''

                SET @MutValue = @MutValueEx + @labaValue;
                SET @EndBal = @ColumnBeginEx + @MutValue;
                SET @DebitValue = @DebitValueEx + @DebitValueToday;
                SET @CreditValue = @CreditValueEx + @CreditValueToday;

                SET @SqlStmt =
                N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
                                    MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                                WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                AND COA_CODE=''' + @AccountCode2 + ''' 
                                AND OUTLET_CODE=''' + @OutletCode + '''';

                -- PRINT 'query udate coa profit and loss';
                -- PRINT @SqlStmt;
                EXECUTE sp_executesql @SqlStmt;

                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;


                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                    MUT_VALUE = (MUT_VALUE + @labaValue),
                    -- END_BAL = (ISNULL(@EndBalDtl, 0) + (MUT_VALUE + @MutValueToday)),
                    END_BAL = ISNULL(@EndBalDtl, 0) + (MUT_VALUE + ISNULL(@labaValue, 0)),
                    DB = (DB + ISNULL(@DebitValueToday, 0)),
                    CR = (CR + ISNULL(@CreditValueToday, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode1
                AND SALDO_DATE = @SystemDate
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                BEGIN
                    SET @NextBal = NULL
                END
                ELSE
                BEGIN
                    SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1
                    SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                    --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                    --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                    SET @SqlStmt =
                    N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                    ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
                                            WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                            AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode2 + '''
                                            AND OUTLET_CODE=''' + @OutletCode + '''';

                    EXECUTE sp_executesql @SqlStmt;
                END

                -- check if #ListCoaLevel in level 2
                -- else skip this script
                PRINT '-- PROFIT CHECK IF COA HAS COA ACUM LEVEL 1?'
                PRINT '-- COA: ' + @AccountCode2 + ' LEVEL: ' + @Level + ' COA ACUM: ' + @CoaAcum;
                PRINT '-- COA LEVEL: ' + @Level

                -- debug
                -- SELECT
                --     'PROFIT BUKAN LEVEL 1 ' + CONVERT(varchar, @SystemDate),
                --     *
                -- FROM AKT.dbo.TRX_SALDO Z
                -- WHERE Z.COA_CODE = @AccountCode2
                -- OR Z.COA_CODE = '100000000000';

                SET @CoaAcumLevel = NULL -- restart variable
                SELECT
                    @CoaAcumLevel = [LEVEL]
                FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum
                AND [LEVEL] = '1'

                PRINT '-- CoaAcumLevel is ' + @CoaAcumLevel

                IF (@CoaAcumLevel = '1')
                BEGIN
                    SELECT
                        @AccountCode1 = ISNULL(COA_CODE, ' ')
                    FROM COA_MST
                    WHERE COA_CODE = @CoaAcum
                    AND ACTIVE_STATUS = 1;

                    IF @AccountCode1 <> ' '
                        OR @AccountCode1 IS NOT NULL
                    BEGIN
                        SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(15,2) OUTPUT,
                                                            @MutValueEx NUMERIC(15,2) OUTPUT,
                                                            @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                            @CreditValueEx NUMERIC(15,2) OUTPUT';

                        SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                                            @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                                    FROM AKT.dbo.TRX_SALDO
                                                    WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                    AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
                                                    AND OUTLET_CODE=''' + @OutletCode + '''';

                        EXECUTE sp_executesql @SqlStmt,
                                              @ParmDefinition,
                                              @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                              @MutValueEx = @MutValueEx OUTPUT,
                                              @DebitValueEx = @DebitValueEx OUTPUT,
                                              @CreditValueEx = @CreditValueEx OUTPUT;


                        SET @MutValue = @MutValueEx + @labaValue;
                        SET @EndBal = @ColumnBeginEx + @MutValue;
                        SET @DebitValue = @DebitValueEx + @DebitValueToday;
                        SET @CreditValue = @CreditValueEx + @CreditValueToday;

                        SET @SqlStmt =
                        N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                        ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                        ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ',' +
                        ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@DebitValue, 0)) + ',' +
                        ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(@CreditValue, 0)) + ',
                                            MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                                        WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                        AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' 
                                        AND COA_CODE=''' + @AccountCode1 + ''' 
                                        AND OUTLET_CODE=''' + @OutletCode + '''';

                        EXECUTE sp_executesql @SqlStmt;


                        SELECT
                            @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                            @MutValueDtl = ISNULL(MUT_VALUE, 0),
                            @DebitValueDtl = ISNULL(DB, 0),
                            @CreditValueDtl = ISNULL(CR, 0)
                        FROM AKT.dbo.TRX_SALDO_DTL
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;


                        UPDATE AKT.dbo.TRX_SALDO_DTL
                        SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                            MUT_VALUE = MUT_VALUE + ISNULL(@labaValue, 0),
                            END_BAL = (ISNULL(@EndBalDtl, 0) + (MUT_VALUE + @labaValue)),
                            DB = (DB + ISNULL(@DebitValueToday, 0)),
                            CR = (CR + ISNULL(@CreditValueToday, 0)),
                            MODIFY_USER = @UserID,
                            MODIFY_DATE = GETDATE()
                        WHERE BRANCH_CODE = @BranchCode1
                        AND SALDO_DATE = @SystemDate
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        IF (CAST(@MonthPeriod AS numeric(2)) = 12)
                        BEGIN
                            SET @NextBal = NULL
                        END
                        ELSE
                        BEGIN
                            SET @NextBal = CAST(@MonthPeriod AS numeric(2)) + 1

                            SET @ColumnBeginBal = 'BEGIN_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnMutValue = 'MUT_VALUE_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            SET @ColumnEndBal = 'END_BAL_' + ISNULL(CAST(@NextBal AS varchar(max)), '');
                            --SET @ColumnDebitValue = 'DB_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');
                            --SET @ColumnCreditValue = 'CR_'+ ISNULL(CAST(@NextBal AS varchar(max)),'');

                            SET @SqlStmt =
                            N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                            ISNULL(@ColumnBeginBal, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ' 
                                                    WHERE BRANCH_CODE=''' + @BranchCode1 + ''' 
                                                    AND YEAR_PERIOD=' + CONVERT(varchar(4), @YearPeriod) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
                                                    AND OUTLET_CODE=''' + @OutletCode + '''';

                            EXECUTE sp_executesql @SqlStmt;

                            -- debug
                            -- SELECT
                            --     'PROFIT LEVEL 1 ' + CONVERT(varchar, @SystemDate),
                            --     *
                            -- FROM AKT.dbo.TRX_SALDO Z
                            -- WHERE Z.COA_CODE = @AccountCode1;

                        END
                    END
                END
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL

        -- end update profit & loss summary
        END

		-- SET CURRENT YEAR EARNINGS
        BEGIN

            -- set dynamic field 
            SET @ColumnBegin = 'BEGIN_BAL_' + ISNULL(CAST(@currMonth AS varchar(max)), '');
            SET @ColumnEnd = 'END_BAL_' + ISNULL(CAST(@currMonth AS varchar(max)), '');
            SET @ColumnMut = 'MUT_VALUE_' + ISNULL(CAST(@currMonth AS varchar(max)), '');
            SET @ColumnDebit = 'DB_' + ISNULL(CAST(@currMonth AS varchar(max)), '');
            SET @ColumnCredit = 'CR_' + ISNULL(CAST(@currMonth AS varchar(max)), '');

            -- get data per outlet code
            DECLARE c__outlet_code CURSOR LOCAL FORWARD_ONLY FOR
            SELECT DISTINCT OUTLET_CODE FROM AKT.dbo.TRX_SALDO 
            WHERE BRANCH_CODE = @BranchCode
            AND YEAR_PERIOD = @currYear
            
            OPEN c__outlet_code
            WHILE 1 = 1
            BEGIN

                FETCH c__outlet_code INTO @OutletCode

                IF @@fetch_status = -1
                    BREAK;

                -- get PL Summary ending balance, debit, and credit
                SET @ParmDefinition = N'@endBalPLSummary NUMERIC(15,2) OUTPUT,
                                            @debitPLSummary numeric (15,2) OUTPUT,
                                            @creditPLSummary numeric (15,2) OUTPUT';
                
                SET @SqlStmt = N'SELECT @endBalPLSummary = ' + ISNULL(@ColumnEnd, 0) + ', @debitPLSummary = ' + ISNULL(@ColumnDebit, 0) + ',
                                        @creditPLSummary = ' + ISNULL(@ColumnCredit, 0) + '
                                        FROM AKT.dbo.TRX_SALDO
                                        WHERE BRANCH_CODE=''' + @BranchCode + ''' 
                                        AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' 
                                        AND COA_CODE=''' + @coaPLSummary + ''' 
                                        AND OUTLET_CODE=''' + @OutletCode + '''';
                
                -- PRINT 'INIT PL SUMMARY: ' + @SqlStmt
                
                -- SELECT 'INIT PL SUMMARY', END_BAL_12, DB_12, CR_12 
                -- FROM AKT.dbo.TRX_SALDO
                -- WHERE BRANCH_CODE = @BranchCode
                -- AND YEAR_PERIOD = @currYear
                -- AND COA_CODE = @coaPLSummary
                -- AND OUTLET_CODE = @OutletCode


                EXECUTE sp_executesql @SqlStmt,
                                        @ParmDefinition,
                                        @endBalPLSummary = @endBalPLSummary OUTPUT,
                                        @debitPLSummary = @debitPLSummary OUTPUT,
                                        @creditPLSummary = @creditPLSummary OUTPUT;
                
                -- mutasi value untuk current year earnings, didapatkan dari selisih debit kredit pada PL Summary
                -- kemungkinan akan mengeluarkan angka berupa positif, negatif, atau nol
                SET @mutValueCurrentYearEarnings = ISNULL(@debitPLSummary, 0) - ISNULL(@creditPLSummary, 0);

                -- PRINT 'CEK SQLSTMT: ' + @SqlStmt
                -- SELECT 'SqlStmt', @SqlStmt
                -- SELECT 'mutValueCurrentYearEarnings', @mutValueCurrentYearEarnings

                DECLARE C_COA_LEVEL CURSOR LOCAL FOR
                WITH COA_LEVEL AS (SELECT
                    COA_MST.COA_CODE,
                    COA_MST.LEVEL,
                    COA_MST.COA_ACCUM,
                    1 AS LEVELS
                FROM COA_MST
                WHERE COA_MST.COA_CODE = @coaCurrentYearEarnings
                AND ACTIVE_STATUS = 1
                UNION ALL
                SELECT
                    COA_MST.COA_CODE,
                    COA_MST.LEVEL,
                    COA_MST.COA_ACCUM,
                    COA_LEVEL.LEVEL + 1 AS LEVELS
                FROM COA_MST,
                        COA_LEVEL
                WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
                AND ACTIVE_STATUS = 1
                AND COA_MST.LEVEL > 0)

                SELECT DISTINCT
                    COA_LEVEL.COA_CODE,
                    COA_LEVEL.LEVEL,
                    COA_LEVEL.COA_ACCUM
                FROM COA_LEVEL
                WHERE COA_LEVEL.LEVEL > 1
                ORDER BY COA_LEVEL.LEVEL DESC;

                OPEN C_COA_LEVEL
                WHILE 1 = 1
                BEGIN
                    FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;
                    
                    IF @@FETCH_STATUS = -1
                        BREAK

                    -- ## UPDATE TRX SALDO ##'
                    SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
                                                @MutValueEx NUMERIC(20,4) OUTPUT,
                                                @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                @CreditValueEx NUMERIC(15,2) OUTPUT';
                    
                    -- PRINT @AccountCode2 + ', ParmDefinition: ' + @ParmDefinition
                    
                    SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                            @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                            FROM AKT.dbo.TRX_SALDO
                                            WHERE BRANCH_CODE=''' + @BranchCode + ''' 
                                            AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' 
                                            AND COA_CODE=''' + @AccountCode2 + ''' 
                                            AND OUTLET_CODE=''' + @OutletCode + '''';
                    
                    -- PRINT @AccountCode2 + ', @SqlStmt: ' + @SqlStmt

                    EXECUTE sp_executesql @SqlStmt,
                                            @ParmDefinition,
                                            @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                            @MutValueEx = @MutValueEx OUTPUT,
                                            @DebitValueEx = @DebitValueEx OUTPUT,
                                            @CreditValueEx = @CreditValueEx OUTPUT;
                                            

                    SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings * -1, 0);
                    SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);

                    -- cek apakah selisih debit kredit PL Summary, positif atau negatif.
                    -- jika positif, masukkan jumlah selisih ke kredit current year earnings.
                    -- jika negatif, masukkan ke debit
                    -- jika tidak keduanya, biarkan apa adanya. harusnya jumlah angka di kondisi ini adalah 0
                    IF (SIGN(@mutValueCurrentYearEarnings) = 1) 
                    BEGIN
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + 0;
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings, 0);
                    END
                    ELSE IF (SIGN(@mutValueCurrentYearEarnings) = -1)
                    BEGIN
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings, 0);
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + 0;
                    END
                    ELSE
                    BEGIN
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings, 0);
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings, 0);
                    END

                    SET @SqlStmt =
                    N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                    ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                    ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ', ' +
                    ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(ABS(@DebitValue), 0)) + ',' +
                    ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(ABS(@CreditValue), 0)) + ',
                                MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                            WHERE BRANCH_CODE=''' + @BranchCode + ''' 
                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' AND COA_CODE=''' + @AccountCode2 + ''' 
                                AND OUTLET_CODE=''' + @OutletCode + '''';

                    -- PRINT @AccountCode2 + ', UPDATE TRX SALOD: ' + @SqlStmt
                    
                    -- execute update trx saldo coa query'
                    EXECUTE sp_executesql @SqlStmt;

                    -- ## UPDATE TRX_SALDO DETAIL ##'
                    SELECT
                        @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                        @MutValueDtl = ISNULL(MUT_VALUE, 0),
                        @DebitValueDtl = ISNULL(DB, 0),
                        @CreditValueDtl = ISNULL(CR, 0)
                    FROM AKT.dbo.TRX_SALDO_DTL
                    WHERE BRANCH_CODE = @BranchCode
                    AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate) -- get day before
                    AND COA_CODE = @AccountCode2
                    AND OUTLET_CODE = @OutletCode;

                    -- execute update trx saldo detail query'
                    UPDATE AKT.dbo.TRX_SALDO_DTL
                    SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                        MUT_VALUE = MUT_VALUE + ISNULL(@mutValueCurrentYearEarnings * -1,0),
                        END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@mutValueCurrentYearEarnings * -1,0)),
                        DB = (DB + ISNULL(ABS(@DebitValue), 0)),
                        CR = (CR + ISNULL(ABS(@CreditValue), 0)),
                        MODIFY_USER = @UserID,
                        MODIFY_DATE = GETDATE()
                    WHERE BRANCH_CODE = @BranchCode
                    AND SALDO_DATE = @SystemDate
                    AND COA_CODE = @AccountCode2
                    AND OUTLET_CODE = @OutletCode;

                    IF CAST(@currMonth AS numeric(2)) = 12
                    BEGIN
                        print '-- NO NEED TO UPDATE BEGIN BALANCE'
                        print ''
                        SET @NextBal = NULL
                    END
                    ELSE
                    BEGIN
                        -- if not in december
                        -- update beginBalace, mutValue, endBalance for next month
                        SET @NextBal = CAST(@currMonth AS numeric(2)) + 1
                        SET @COLUMNBEGINBAL = 'BEGIN_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                        SET @COLUMNMUTVALUE = 'MUT_VALUE_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                        SET @COLUMNENDBAL = 'END_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                        --SET @COLUMNDEBITVALUE = 'DB_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');
                        --SET @COLUMNCREDITVALUE = 'CR_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');

                        SET @SQLSTMT =
                        N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                        ISNULL(@COLUMNBEGINBAL, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
                                        WHERE BRANCH_CODE=''' + @BRANCHCODE + ''' 
                                        AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' 
                                        AND COA_CODE=''' + @ACCOUNTCODE2 + ''' 
                                        AND OUTLET_CODE=''' + @OutletCode + '''';

                        -- execute begin balcance for next month'
                        EXECUTE SP_EXECUTESQL @SQLSTMT;
                    END

                    -- IS COA ACUM LEVEL 1?
                    SET @CoaAcumLevel = NULL -- restart variable
                    SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                    WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'

                    IF (@CoaAcumLevel = '1')
                    BEGIN
                        SELECT
                            @AccountCode1 = ISNULL(COA_CODE, ' ')
                        FROM COA_MST
                        WHERE COA_CODE = @CoaAcum
                        AND ACTIVE_STATUS = 1;

                        IF @AccountCode1 <> ' ' OR @AccountCode1 IS NOT NULL
                        BEGIN
                            -- ## UPDATE TRX SALDO ##'
                            SET @ParmDefinition = N'@ColumnBeginEx NUMERIC(20,4) OUTPUT,
                                                        @MutValueEx NUMERIC(20,4) OUTPUT,
                                                        @DebitValueEx NUMERIC(15,2) OUTPUT,
                                                        @CreditValueEx NUMERIC(15,2) OUTPUT';
                            
                            SET @SqlStmt = N'SELECT @ColumnBeginEx = ' + ISNULL(@ColumnBegin, 0) + ', @MutValueEx = ' + ISNULL(@ColumnMut, 0) + ',
                                                    @DebitValueEx = ' + ISNULL(@ColumnDebit, 0) + ', @CreditValueEx = ' + ISNULL(@ColumnCredit, 0) + '
                                                    FROM AKT.dbo.TRX_SALDO
                                                    WHERE BRANCH_CODE=''' + @BranchCode + ''' 
                                                    AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' 
                                                    AND COA_CODE=''' + @AccountCode1 + ''' 
                                                    AND OUTLET_CODE=''' + @OutletCode + '''';

                            EXECUTE sp_executesql @SqlStmt,
                                                    @ParmDefinition,
                                                    @ColumnBeginEx = @ColumnBeginEx OUTPUT,
                                                    @MutValueEx = @MutValueEx OUTPUT,
                                                    @DebitValueEx = @DebitValueEx OUTPUT,
                                                    @CreditValueEx = @CreditValueEx OUTPUT;

                            SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@mutValueCurrentYearEarnings * -1, 0);
                            SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);
                            SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@DebitValue, 0);
                            SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@CreditValue, 0);

                            SET @SqlStmt =
                            N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                            ISNULL(@ColumnMut, ' ') + ' = ' + CONVERT(varchar, ISNULL(@MutValue, 0)) + ', ' +
                            ISNULL(@ColumnEnd, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + ', ' +
                            ISNULL(@ColumnDebit, ' ') + ' = ' + CONVERT(varchar, ISNULL(ABS(@DebitValue), 0)) + ',' +
                            ISNULL(@ColumnCredit, ' ') + ' = ' + CONVERT(varchar, ISNULL(ABS(@CreditValue), 0)) + ',
                                        MODIFY_USER= ''' + @UserID + ''' , MODIFY_DATE= ''' + CONVERT(varchar, GETDATE()) + '''
                                    WHERE BRANCH_CODE=''' + @BranchCode + ''' 
                                        AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' AND COA_CODE=''' + @AccountCode1 + ''' 
                                        AND OUTLET_CODE=''' + @OutletCode + '''';
                            
                            -- execute update trx saldo coa query'
                            EXECUTE sp_executesql @SqlStmt;

                            -- ## UPDATE TRX_SALDO DETAIL ##'
                            SELECT
                                @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                                @MutValueDtl = ISNULL(MUT_VALUE, 0),
                                @DebitValueDtl = ISNULL(DB, 0),
                                @CreditValueDtl = ISNULL(CR, 0)
                            FROM AKT.dbo.TRX_SALDO_DTL
                            WHERE BRANCH_CODE = @BranchCode
                            AND SALDO_DATE = DATEADD(DAY, -1, @SystemDate) -- get day before
                            AND COA_CODE = @AccountCode1
                            AND OUTLET_CODE = @OutletCode;

                            -- execute update trx saldo detail query'
                            UPDATE AKT.dbo.TRX_SALDO_DTL
                            SET BEGIN_BAL = ISNULL(@EndBalDtl, 0),
                                MUT_VALUE = MUT_VALUE + ISNULL(@mutValueCurrentYearEarnings * -1,0),
                                END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@mutValueCurrentYearEarnings * -1,0)),
                                DB = (DB + ISNULL(ABS(@DebitValue), 0)),
                                CR = (CR + ISNULL(ABS(@CreditValue), 0)),
                                MODIFY_USER = @UserID,
                                MODIFY_DATE = GETDATE()
                            WHERE BRANCH_CODE = @BranchCode
                            AND SALDO_DATE = @SystemDate
                            AND COA_CODE = @AccountCode1
                            AND OUTLET_CODE = @OutletCode;

                            IF CAST(@currMonth AS numeric(2)) = 12
                            BEGIN
                                print '-- NO NEED TO UPDATE BEGIN BALANCE'
                                print ''
                                SET @NextBal = NULL
                            END
                            ELSE
                            BEGIN
                                -- if not in december
                                -- update beginBalace, mutValue, endBalance for next month
                                SET @NextBal = CAST(@currMonth AS numeric(2)) + 1
                                SET @COLUMNBEGINBAL = 'BEGIN_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                                SET @COLUMNMUTVALUE = 'MUT_VALUE_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                                SET @COLUMNENDBAL = 'END_BAL_' + ISNULL(CAST(@NEXTBAL AS varchar(max)), '');
                                --SET @COLUMNDEBITVALUE = 'DB_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');
                                --SET @COLUMNCREDITVALUE = 'CR_'+ ISNULL(CAST(@NEXTBAL AS VARCHAR(MAX)),'');

                                SET @SQLSTMT =
                                N'UPDATE AKT.dbo.TRX_SALDO SET ' +
                                ISNULL(@COLUMNBEGINBAL, ' ') + ' = ' + CONVERT(varchar, ISNULL(@EndBal, 0)) + '
                                                WHERE BRANCH_CODE=''' + @BRANCHCODE + ''' 
                                                AND YEAR_PERIOD=' + CONVERT(varchar(4), @currYear) + ' 
                                                AND COA_CODE=''' + @AccountCode1 + ''' 
                                                AND OUTLET_CODE=''' + @OutletCode + '''';

                                -- execute begin balcance for next month'
                                EXECUTE SP_EXECUTESQL @SQLSTMT;
                            END
                        -- end update level 1
                        END
                    -- end proses level 1
                    END

                -- end loop
                END

                CLOSE C_COA_LEVEL
                DEALLOCATE C_COA_LEVEL

            -- end loop outled code
            END
            CLOSE c__outlet_code
            DEALLOCATE c__outlet_code
        -- end update current year earnings
        END

        -- debug
        -- SELECT 'CEK CURRENT YEAR EARNINGS', BEGIN_BAL_12, MUT_VALUE_12, END_BAL_12 FROM AKT.dbo.TRX_SALDO
        -- WHERE COA_CODE = '220009000001' OR COA_CODE ='220008000001'

        -- insert AKT.dbo.TRX_SALDO untuk periode tahun depan 
        BEGIN
            INSERT INTO AKT.dbo.TRX_SALDO ([BRANCH_CODE],
            [YEAR_PERIOD],
            [OUTLET_CODE],
            [COA_CODE],
            [BEGIN_BAL_1],
            [MUT_VALUE_1],
            [END_BAL_1],
            [BEGIN_BAL_2],
            [MUT_VALUE_2],
            [END_BAL_2],
            [BEGIN_BAL_3],
            [MUT_VALUE_3],
            [END_BAL_3],
            [BEGIN_BAL_4],
            [MUT_VALUE_4],
            [END_BAL_4],
            [BEGIN_BAL_5],
            [MUT_VALUE_5],
            [END_BAL_5],
            [BEGIN_BAL_6],
            [MUT_VALUE_6],
            [END_BAL_6],
            [BEGIN_BAL_7],
            [MUT_VALUE_7],
            [END_BAL_7],
            [BEGIN_BAL_8],
            [MUT_VALUE_8],
            [END_BAL_8],
            [BEGIN_BAL_9],
            [MUT_VALUE_9],
            [END_BAL_9],
            [BEGIN_BAL_10],
            [MUT_VALUE_10],
            [END_BAL_10],
            [BEGIN_BAL_11],
            [MUT_VALUE_11],
            [END_BAL_11],
            [BEGIN_BAL_12],
            [MUT_VALUE_12],
            [END_BAL_12],
            [DB_1],
            [CR_1],
            [DB_2],
            [CR_2],
            [DB_3],
            [CR_3],
            [DB_4],
            [CR_4],
            [DB_5],
            [CR_5],
            [DB_6],
            [CR_6],
            [DB_7],
            [CR_7],
            [DB_8],
            [CR_8],
            [DB_9],
            [CR_9],
            [DB_10],
            [CR_10],
            [DB_11],
            [CR_11],
            [DB_12],
            [CR_12],
            [CREATE_USER],
            [CREATE_DATE])
                SELECT
                    trd.branch_code,
                    @currYear + 1,
                    trd.OUTLET_CODE,
                    cm.COA_CODE,
                    CASE
                        WHEN cm.coa_code LIKE '4%' OR cm.coa_code LIKE '3%' OR cm.coa_code LIKE '9%' THEN 0
                        ELSE ISNULL(trd.END_BAL_12, 0)
                    END AS [BEGIN_BAL_1],
                    0 AS [MUT_VALUE_1],
                    CASE
                        WHEN cm.coa_code LIKE '4%' OR cm.coa_code LIKE '3%' OR cm.coa_code LIKE '9%' THEN 0
                        ELSE ISNULL(trd.END_BAL_12, 0)
                    END AS [END_BAL_1],
                    0 AS [BEGIN_BAL_2],
                    0 AS [MUT_VALUE_2],
                    0 AS [END_BAL_2],
                    0 AS [BEGIN_BAL_3],
                    0 AS [MUT_VALUE_3],
                    0 AS [END_BAL_3],
                    0 AS [BEGIN_BAL_4],
                    0 AS [MUT_VALUE_4],
                    0 AS [END_BAL_4],
                    0 AS [BEGIN_BAL_5],
                    0 AS [MUT_VALUE_5],
                    0 AS [END_BAL_5],
                    0 AS [BEGIN_BAL_6],
                    0 AS [MUT_VALUE_6],
                    0 AS [END_BAL_6],
                    0 AS [BEGIN_BAL_7],
                    0 AS [MUT_VALUE_7],
                    0 AS [END_BAL_7],
                    0 AS [BEGIN_BAL_8],
                    0 AS [MUT_VALUE_8],
                    0 AS [END_BAL_8],
                    0 AS [BEGIN_BAL_9],
                    0 AS [MUT_VALUE_9],
                    0 AS [END_BAL_9],
                    0 AS [BEGIN_BAL_10],
                    0 AS [MUT_VALUE_10],
                    0 AS [END_BAL_10],
                    0 AS [BEGIN_BAL_11],
                    0 AS [MUT_VALUE_11],
                    0 AS [END_BAL_11],
                    0 AS [BEGIN_BAL_12],
                    0 AS [MUT_VALUE_12],
                    0 AS [END_BAL_12],
                    0 AS [DB_1],
                    0 AS [CR_1],
                    0 AS [DB_2],
                    0 AS [CR_2],
                    0 AS [DB_3],
                    0 AS [CR_3],
                    0 AS [DB_4],
                    0 AS [CR_4],
                    0 AS [DB_5],
                    0 AS [CR_5],
                    0 AS [DB_6],
                    0 AS [CR_6],
                    0 AS [DB_7],
                    0 AS [CR_7],
                    0 AS [DB_8],
                    0 AS [CR_8],
                    0 AS [DB_9],
                    0 AS [CR_9],
                    0 AS [DB_10],
                    0 AS [CR_10],
                    0 AS [DB_11],
                    0 AS [CR_11],
                    0 AS [DB_12],
                    0 AS [CR_12],
                    @UserID,
                    @SystemDate
                FROM coa_mst cm
                JOIN AKT.dbo.TRX_SALDO trd
                    ON cm.coa_code = trd.coa_code
                WHERE trd.year_period = @currYear;
        END

        -- debug
        -- SELECT 'NEW YEAR BEGIN BAL', BEGIN_BAL_1 FROM AKT.dbo.TRX_SALDO
        -- WHERE COA_CODE = '220009000001' OR COA_CODE ='220008000001'

        -- delete and insert trx_saldo_detail only works if fiscal status of present month and year active (status = 1)
        PRINT '-- delete saldo detail'
        -- DELETE SALDO DTL 
        DELETE AKT.dbo.TRX_SALDO_DTL
        WHERE BRANCH_CODE = @BranchCode
            AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
            AND COA_CODE IN (SELECT
                COA_CODE
            FROM AKT.dbo.TRX_SALDO
            WHERE BRANCH_CODE = @BranchCode
            AND YEAR_PERIOD = @currYear);

        PRINT '-- insert saldo detail'
        --INSERT SALDO DTL
        INSERT AKT.dbo.TRX_SALDO_DTL (BRANCH_CODE,
        OUTLET_CODE,
        SALDO_DATE,
        COA_CODE,
        BEGIN_BAL,
        MUT_VALUE,
        END_BAL,
        DB,
        CR,
        CREATE_USER,
        CREATE_DATE)
            SELECT
                @BranchCode,
                A.OUTLET_CODE,
                DATEADD(DAY, +1, @SystemDate),
                A.COA_CODE,
                CASE
                    WHEN A.COA_CODE LIKE '3%' OR A.COA_CODE LIKE '4%' OR A.COA_CODE LIKE '9%'
                    THEN 0
                ELSE 
                    ISNULL((SELECT
                            B.END_BAL
                        FROM AKT.dbo.TRX_SALDO_DTL AS B
                        WHERE B.BRANCH_CODE = @BranchCode
                        AND B.SALDO_DATE = CONVERT(varchar, @SystemDate)
                        AND B.COA_CODE = A.COA_CODE
                        AND B.OUTLET_CODE = A.OUTLET_CODE), 0)
                END,
                0,
                CASE
                    WHEN A.COA_CODE LIKE '3%' OR A.COA_CODE LIKE '4%' OR A.COA_CODE LIKE '9%'
                    THEN 0
                ELSE 
                    ISNULL((SELECT
                        B2.END_BAL
                    FROM AKT.dbo.TRX_SALDO_DTL AS B2
                    WHERE B2.BRANCH_CODE = @BranchCode
                    AND B2.SALDO_DATE = CONVERT(varchar, @SystemDate)
                    AND B2.COA_CODE = A.COA_CODE
                    AND B2.OUTLET_CODE = A.OUTLET_CODE), 0)
                END,
                0,
                0,
                @UserID,
                @SystemDate
            FROM AKT.dbo.TRX_SALDO AS A
            WHERE A.BRANCH_CODE = @BranchCode
            AND A.YEAR_PERIOD = @currYear;

        -- debug
        -- SELECT 'NEW YEAR BEGIN BAL', BEGIN_BAL FROM AKT.dbo.TRX_SALDO_DTL
        -- WHERE COA_CODE = '220009000001' OR COA_CODE ='220008000001'

        -- Create JOURNAL for COA RETAINED EARNINGS & CURRENT YEAR EARNINGS 
        PRINT 'Create JOURNAL for COA RETAINED EARNINGS & CURRENT YEAR EARNINGS '
		DECLARE @EarningAmt numeric (15,2)
		SET @EarningAmt = (SELECT END_BAL_12 FROM AKT.dbo.TRX_SALDO WHERE COA_CODE = '910000000000' AND YEAR_PERIOD = @currYear)
       
        INSERT INTO CF_JOURNAL_INFO (
        BRANCH_CODE,
        OUTLET_CODE	,
        JOURNAL_NO,
        SEQ_NO,
        CONTRACT_NO,
        TRANSACTION_NO,
        TRANSACTION_DATE,
        VALUE_DATE,
        BUSINESS_LINE,
        GROUP_CODE,
        ACCOUNT_CODE,
        POS_JOURNAL,
        CURRENCY_CODE,
        AMOUNT,
        REVERSAL_FLAG,
        REFERENCE_NO,
        FUNCTIONAL_CODE,
        REPAYMENT_TYPE,
        REPAYMENT_SOURCE_CODE,
        REPAYMENT_CHANNEL_CODE,
        PROCESS_FLAG,
        --PROCESS_TRANSACTION_DATE,
        --PROCESS_SYSTEM_DATE,
        --PROCESS_BY_USER,
        JOURNAL_HO_FLAG,
        NOTES,
        --SOURCE_CODE,
        --RK_CODE,
        --CURR_RATE,
        CREATE_USER,
        CREATE_DATE,
        MODIFY_USER,
        MODIFY_DATE)
        VALUES (ISNULL(@BranchCode,'0000'),ISNULL(@OutletCode,'000001'),'ACCT-EARNING'+CONVERT(VARCHAR,(@currYear))+'A',1,'','',@SystemDate,@SystemDate,'','EARNING',@coaCurrentYearEarnings,'D','IDR',@EarningAmt,'0','','EARNING','','','','1','1','EARNING '+CONVERT(VARCHAR,@curryear),@UserID,@SystemDate,@UserID,@SystemDate),
                (ISNULL(@BranchCode,'0000'),ISNULL(@OutletCode,'000001'),'ACCT-EARNING'+CONVERT(VARCHAR,(@currYear))+'B',2,'','',@SystemDate,@SystemDate,'','EARNING',@coaRetainedEarnings,'C','IDR',@EarningAmt,'0','','EARNING','','','','1','1','EARNING '+CONVERT(VARCHAR,@curryear),@UserID,@SystemDate,@UserID,@SystemDate);

        
        -- get endBal currentYearEarnings FOR PROSES RETAINED EARNINGS & CLOSING CURRENT YEAR EARNINGS 
        SELECT 
            @endBalCurrentYearEarnings = END_BAL_12 -- -365
        FROM AKT.dbo.TRX_SALDO
        WHERE 
            COA_CODE = @coaCurrentYearEarnings
            AND BRANCH_CODE = @BranchCode
            AND OUTLET_CODE = @OutletCode
            AND YEAR_PERIOD = @currYear

        -- SELECT 'ENDING YEAR EARNINGS', @endBalCurrentYearEarnings

        -- Proses RETAINED EARNINGS TRX_SALD & TRX_SALDO_DTL
        BEGIN     
            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVEL_
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @coaRetainedEarnings
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL_ + 1 AS LEVELS
            FROM COA_MST,
                COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 1)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                IF @@FETCH_STATUS = -1
                    BREAK

                -- get existing value from current_year+1 begin_bal_1
                SELECT 
                    @ColumnBeginEx = BEGIN_BAL_1,
                    @MutValueEx = MUT_VALUE_1,
                    @DebitValueEx = DB_1,
                    @CreditValueEx = CR_1
                FROM AKT.dbo.TRX_SALDO
                WHERE 
                    BRANCH_CODE = @BranchCode
                    AND OUTLET_CODE = @OutletCode
                    AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    AND COA_CODE = @AccountCode2

                -- debug
                -- SELECT 
                --     'RETAINED EARNINGS BUKAN LEVEL 1 ' + @AccountCode2,
                --     BEGIN_BAL_1,
                --     MUT_VALUE_1,
                --     END_BAL_1,
                --      DB_1,
                --     CR_1
                -- FROM AKT.dbo.TRX_SALDO
                -- WHERE 
                --     BRANCH_CODE = @BranchCode
                --     AND OUTLET_CODE = @OutletCode
                --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                --     AND COA_CODE = @AccountCode2

                SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0); -- -365
                SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0); -- -365
                
                IF (SIGN(@endBalCurrentYearEarnings) = 1)  -- -100
                BEGIN
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + 0;
                END
                ELSE IF (SIGN(@endBalCurrentYearEarnings) = -1)
                BEGIN
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + 0;
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings * -1, 0); -- 365
                END
                ELSE
                BEGIN
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                END

                -- UPDATE TRX SALDO
                UPDATE AKT.dbo.TRX_SALDO
                    SET
                        MUT_VALUE_1 = @MutValue,
                        END_BAL_1 = @EndBal,
                        DB_1 = @DebitValue,
                        CR_1 = @CreditValue
                WHERE 
                    BRANCH_CODE = @BranchCode
                    AND OUTLET_CODE = @OutletCode
                    AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    AND COA_CODE = @AccountCode2

                -- SELECT 
                --     'UPDATED RETAINED EARNINGS BUKAN LEVEL 1 ' + @AccountCode2,
                --     BEGIN_BAL_1,
                --     MUT_VALUE_1,
                --     END_BAL_1,
                --      DB_1,
                --     CR_1
                -- FROM AKT.dbo.TRX_SALDO
                -- WHERE 
                --     BRANCH_CODE = @BranchCode
                --     AND OUTLET_CODE = @OutletCode
                --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                --     AND COA_CODE = @AccountCode2

                -- UPDATE TRX SALDO DTL
                 -- ## UPDATE TRX_SALDO DETAIL ##'
                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode
                AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT 'RETAINED EARNINGS INIT TRX_SALDO_DTL ' + @AccountCode2,
                --     BEGIN_BAL,
                --     ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)) AS 'EndBalDtl',
                --     ISNULL(MUT_VALUE, 0) AS '@MutValueDtl',
                --     ISNULL(DB, 0) AS '@DebitValueDtl',
                --     ISNULL(CR, 0) AS '@CreditValueDtl'
                -- FROM AKT.dbo.TRX_SALDO_DTL
                -- WHERE BRANCH_CODE = @BranchCode
                -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode;
				-- select top 1 * from akt.dbo.TRX_SALDO_DTL

                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET 
                    MUT_VALUE = MUT_VALUE + ISNULL(@endBalCurrentYearEarnings,0),
                    END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@endBalCurrentYearEarnings,0)),
                    DB = (DB + ISNULL(@DebitValue, 0)),
                    CR = (CR + ISNULL(@CreditValue, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode
                AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT 'UPDATED RETAINED EARNINGS INIT TRX_SALDO_DTL ' + @AccountCode2,
                --     BEGIN_BAL,
                --     ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)) AS 'EndBalDtl',
                --     ISNULL(MUT_VALUE, 0) AS '@MutValueDtl',
                --     ISNULL(DB, 0) AS '@DebitValueDtl',
                --     ISNULL(CR, 0) AS '@CreditValueDtl'
                -- FROM AKT.dbo.TRX_SALDO_DTL
                -- WHERE BRANCH_CODE = @BranchCode
                -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode;

                -- IS COA ACUM LEVEL 1?
                SET @CoaAcumLevel = NULL -- restart variable
                SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'

                IF (@CoaAcumLevel = '1')
                BEGIN
                    SELECT
                            @AccountCode1 = ISNULL(COA_CODE, ' ')
                        FROM COA_MST
                        WHERE COA_CODE = @CoaAcum
                        AND ACTIVE_STATUS = 1;

                        SELECT 
                            @ColumnBeginEx = BEGIN_BAL_1, -- 35000
                            @MutValueEx = MUT_VALUE_1, -- 0
                            @DebitValueEx = DB_1, -- 0
                            @CreditValueEx = CR_1 -- 0
                        FROM AKT.dbo.TRX_SALDO
                        WHERE 
                            BRANCH_CODE = @BranchCode
                            AND OUTLET_CODE = @OutletCode
                            AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                            AND COA_CODE = @AccountCode1

                        -- debug
                        -- SELECT
                        --     'RETAINED EARNINGS TRX_SALDO ' + @AccountCode1, 
                        --     BEGIN_BAL_1, -- 35000
                        --     MUT_VALUE_1, -- 0
                        --     DB_1, -- 0
                        --     CR_1 -- 0
                        -- FROM AKT.dbo.TRX_SALDO
                        -- WHERE 
                        --     BRANCH_CODE = @BranchCode
                        --     AND OUTLET_CODE = @OutletCode
                        --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                        --     AND COA_CODE = @AccountCode1

                        SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0); -- -365
                        SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0); -- -330
                        
                        IF (SIGN(@endBalCurrentYearEarnings) = 1)  -- -100
                        BEGIN
                            SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                            SET @CreditValue = ISNULL(@CreditValueEx, 0) + 0;
                        END
                        ELSE IF (SIGN(@endBalCurrentYearEarnings) = -1)
                        BEGIN
                            SET @DebitValue = ISNULL(@DebitValueEx, 0) + 0; -- 0
                            SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings * -1, 0); -- -365
                        END
                        ELSE
                        BEGIN
                            SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                            SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                        END

                        -- UPDATE TRX SALDO
                        UPDATE AKT.dbo.TRX_SALDO
                            SET
                                MUT_VALUE_1 = @MutValue,
                                END_BAL_1 = @EndBal,
                                DB_1 = @DebitValue,
                                CR_1 = @CreditValue
                        WHERE 
                            BRANCH_CODE = @BranchCode
                            AND OUTLET_CODE = @OutletCode
                            AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                            AND COA_CODE = @AccountCode1

                        
                        -- debug
                        -- SELECT
                        --     'UPDATED RETAINED EARNINGS TRX_SALDO ' + @AccountCode1, 
                        --     BEGIN_BAL_1, -- 35000
                        --     MUT_VALUE_1, -- 0
                        --     END_BAL_1,
                        --     DB_1, -- 0
                        --     CR_1 -- 0
                        -- FROM AKT.dbo.TRX_SALDO
                        -- WHERE 
                        --     BRANCH_CODE = @BranchCode
                        --     AND OUTLET_CODE = @OutletCode
                        --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                        --     AND COA_CODE = @AccountCode1


                        -- UPDATE TRX SALDO DTL
                        -- ## UPDATE TRX_SALDO DETAIL ##'
                        SELECT
                            @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                            @MutValueDtl = ISNULL(MUT_VALUE, 0),
                            @DebitValueDtl = ISNULL(DB, 0),
                            @CreditValueDtl = ISNULL(CR, 0)
                        FROM AKT.dbo.TRX_SALDO_DTL
                        WHERE BRANCH_CODE = @BranchCode
                        AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        -- debug
                        -- SELECT
                        --     'RETAINED EARNINGS LEVEL 1 ' + @AccountCode1,
                        --     BEGIN_BAL,
                        --     ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)) AS '@EndBalDtl = ',
                        --     ISNULL(MUT_VALUE, 0) AS '@MutValueDtl = ',
                        --     ISNULL(DB, 0) AS '@DebitValueDtl = ',
                        --     ISNULL(CR, 0) AS '@CreditValueDtl = '
                        -- FROM AKT.dbo.TRX_SALDO_DTL
                        -- WHERE BRANCH_CODE = @BranchCode
                        -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                        -- AND COA_CODE = @AccountCode1
                        -- AND OUTLET_CODE = @OutletCode;

                        UPDATE AKT.dbo.TRX_SALDO_DTL
                        SET 
                            MUT_VALUE = MUT_VALUE + ISNULL(@endBalCurrentYearEarnings,0),
                            END_BAL =  ISNULL(@EndBalDtl, 0) +  (MUT_VALUE + ISNULL(@endBalCurrentYearEarnings,0)),
                            DB = (DB + ISNULL(@DebitValue, 0)),
                            CR = (CR + ISNULL(@CreditValue, 0)),
                            MODIFY_USER = @UserID,
                            MODIFY_DATE = GETDATE()
                        WHERE BRANCH_CODE = @BranchCode
                        AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                        AND COA_CODE = @AccountCode1
                        AND OUTLET_CODE = @OutletCode;

                        -- SELECT
                        --     'UPDATED RETAINED EARNINGS LEVEL 1 ' + @AccountCode1,
                        --     BEGIN_BAL,
                        --     ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)) AS '@EndBalDtl = ',
                        --     ISNULL(MUT_VALUE, 0) AS '@MutValueDtl = ',
                        --     ISNULL(DB, 0) AS '@DebitValueDtl = ',
                        --     ISNULL(CR, 0) AS '@CreditValueDtl = '
                        -- FROM AKT.dbo.TRX_SALDO_DTL
                        -- WHERE BRANCH_CODE = @BranchCode
                        -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                        -- AND COA_CODE = @AccountCode1
                        -- AND OUTLET_CODE = @OutletCode;
                END

            -- end loop C_COA_LEVEL
            END   
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL         
        -- end proses RETAINED EARNINGS TRX_SALD & TRX_SALDO_DTL
        END


        -- proses CURRENT YEAR EARNINGS close / make 0 current year earnings
        BEGIN

            
            -- JURNAL PEMBALIK,  kalo dia -3000 maka harus jadi 3000 didebit
            SET @endBalCurrentYearEarnings = @endBalCurrentYearEarnings * -1;

            DECLARE C_COA_LEVEL CURSOR LOCAL FOR
            WITH COA_LEVEL AS (SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                1 AS LEVEL_
            FROM COA_MST
            WHERE COA_MST.COA_CODE = @coaCurrentYearEarnings
            AND ACTIVE_STATUS = 1
            UNION ALL
            SELECT
                COA_MST.COA_CODE,
                COA_MST.LEVEL,
                COA_MST.COA_ACCUM,
                COA_LEVEL.LEVEL_ + 1 AS LEVELS
            FROM COA_MST,
                COA_LEVEL
            WHERE COA_LEVEL.COA_ACCUM = COA_MST.COA_CODE
            AND ACTIVE_STATUS = 1
            AND COA_MST.LEVEL > 1)

            SELECT DISTINCT
                COA_LEVEL.COA_CODE,
                COA_LEVEL.LEVEL,
                COA_LEVEL.COA_ACCUM
            FROM COA_LEVEL
            WHERE COA_LEVEL.LEVEL > 1
            ORDER BY COA_LEVEL.LEVEL DESC;

            OPEN C_COA_LEVEL
            WHILE 1 = 1
            BEGIN
                FETCH C_COA_LEVEL INTO @AccountCode2, @Level, @CoaAcum;

                IF @@FETCH_STATUS = -1
                    BREAK

                -- get existing value from current_year+1 begin_bal_1
                SELECT 
                    @ColumnBeginEx = BEGIN_BAL_1,
                    @MutValueEx = MUT_VALUE_1,
                    @DebitValueEx = DB_1,
                    @CreditValueEx = CR_1
                FROM AKT.dbo.TRX_SALDO
                WHERE 
                    BRANCH_CODE = @BranchCode
                    AND OUTLET_CODE = @OutletCode
                    AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    AND COA_CODE = @AccountCode2

                -- debug
                -- SELECT 'MUT_VALUE_DIBALIK ' + @AccountCode2, @endBalCurrentYearEarnings;
                -- SELECT 'CURRENT YEAR EARNINGS BUKAN LEVEL 1 ' + @AccountCode2,
                --     BEGIN_BAL_1,
                --     MUT_VALUE_1,
                --     END_BAL_1,
                --     DB_1,
                --     CR_1
                -- FROM AKT.dbo.TRX_SALDO
                -- WHERE 
                --     BRANCH_CODE = @BranchCode
                --     AND OUTLET_CODE = @OutletCode
                --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                --     AND COA_CODE = @AccountCode2

                IF (SIGN(@endBalCurrentYearEarnings) = -1) 
                BEGIN
                    -- change from positife to negative number (-100 * -1)
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + 0;
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings * -1, 0);
                END
                ELSE IF (SIGN(@endBalCurrentYearEarnings) = 1)
                BEGIN
                    -- change from negative to positive number (100 * -1)
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + 0;
                END
                ELSE
                BEGIN
                    SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);;
                    SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                END

                SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);

                -- debug
                -- SELECT 'MUT, END, DB, CR', @MutValue, @EndBal, @DebitValue, @CreditValue

                -- UPDATE TRX SALDO
                UPDATE AKT.dbo.TRX_SALDO
                    SET
                        --BEGIN_BAL_1 = @endBalCurrentYearEarnings,
                        MUT_VALUE_1 = @MutValue,
                        END_BAL_1 = BEGIN_BAL_1 + @MutValue,
                        DB_1 = @DebitValue,
                        CR_1 = @CreditValue
                WHERE 
                    BRANCH_CODE = @BranchCode
                    AND OUTLET_CODE = @OutletCode
                    AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    AND COA_CODE = @AccountCode2
                
                -- debug
                -- SELECT 'UPDATED CURRENT YEAR EARNINGS BUKAN LEVEL 1 ' + @AccountCode2,
                --     BEGIN_BAL_1,
                --     MUT_VALUE_1,
                --     END_BAL_1,
                --     DB_1,
                --     CR_1
                -- FROM AKT.dbo.TRX_SALDO
                -- WHERE 
                --     BRANCH_CODE = @BranchCode
                --     AND OUTLET_CODE = @OutletCode
                --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                --     AND COA_CODE = @AccountCode2

                -- UPDATE TRX SALDO DTL
                 -- ## UPDATE TRX_SALDO DETAIL ##'
                SELECT
                    @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                    @MutValueDtl = ISNULL(MUT_VALUE, 0),
                    @DebitValueDtl = ISNULL(DB, 0),
                    @CreditValueDtl = ISNULL(CR, 0)
                FROM AKT.dbo.TRX_SALDO_DTL
                WHERE BRANCH_CODE = @BranchCode
                AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT
                --     'CURRENT YEAR EARNINGS DTL ' + @AccountCode2,
                --     *
                -- FROM AKT.dbo.TRX_SALDO_DTL
                -- WHERE BRANCH_CODE = @BranchCode
                -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode;

                UPDATE AKT.dbo.TRX_SALDO_DTL
                SET 
                    MUT_VALUE = MUT_VALUE + ISNULL(@endBalCurrentYearEarnings, 0),
                    END_BAL =  BEGIN_BAL + (MUT_VALUE + ISNULL(@endBalCurrentYearEarnings, 0)),
                    DB = (ISNULL(@DebitValue, 0)),
                    CR = (ISNULL(@CreditValue, 0)),
                    MODIFY_USER = @UserID,
                    MODIFY_DATE = GETDATE()
                WHERE BRANCH_CODE = @BranchCode
                AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                AND COA_CODE = @AccountCode2
                AND OUTLET_CODE = @OutletCode;

                -- debug
                -- SELECT
                --     'UPDATED CURRENT YEAR EARNINGS DTL ' + @AccountCode2,
                --     *
                -- FROM AKT.dbo.TRX_SALDO_DTL
                -- WHERE BRANCH_CODE = @BranchCode
                -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                -- AND COA_CODE = @AccountCode2
                -- AND OUTLET_CODE = @OutletCode;

                SET @CoaAcumLevel = NULL -- restart variable
                SELECT @CoaAcumLevel = [LEVEL] FROM COA_MST x
                WHERE [COA_CODE] = @CoaAcum AND [LEVEL] = '1'

                IF (@CoaAcumLevel = '1')
                BEGIN
                    SELECT 
                        @ColumnBeginEx = BEGIN_BAL_1,
                        @MutValueEx = MUT_VALUE_1,
                        @DebitValueEx = DB_1,
                        @CreditValueEx = CR_1
                    FROM AKT.dbo.TRX_SALDO
                    WHERE 
                        BRANCH_CODE = @BranchCode
                        AND OUTLET_CODE = @OutletCode
                        AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                        AND COA_CODE = @AccountCode1

                    -- debug
                    -- SELECT 
                    --     'INIT CURRENT EARNIINGS LEVEL 1: ' + @AccountCode1, BEGIN_BAL_1,
                    --     MUT_VALUE_1,
                    --     DB_1,
                    --     CR_1
                    -- FROM AKT.dbo.TRX_SALDO
                    -- WHERE 
                    --     BRANCH_CODE = @BranchCode
                    --     AND OUTLET_CODE = @OutletCode
                    --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    --     AND COA_CODE = @AccountCode1

                    IF (SIGN(@endBalCurrentYearEarnings) = -1) 
                    BEGIN
                        -- change from positife to negative number (-100 * -1)
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + 0;
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings * -1, 0);
                    END
                    ELSE IF (SIGN(@endBalCurrentYearEarnings) = 1)
                    BEGIN
                        -- change from negative to positive number (100 * -1)
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);;
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + 0;
                    END
                    ELSE
                    BEGIN
                        SET @DebitValue = ISNULL(@DebitValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                        SET @CreditValue = ISNULL(@CreditValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                    END
                    
                    SET @MutValue = ISNULL(@MutValueEx, 0) + ISNULL(@endBalCurrentYearEarnings, 0);
                    SET @EndBal = ISNULL(@ColumnBeginEx, 0) + ISNULL(@MutValue, 0);

                    UPDATE AKT.dbo.TRX_SALDO
                        SET
                            -- BEGIN_BAL_1 = @endBalCurrentYearEarnings,
                            MUT_VALUE_1 = @MutValue,
                            END_BAL_1 = BEGIN_BAL_1 + @MutValue,
                            DB_1 = @DebitValue,
                            CR_1 = @CreditValue
                    WHERE 
                        BRANCH_CODE = @BranchCode
                        AND OUTLET_CODE = @OutletCode
                        AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                        AND COA_CODE = @AccountCode1

                    -- debug
                    -- SELECT 'UPDATE CURRENT EARNIINGS LEVEL 1: ' + @AccountCode1,
                    --     BEGIN_BAL_1,
                    --     MUT_VALUE_1,
                    --     END_BAL_1,
                    --     DB_1,
                    --     CR_1
                    -- FROM AKT.dbo.TRX_SALDO
                    -- WHERE 
                    --     BRANCH_CODE = @BranchCode
                    --     AND OUTLET_CODE = @OutletCode
                    --     AND YEAR_PERIOD = CONVERT(VARCHAR, @currYear + 1)
                    --     AND COA_CODE = @AccountCode1
                     
                    -- ## UPDATE TRX_SALDO DETAIL ##'
                    SELECT
                        @EndBalDtl = ISNULL(END_BAL, (BEGIN_BAL + MUT_VALUE)),
                        @MutValueDtl = ISNULL(MUT_VALUE, 0),
                        @DebitValueDtl = ISNULL(DB, 0),
                        @CreditValueDtl = ISNULL(CR, 0)
                    FROM AKT.dbo.TRX_SALDO_DTL
                    WHERE BRANCH_CODE = @BranchCode
                    AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                    AND COA_CODE = @AccountCode1
                    AND OUTLET_CODE = @OutletCode;

                    -- debug
                    -- SELECT
                    --     'CURRENT YEAR EARNINGS DTL LEVEL 1 ' + @AccountCode1,
                    --    *
                    -- FROM AKT.dbo.TRX_SALDO_DTL
                    -- WHERE BRANCH_CODE = @BranchCode
                    -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                    -- AND COA_CODE = @AccountCode1
                    -- AND OUTLET_CODE = @OutletCode;

                    UPDATE AKT.dbo.TRX_SALDO_DTL
                    SET 
                        MUT_VALUE = MUT_VALUE + ISNULL(@endBalCurrentYearEarnings, 0),
                        END_BAL =  BEGIN_BAL + (MUT_VALUE + ISNULL(@endBalCurrentYearEarnings, 0)),
                        DB = (ISNULL(@DebitValue, 0)),
                        CR = (ISNULL(@CreditValue, 0)),
                        MODIFY_USER = @UserID,
                        MODIFY_DATE = GETDATE()
                    WHERE BRANCH_CODE = @BranchCode
                    AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                    AND COA_CODE = @AccountCode1
                    AND OUTLET_CODE = @OutletCode;

                    -- debug
                    -- debug
                    -- SELECT
                    --     'UPDATED CURRENT YEAR EARNINGS DTL LEVEL 1 ' + @AccountCode1,
                    --     *
                    -- FROM AKT.dbo.TRX_SALDO_DTL
                    -- WHERE BRANCH_CODE = @BranchCode
                    -- AND SALDO_DATE = DATEADD(DAY, +1, @SystemDate)
                    -- AND COA_CODE = @AccountCode1
                    -- AND OUTLET_CODE = @OutletCode;
                END
            -- end loop C_COA_LEVEL
            END
            CLOSE C_COA_LEVEL
            DEALLOCATE C_COA_LEVEL

        -- end proses CURRENT YEAR EARNINGS
        END

        -- change status this fiscal date to close
        BEGIN
            UPDATE AKT.dbo.CALENDAR_FISCAL_MST
            SET STATUS = '2'
            WHERE [YEAR] = @currYear
            AND [MONTH] = @currMonth
        END

        -- update first month of new next year
        BEGIN
            UPDATE AKT.dbo.CALENDAR_FISCAL_MST
            SET STATUS = '1'
            WHERE [YEAR] = @currYear + 1
            AND [MONTH] = 1
        END


        SET @FlagSuccess = 'Y';
        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = 'Y',
            [ERROR_MESSAGE] = 'SUCCESS',
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOY';

        PRINT 'END EOY';

    END TRY
    BEGIN CATCH
        SET @FlagSuccess = 'N';

        UPDATE EOD_LOG
        SET END_DATE = GETDATE(),
            END_TIME = GETDATE(),
            SUCCESS_FLAG = @FlagSuccess,
            [ERROR_MESSAGE] = CONVERT(nvarchar(5), ERROR_LINE()) + ': ' + ERROR_MESSAGE(),
            MODIFY_USER = @UserID,
            MODIFY_DATE = GETDATE()
        WHERE BRANCH_CODE = @BranchCode
        AND CURRENT_TRANSACTION_DATE = @SystemDate
        AND SEQUENCE_NO = @vMaxSeq_ACC
        AND EOD_JOB_TYPE = 'SAL'
        AND [DESCRIPTION] = 'SP_CLOSING_EOY';

        SELECT
            ERROR_NUMBER() AS number,
            ERROR_LINE() AS line,
            ERROR_MESSAGE()
    END CATCH
END